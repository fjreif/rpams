﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Data.DataServiceLayer.Queue;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.Queue;
using Lear.RpaMS.UI.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class PaymentInProgressSendToBankViewModel : ViewModelBase
    {
        private ViewModelManager manager = null;
        private Function function = null;
        private ObservableCollection<BindingViewModel<PaymentSendToBank>> _BindingItems = new ObservableCollection<BindingViewModel<PaymentSendToBank>>();

        #region Constructor
        public PaymentInProgressSendToBankViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion

        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public ObservableCollection<BindingViewModel<PaymentSendToBank>> BindingItems
        {
            get { return _BindingItems; }
            set
            {
                _BindingItems = value;
                RaisePropertyChanged("BindingItems");
            }
        }


        public override void Init()
        {
            function = manager.CheckAndAddFunction(this.LinkedComponent);
            DisplayStatusMessage(function.Name);
            function.Parameters = FunctionService.LoadParameters(function.Id);
            function.Processes = FunctionService.LoadProcesses(function.Id);
        }

        public virtual void LoadItems()
        {
            try
            {
                ValidationMessages.Clear();
                PaymentSendToBankData data = new PaymentSendToBankData();
                List<PaymentSendToBank> items = data.GetPaymentSendToBank(
                        Convert.ToInt32(AppLayer.AppSettings.Instance.CurrentCompany.CompanyCode)
                        );
                if (items != null)
                {
                    List<BindingViewModel<PaymentSendToBank>> itemsSendToBank = ViewModelHelper<PaymentSendToBank>.ModelToViewModel(items);
                    BindingItems = new ObservableCollection<BindingViewModel<PaymentSendToBank>>(itemsSendToBank);
                } else
                {
                    BindingItems = new ObservableCollection<BindingViewModel<PaymentSendToBank>>();
                }
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorLoadItems", ex.Message);
            }
        }

        public void DeleteDocument(PaymentSendToBank entity)
        {
            try
            {
                ValidationMessages.Clear();
                List<int> listIDs = new List<int>();
                entity.Items.ForEach(item =>
                {
                    listIDs.Add(item.Id);
                });
                PaymentItemService.DeactivatePaymentInProgressDocument(entity.DocumentID,
                    listIDs,
                    StatusPaymentListCode.WaitingBankDocument);

                //Move file
                try
                {

                }
                catch (Exception ex)
                {
                    AddValidationMessage("ErrorMoveFile", ex.Message);
                }

            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorDelteDocument", ex.Message);
            }
        }

        public void ShowDocument(string fileName)
        {
            try
            {
                ValidationMessages.Clear();
                Process p = new Process();
                p.StartInfo.FileName = fileName;
                p.Start();
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorShowDocument", ex.Message);
            }
        }

        public int GenerateAutomationQueue(List<PaymentSendToBank> items)
        {
            QueueItem queueItem = new QueueItem();
            int[] ids = new int[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                ids[i] = items[i].DocumentID;
            }
            string jsonItems = JsonConvert.SerializeObject(ids);
            queueItem.TransactionItem = jsonItems;
            queueItem.TransactionItemType = ids.GetType().FullName;
            return QueueService.InsertQueueItem(queueItem);
        }

    }
}
