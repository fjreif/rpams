﻿using Lear.RpaMS.Data.DataServiceLayer.Membership;
using Lear.RpaMS.UI.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Controls
{
    /// <summary>
    /// Interaction logic for ComboBoxItemDefaultControl.xaml
    /// </summary>
    public partial class ComboBoxItemDefaultControl : UserControl
    {

        public enum ComboBoxDefaultTypeList
        {
            BusinessPlace = 1,
            Customer = 2
        }

        private string _TableNameData = string.Empty;
        private string _TableItemDefault = string.Empty;
        private string _TextChangeDefaultItem = string.Empty;
        private string _NameDisplayMember = string.Empty;
        private string _NameSelectedValue = string.Empty;
        private ComboBoxDefaultTypeList _ComboBoxTypeID;


        #region Events
        public delegate void AfterChangeEventHandler(object sender);
        public event AfterChangeEventHandler AfterChange;
        #endregion

        public ComboBoxItemDefaultControl()
        {
            InitializeComponent();
        }

        #region Public Properties
        public string TableNameData
        {
            get { return _TableNameData; }
            set
            {
                _TableNameData = value;
            }
        }

        public string TableItemDefault
        {
            get { return _TableItemDefault; }
            set
            {
                _TableItemDefault = value;
            }
        }

        public string TextChangeDefaultItem
        {
            get { return _TextChangeDefaultItem; }
            set
            {
                _TextChangeDefaultItem = value;
            }
        }

        public string NameDisplayMember
        {
            get { return _NameDisplayMember; }
            set
            {
                _NameDisplayMember = value;
            }
        }

        public string NameSelectedValue
        {
            get { return _NameSelectedValue; }
            set
            {
                _NameSelectedValue = value;
            }
        }

        public ComboBoxDefaultTypeList ComboBoxTypeID
        {
            get { return _ComboBoxTypeID; }
            set
            {
                _ComboBoxTypeID = value;
            }
        }
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings =
                    MembershipService.LoadPreferredSettings(AppLayer.AppSettings.Instance.CurrentUser.UserName);
                ControlsData controlsData = new ControlsData();
                DataTable data = controlsData.LoadDatatable(this.TableNameData);
                List<ItemControl> items = new List<ItemControl>();
                if (data != null && data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        items.Add(new ItemControl()
                        {
                            Id = row[this.NameSelectedValue],
                            Value = row[this.NameDisplayMember].ToString()
                        });
                    }
                    cboItemDefault.ItemsSource = items;
                    cboItemDefault.SelectedValuePath = this.NameSelectedValue;
                    cboItemDefault.DisplayMemberPath = "Value";

                    items.Insert(0, new ItemControl() { Id = 0, Value = this.TextChangeDefaultItem });
                }

                this.SetDefaultValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error when try to open ComboBoxItemDefaultControl: " + ex.Message);
            }
        }

        private void SetDefaultValue()
        {
            if (AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings == null)
            {
                this.SetFirstValue();
                return;
            }

            ItemControl itemDefault = null;
            int? id = 0;
            if (this.ComboBoxTypeID == ComboBoxDefaultTypeList.BusinessPlace)
            {
                id = AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings.BusinessPlaceID;
            }
            else if (this.ComboBoxTypeID == ComboBoxDefaultTypeList.Customer)
            {
                id = AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings.CustomerID;
            }

            List<ItemControl> items = (List<ItemControl>)cboItemDefault.ItemsSource;
            itemDefault = items.Find(item => Convert.ToInt32(item.Id) == id);
            if (itemDefault != null)
            {
                cboItemDefault.SelectedItem = itemDefault;
            }
            else
            {
                this.SetFirstValue();
            }
        }

        private void SetFirstValue()
        {
            List<ItemControl> items = (List<ItemControl>)cboItemDefault.ItemsSource;
            if (items.Count > 1)
            {
                cboItemDefault.SelectedIndex = 1;
            }
            else
            {
                cboItemDefault.SelectedIndex = 0;
            }
        }

        public ItemControl GetSelectedValue()
        {
            ItemControl item = new ItemControl();
            if (cboItemDefault.SelectedItem == null)
            {
                if (AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings == null)
                {
                    AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings =
                        MembershipService.LoadPreferredSettings(AppLayer.AppSettings.Instance.CurrentUser.UserName);
                }
                item.Id = this.GetId();
            }
            else
            {
                item = (ItemControl)cboItemDefault.SelectedItem;
                item.Index = cboItemDefault.SelectedIndex;
            }
            return item;
        }

        private void SaveDefaultItem(ItemControl itemDefault)
        {
            if (this.ComboBoxTypeID == ComboBoxDefaultTypeList.BusinessPlace)
            {
                AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings.BusinessPlaceID = Convert.ToInt32(itemDefault.Id);
            }
            else if (this.ComboBoxTypeID == ComboBoxDefaultTypeList.Customer)
            {
                AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings.CustomerID = Convert.ToInt32(itemDefault.Id);
            }

            MembershipService.SavePreferredSettings(
                AppLayer.AppSettings.Instance.CurrentUser.UserName,
                AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings);
        }

        private void CboItemDefault_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ItemControl item = this.GetSelectedValue();
            if (e.RemovedItems != null && e.RemovedItems.Count > 0)
            {
                ItemControl previousItem = (ItemControl)e.RemovedItems[0];
                if (item.Index == 0)
                {
                    this.SaveDefaultItem(previousItem);
                    if (this.cboItemDefault.SelectedItem != null)
                    {
                        MessageBox.Show("Item padrão alterado!");
                    }
                    this.cboItemDefault.SelectedItem = previousItem;
                } else
                {
                    RaiseAfterChange();
                }
            }
        }

        private int? GetId()
        {
            int? id = 0;
            if (this.ComboBoxTypeID == ComboBoxDefaultTypeList.BusinessPlace)
            {
                id = AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings.BusinessPlaceID;
            }
            else if (this.ComboBoxTypeID == ComboBoxDefaultTypeList.Customer)
            {
                id = AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings.CustomerID;
            }
            return id;
        }

        private void RaiseAfterChange()
        {
            AfterChange(this.cboItemDefault);
        }

    }
}
