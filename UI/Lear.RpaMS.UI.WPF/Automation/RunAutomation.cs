﻿using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ViewModelLayer;
using Lear.RpaMS.UI.WPF.UserControls.Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Lear.RpaMS.UI.WPF.Automation
{
    public class RunAutomation
    {

        private ViewModelManager manager = new ViewModelManager();

        public void StartNewWindow(string projectCode, string processCode, string arguments)
        {
            // Create a thread
            Thread newWindowThread = new Thread(new ThreadStart(() =>
            {
                // Create our context, and install it:
                SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));

                RunAutomationControl userControl = new RunAutomationControl();
                userControl.ProjectCode = projectCode;
                userControl.ProcessCode = processCode;
                userControl.Arguments = arguments;

                Function function = manager.CheckAndAddFunction(userControl.GetType().FullName);

                string _title = function.Name;
                Window window = new Window
                {
                    Title = _title,
                    Content = userControl,
                    Height = function.ScreenHeight,
                    Width = function.ScreenWidth
                };
                window.ShowInTaskbar = function.IsShowInTaskbar;
                window.ResizeMode = ResizeMode.NoResize;
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

                // When the window closes, shut down the dispatcher
                window.Closed += (s, e) =>
                   Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);

                window.Show();
                // Start the Dispatcher Processing
                Dispatcher.Run();
            }));
            // Setup and start thread as before
            newWindowThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
            newWindowThread.Start();            
        }


    }
}
