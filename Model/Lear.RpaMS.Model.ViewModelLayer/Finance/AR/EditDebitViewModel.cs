﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AR;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AR
{
    public class EditDebitViewModel : ViewModelBase
    {

        private ViewModelManager manager = null;
        private Function function = null;
        private Debit _Entity = new Debit();
        private ObservableCollection<StatusBase> _ListStatusDebit = null;
        private ObservableCollection<DebitDestinationArea> _ListDebitDestinationArea = null;
        private ObservableCollection<DebitEvidence> _ListDebitEvidence = null;
        private int _SelectedDebitDestinationAreaID;
        private int _SelectedDebitEvidenceID;
        private int _SelectedStatusDebitID;

        #region Constructor
        public EditDebitViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion

        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public Debit Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }

        public ObservableCollection<StatusBase> ListStatusDebit
        {
            get { return _ListStatusDebit; }
            set
            {
                _ListStatusDebit = value;
                RaisePropertyChanged("ListStatusDebit");
            }
        }

        public ObservableCollection<DebitDestinationArea> ListDebitDestinationArea
        {
            get { return _ListDebitDestinationArea; }
            set
            {
                _ListDebitDestinationArea = value;
                RaisePropertyChanged("ListDebitDestinationArea");
            }
        }

        public ObservableCollection<DebitEvidence> ListDebitEvidence
        {
            get { return _ListDebitEvidence; }
            set
            {
                _ListDebitEvidence = value;
                RaisePropertyChanged("ListDebitEvidence");
            }
        }

        public int SelectedDebitDestinationAreaID
        {
            get { return _SelectedDebitDestinationAreaID; }
            set
            {
                _SelectedDebitDestinationAreaID = value;
                RaisePropertyChanged("SelectedDebitDestinationAreaID");
            }
        }

        public int SelectedDebitEvidenceID
        {
            get { return _SelectedDebitEvidenceID; }
            set
            {
                _SelectedDebitEvidenceID = value;
                RaisePropertyChanged("SelectedDebitEvidenceID");
            }
        }

        public int SelectedStatusDebitID
        {
            get { return _SelectedStatusDebitID; }
            set
            {
                _SelectedStatusDebitID = value;
                RaisePropertyChanged("SelectedStatusDebitID");
            }
        }

        public override void Init()
        {
            try
            {
                ValidationMessages.Clear();
                function = manager.CheckAndAddFunction(this.LinkedComponent);
                DisplayStatusMessage(function.Name);

                this.LoadListStatusDebit();
                this.LoadListDebitDestinationArea();
                this.LoadListDebitEvidences();                
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorInit", ex.Message);
            }
        }

        public void InitEntity(Debit entity)
        {
            this.Entity = entity;
            if (entity.DebitDestinationAreaID != null)
            {
                this.SelectedDebitDestinationAreaID = (int)entity.DebitDestinationAreaID;
            }
            if (entity.DebitEvidenceID != null)
            {
                this.SelectedDebitEvidenceID = (int)entity.DebitEvidenceID;
            }
            if (entity.StatusDebitID != null)
            {
                this.SelectedStatusDebitID = (int)entity.StatusDebitID;
            }
        }

        private void LoadListStatusDebit()
        {
            List<StatusBase> status = StatusBaseService.LoadAllStatusDebitAR();
            ListStatusDebit = new ObservableCollection<StatusBase>(status);
        }

        private void LoadListDebitDestinationArea()
        {
            List<DebitDestinationArea> destination = DebitService.LoadAllDestinationArea();
            ListDebitDestinationArea = new ObservableCollection<DebitDestinationArea>(destination);
        }

        private void LoadListDebitEvidences()
        {
            List<DebitEvidence> evidences = DebitService.LoadAllEvicences();
            ListDebitEvidence = new ObservableCollection<DebitEvidence>(evidences);
        }

        public void Save()
        {
            try
            {
                Entity.DebitDestinationAreaID = this.SelectedDebitDestinationAreaID;
                Entity.DebitEvidenceID = this.SelectedDebitEvidenceID;
                Entity.StatusDebitID = this.SelectedStatusDebitID;
                DebitService.Update(Entity);

                DisplayNotificationMessage("Débito atualizado com sucesso!");                
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorSave", ex.Message);
            }
        }

    }
}
