﻿using Lear.RpaMS.Common.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ModelLayer
{
    public class FunctionParameter : CommonBase
    {

        private int _Id;
        private int _FunctionId;        
        private string _Name;
        private string _Value;
        private bool _Active;


        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                RaisePropertyChanged("Id");
            }
        }

        public int FunctionId
        {
            get { return _FunctionId; }
            set
            {
                _FunctionId = value;
                RaisePropertyChanged("FunctionId");
            }
        }

        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                RaisePropertyChanged("Name");
            }
        }


        public string Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                RaisePropertyChanged("Value");
            }
        }

        public bool Active
        {
            get { return _Active; }
            set
            {
                _Active = value;
                RaisePropertyChanged("Active");
            }
        }


    }
}
