﻿using Lear.RpaMS.Common.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ModelLayer
{
    public class Function : CommonBase
    {

        private int _FunctionId;
        private string _Name;
        private string _Component;
        private bool _IsNewWindow;
        private int _ScreenHeight;
        private int _ScreenWidth;
        private bool _IsShowInTaskbar;
        private bool _Active;

        private List<FunctionParameter> _parameters;

        public Function()
        {
            _parameters = new List<FunctionParameter>();
        }

        #region Public Properties
        public int FunctionId
        {
            get { return _FunctionId; }
            set
            {
                _FunctionId = value;
                RaisePropertyChanged("FunctionId");
            }
        }

        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Component
        {
            get { return _Component; }
            set
            {
                _Component = value;
                RaisePropertyChanged("Component");
            }
        }

        public bool IsNewWindow
        {
            get { return _IsNewWindow; }
            set
            {
                _IsNewWindow = value;
                RaisePropertyChanged("IsNewWindow");
            }
        }

        public int ScreenHeight
        {
            get { return _ScreenHeight; }
            set
            {
                _ScreenHeight = value;
                RaisePropertyChanged("ScreenHeight");
            }
        }

        public int ScreenWidth
        {
            get { return _ScreenWidth; }
            set
            {
                _ScreenWidth = value;
                RaisePropertyChanged("ScreenWidth");
            }
        }


        public bool IsShowInTaskbar
        {
            get { return _IsShowInTaskbar; }
            set
            {
                _IsShowInTaskbar = value;
                RaisePropertyChanged("IsShowInTaskbar");
            }
        }

        public bool Active
        {
            get { return _Active; }
            set
            {
                _Active = value;
                RaisePropertyChanged("Active");
            }
        }

        public List<FunctionParameter> Parameters
        {
            get { return _parameters; }
            set
            {
                _parameters = value;
                RaisePropertyChanged("Parameters");
            }
        }
        #endregion


        public FunctionParameter GetParameterByName(string paramName)
        {
            if (_parameters != null && _parameters.Count > 0)
            {
                return _parameters.Find(param => param.Name == paramName);
            }
            return null;
        }

        //public List<FunctionParameter> GetParameters()
        //{
        //    if (_parameters == null)
        //    {
        //        return null;
        //    }
        //    List<FunctionParameter> paramList = new List<FunctionParameter>();
        //    foreach (FunctionParameter param in _parameters)
        //    {
        //        FunctionParameter parameter = null;
        //        param.Clone(param, parameter);
        //        paramList.Add(parameter);
        //    }
        //    return paramList;
        //}

    }
}
