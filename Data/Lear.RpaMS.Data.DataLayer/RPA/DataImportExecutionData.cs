﻿using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataLayer.RPA
{
    public class DataImportExecutionData
    {

        private static readonly string TABLE_NAME = "RPA_DataImportExecution";

        public int StartExecution(int dataImportRoutineID, string message)
        {
            SqlCommand command = null;
            int insertedID = 0;

            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.INSERT);
                query.AddFields("DataImportRoutineID, UserCode, MachineName, DateTimeStart, Message");
                query.AddTable(TABLE_NAME);
                query.AddValues("@DataImportRoutineID, @UserCode, @MachineName, GETDATE(), @Message");
                query.IsScopeIdentity = true;

                command = DataConnectionBase.GetCommand();
                command.CommandText = query.GetQuery();
                command.Parameters.Add("@DataImportRoutineID", System.Data.SqlDbType.Int);
                command.Parameters["@DataImportRoutineID"].Value = dataImportRoutineID;
                command.Parameters.Add("@UserCode", System.Data.SqlDbType.VarChar);
                command.Parameters["@UserCode"].Value = AppLayer.AppSettings.Instance.CurrentUser.UserName;
                command.Parameters.Add("@MachineName", System.Data.SqlDbType.VarChar);
                command.Parameters["@MachineName"].Value = AppLayer.AppSettings.Instance.CurrentMachineName;
                command.Parameters.Add("@Message", System.Data.SqlDbType.VarChar);
                command.Parameters["@Message"].Value = message;

                command.Connection.Open();
                object ret = command.ExecuteScalar();
                if (ret != null)
                {
                    insertedID = Convert.ToInt32(ret);
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return insertedID;
        }


        public void EndExecution(int dataImportExecuitonID, StatusExecution status, string message)
        {
            SqlCommand command = null;

            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.UPDATE);
                query.AddTable(TABLE_NAME);
                query.AddValues("DateTimeEnd = GETDATE(), StatusExecutionID = @StatusExecutionID, Message = @Message");
                query.AddWhereConditions("ID = @ID");
                
                command = DataConnectionBase.GetCommand();
                command.CommandText = query.GetQuery();
                command.Parameters.Add("@StatusExecutionID", System.Data.SqlDbType.Int);
                command.Parameters["@StatusExecutionID"].Value = (int)status;
                command.Parameters.Add("@Message", System.Data.SqlDbType.VarChar);
                command.Parameters["@Message"].Value = message;
                command.Parameters.Add("@ID", System.Data.SqlDbType.Int);
                command.Parameters["@ID"].Value = dataImportExecuitonID;

                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }
        }

    }
}
