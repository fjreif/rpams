﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.UI.Data
{
    public class ItemControl
    {


        private object _Id;
        private string _Value;
        private int _Index;

        public object Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        public string Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
            }
        }

        public int Index
        {
            get { return _Index; }
            set
            {
                _Index = value;
            }
        }

    }
}
