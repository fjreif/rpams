﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ModelLayer
{
    public class TransalationLanguage
    {
        public static int ENGLISH = 1;
        public static int PORTUGUESE = 2;
        public static int SPANISH = 3;
        
        public static string GetLanguageCode(int languageID)
        {
            string code = null;
            if (languageID == ENGLISH)
            {
                code = "en";
            } else if (languageID == PORTUGUESE)
            {
                code = "pt";
            } else if (languageID == SPANISH)
            {
                code = "es";
            }
            return code;
        }

    }
}
