﻿using System;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lear.RpaMS.Tests.Business
{
    [TestClass]
    public class ImportPaymentItemsTest
    {



        [TestMethod]
        public void TestImportPendindingPayments()
        {
            string _fileReportImportsToExpire = @"C:\RPA\Dev\#Projects\AutorizacaoPagamentoFaturaArgentina\#Documents\Base PWS - AP Open Itens.XLS";
            string _fileReportVendorLineItems = @"C:\RPA\Dev\#Projects\AutorizacaoPagamentoFaturaArgentina\#Documents\Base FBL1N - AP Open Itens - FREIF.XLS";

            try
            {
                Company company = new Company();
                company.CompanyCode = "5000";

                User user = new User();
                user.UserName = Environment.UserName;

                int executionID = DataImportExecutionService.StartExecution(1, "");

                RpaMS.Business.Finance.AP.ImportPaymentItemsBusiness importBusiness = new RpaMS.Business.Finance.AP.ImportPaymentItemsBusiness();
                string ret = importBusiness.ImportPendingPayments(_fileReportImportsToExpire,
                    _fileReportVendorLineItems,
                    company,
                    user,
                    "DS-DESEMBARAÇADO;MR-MATERIAL RECEBIDO",
                    1,
                    executionID);
                if (String.IsNullOrEmpty(ret))
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;                
            }
        }
    }
}
