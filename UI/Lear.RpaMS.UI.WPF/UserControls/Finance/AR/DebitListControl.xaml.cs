﻿using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AR;
using Lear.RpaMS.Model.ViewModelLayer.Finance.AR;
using Lear.RpaMS.UI.WPF.Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AR
{
    /// <summary>
    /// Interaction logic for DebitListControl.xaml
    /// </summary>
    public partial class DebitListControl : UserControl
    {
        // View model class
        private DebitListViewModel _viewModel = null;

        public DebitListControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (DebitListViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        private async void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            await this.UpdateData();
        }        

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadAutomationDefition();
                await this.UpdateData();
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("ErrorControlLoaded", ex.Message);
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private async Task UpdateData()
        {
            _viewModel.BusinessPlace = cboBusinessPlace.GetSelectedValue();
            _viewModel.Customer = cboCustomer.GetSelectedValue();
            await _viewModel.LoadItems();
        }        

        private void DtgDebit_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                _viewModel.ValidationMessages.Clear();
                DataGrid grid = (DataGrid)sender;                
                if (grid.CurrentItem != null)
                {
                    Common.Library.Binding.BindingViewModel<Debit> debit = (Common.Library.Binding.BindingViewModel<Debit>)grid.CurrentItem;
                    _viewModel.Entity = debit.Entity;
                    _viewModel.LoadDocuments();
                }
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("ErrorDebit_MouseDown", ex.Message);
            }
            
        }

        private void BtnViewDocument_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Model.ModelLayer.RPA.Document document = (Model.ModelLayer.RPA.Document)button.Tag;
            _viewModel.ShowDocument(document);

        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            StartAutoContextMenu.PlacementTarget = imgContextMenu;
            StartAutoContextMenu.IsOpen = true;
        }

        private void BtnStartAutomation_Click(object sender, RoutedEventArgs e)
        {
            this.StartAutomation();
        }

        private void StartAutomation()
        {
            try
            {
                _viewModel.ValidationMessages.Clear();
                Function thisFunction = _viewModel.Manager.GetFunction(this.GetType().FullName);
                RunAutomation runAutomation = new RunAutomation();
                runAutomation.StartNewWindow(
                    thisFunction.GetParameterByName("StartAutomationProjectCode").Value,
                    thisFunction.GetParameterByName("StartAutomationProcessCode").Value,
                    string.Empty);
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("", ex.Message);
            }
        }

        private void StartAutomation(string projectCode, string processCode, string arguments)
        {
            try
            {
                _viewModel.ValidationMessages.Clear();
                Function thisFunction = _viewModel.Manager.GetFunction(this.GetType().FullName);
                RunAutomation runAutomation = new RunAutomation();
                runAutomation.StartNewWindow(projectCode, processCode, arguments);
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("", ex.Message);
            }
        }

        private void LoadAutomationDefition()
        {
            List<ProjectProcess> processes = _viewModel.GetProcesses();
            if (processes != null && processes.Count > 0)
            {
                foreach (var process in processes)
                {
                    MenuItem menuItem = new MenuItem();
                    menuItem.Header = process.ShortName;
                    menuItem.Name = process.Code;
                    menuItem.Tag = process;
                    menuItem.Click += MenuItem_Click;
                    StartAutoContextMenu.Items.Add(menuItem);
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            ProjectProcess process = (ProjectProcess)menuItem.Tag;
            this.StartAutomation(process.Project.Code, process.Code, string.Empty);            
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _viewModel.ValidationMessages.Clear();
                Common.Library.Binding.BindingViewModel<Debit> binding = (Common.Library.Binding.BindingViewModel<Debit>)((Button)sender).Tag;            
                EditDebitControl userControl = new EditDebitControl();
                userControl.SetEntity(binding.Entity);
                WindowManager.CreateNewWindow(userControl);
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("", ex.Message);
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.Close();
        }

        private async void CboBusinessPlace_AfterChange(object sender)
        {
            await this.UpdateData();
        }

        private async void CboCustomer_AfterChange(object sender)
        {
            await this.UpdateData();
        }
        
    }
}
