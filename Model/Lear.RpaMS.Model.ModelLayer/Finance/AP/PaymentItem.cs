﻿using Lear.RpaMS.Common.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ModelLayer.Finance.AP
{
    public class PaymentItem : CommonBase
    {

        private int _Id;
        private string _Company;
        private string _PlantCode;
        private string _PlantName;
        private string _BusinessPlaceCode;
        private string _BusinessPlaceName;
        private string _ProvisionDoc;
        private string _VendorCode;
        private string _VendorName;
        private string _ReferenceImportation;
        private string _ShipmentNumber;
        private string _Currency;
        private string _ShipmentSeqNumber;
        private double _Value;

        private string _ImportReference;
        private DateTime? _ExpirationDate;
        private string _ShipmentStatus;
        private string _Account;
        private string _InvoiceReference;
        private string _Assignment;
        private string _InvoiceDocumentNumber;
        private string _InvoiceDocumentType;
        private DateTime? _InvoicePostingDate;
        private DateTime? _InvoiceDocumentDate;
        private int _DelayAfterNetDueDate;
        private DateTime? _InvoiceNetDueDate;
        private string _InvoiceDocumentCurrency;
        private double _InvoiceAmountCurrency;
        private double _InvoiceAmountLocalCurrency;
        private string _InvoiceText;
                
        private DateTime _DateInsert;
        private string _UserCodeInsert;
        private DateTime? _DateUpdate;
        private string _UserCodeUpdate;

        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                RaisePropertyChanged("ID");
            }
        }

        public string Company
        {
            get { return _Company; }
            set
            {
                _Company = value;
                RaisePropertyChanged("Company");
            }
        }

        public string PlantCode
        {
            get { return _PlantCode; }
            set
            {
                _PlantCode = value;
                RaisePropertyChanged("PlantCode");
            }
        }

        public string PlantName
        {
            get { return _PlantName; }
            set
            {
                _PlantName = value;
                RaisePropertyChanged("PlantName");
            }
        }


        public string BusinessPlaceCode
        {
            get { return _BusinessPlaceCode; }
            set
            {
                _BusinessPlaceCode = value;
                RaisePropertyChanged("BusinessPlaceCode");
            }
        }

        public string BusinessPlaceName
        {
            get { return _BusinessPlaceName; }
            set
            {
                _BusinessPlaceName = value;
                RaisePropertyChanged("BusinessPlaceName");
            }
        }

        public string ProvisionDoc
        {
            get { return _ProvisionDoc; }
            set
            {
                _ProvisionDoc = value;
                RaisePropertyChanged("ProvisionDoc");
            }
        }


        public string VendorCode
        {
            get { return _VendorCode; }
            set
            {
                _VendorCode = value;
                RaisePropertyChanged("VendorCode");
            }
        }


        public string VendorName
        {
            get { return _VendorName; }
            set
            {
                _VendorName = value;
                RaisePropertyChanged("VendorName");
            }
        }

        public string Vendor
        {
            get { return _VendorCode + " - " + _VendorName; }
        }


        public string ReferenceImportation
        {
            get { return _ReferenceImportation; }
            set
            {
                _ReferenceImportation = value;
                RaisePropertyChanged("ReferenceImportation");
            }
        }


        public string ShipmentNumber
        {
            get { return _ShipmentNumber; }
            set
            {
                _ShipmentNumber = value;
                RaisePropertyChanged("ShipmentNumber");
            }
        }


        public string Currency
        {
            get { return _Currency; }
            set
            {
                _Currency = value;
                RaisePropertyChanged("Currency");
            }
        }


        public string ShipmentSeqNumber
        {
            get { return _ShipmentSeqNumber; }
            set
            {
                _ShipmentSeqNumber = value;
                RaisePropertyChanged("ShipmentSeqNumber");
            }
        }

        public double Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                RaisePropertyChanged("Value");
            }
        }

        public DateTime DateInsert
        {
            get { return _DateInsert; }
            set
            {
                _DateInsert = value;
                RaisePropertyChanged("DateInsert");
            }
        }

        public string UserCodeInsert
        {
            get { return _UserCodeInsert; }
            set
            {
                _UserCodeInsert = value;
                RaisePropertyChanged("UserCodeInsert");
            }
        }


        public DateTime? DateUpdate
        {
            get { return _DateUpdate; }
            set
            {
                _DateUpdate = value;
                RaisePropertyChanged("DateUpdate");
            }
        }

        public string UserCodeUpdate
        {
            get { return _UserCodeUpdate; }
            set
            {
                _UserCodeUpdate = value;
                RaisePropertyChanged("UserCodeUpdate");
            }
        }

        public string ImportReference
        {
            get { return _ImportReference; }
            set
            {
                _ImportReference = value;
                RaisePropertyChanged("ImportReference");
            }
        }


        public DateTime? ExpirationDate { get => _ExpirationDate;
            set {
                _ExpirationDate = value;
                RaisePropertyChanged("ExpirationDate");
            }
        }

        public string ShipmentStatus { get => _ShipmentStatus;
            set {
                _ShipmentStatus = value;
                RaisePropertyChanged("ShipmentStatus");
            }
        }

        public string Account { get => _Account;
            set {
                _Account = value;
                RaisePropertyChanged("Account");
            }
        }

        public string InvoiceReference { get => _InvoiceReference;
            set
            {
                _InvoiceReference = value;
                RaisePropertyChanged("InvoiceReference");
            }
        }

        public string Assignment { get => _Assignment;
            set {
                _Assignment = value;
                RaisePropertyChanged("Assignment");
            }
        }

        public string InvoiceDocumentNumber { get => _InvoiceDocumentNumber;
            set {
                _InvoiceDocumentNumber = value;
                RaisePropertyChanged("InvoiceDocumentNumber");
            }
        }

        public string InvoiceDocumentType { get => _InvoiceDocumentType;
            set {
                _InvoiceDocumentType = value;
                RaisePropertyChanged("InvoiceDocumentType");
            }
        }

        public DateTime? InvoicePostingDate { get => _InvoicePostingDate;
            set {
                _InvoicePostingDate = value;
                RaisePropertyChanged("InvoicePostingDate");
            }
        }

        public DateTime? InvoiceDocumentDate { get => _InvoiceDocumentDate;
            set {
                _InvoiceDocumentDate = value;
                RaisePropertyChanged("InvoiceDocumentDate");
            }
        }

        public int DelayAfterNetDueDate { get => _DelayAfterNetDueDate;
            set
            {
                _DelayAfterNetDueDate = value;
                RaisePropertyChanged("DelayAfterNetDueDate");
            }
        }

        public DateTime? InvoiceNetDueDate { get => _InvoiceNetDueDate;
            set
            {
                _InvoiceNetDueDate = value;
                RaisePropertyChanged("InvoiceNetDueDate");
            }
        }

        public string InvoiceDocumentCurrency { get => _InvoiceDocumentCurrency;
            set
            {
                _InvoiceDocumentCurrency = value;
                RaisePropertyChanged("InvoiceDocumentCurrency");
            }
        }

        public double InvoiceAmountCurrency { get => _InvoiceAmountCurrency;
            set
            {
                _InvoiceAmountCurrency = value;
                RaisePropertyChanged("InvoiceAmountCurrency");
            }
        }

        public double InvoiceAmountLocalCurrency { get => _InvoiceAmountLocalCurrency;
            set
            {
                _InvoiceAmountLocalCurrency = value;
                RaisePropertyChanged("InvoiceAmountLocalCurrency");
            }
        }

        public string InvoiceText { get => _InvoiceText;
            set
            {
                _InvoiceText = value;
                RaisePropertyChanged("InvoiceText");
            }
        }

    }
}
