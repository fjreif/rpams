﻿using Lear.RpaMS.Data.DataLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataServiceLayer.RPA
{
    public class DataImportExecutionService
    {

        public static int StartExecution(int dataImportRoutineID, string message)
        {
            DataImportExecutionData data = new DataImportExecutionData();
            return data.StartExecution(dataImportRoutineID, message);
        }


        public static void EndExecution(int dataImportExecuitonID, StatusExecution status, string message)
        {
            DataImportExecutionData data = new DataImportExecutionData();
            data.EndExecution(dataImportExecuitonID, status, message);
        }

    }
}
