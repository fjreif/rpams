﻿using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using Lear.RpaMS.UI.WPF.Automation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for PaymentInProgressControl.xaml
    /// </summary>
    public partial class PaymentInProgressControl : UserControl
    {
        public PaymentInProgressControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (PaymentInProgressViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        // View model class
        private PaymentInProgressViewModel _viewModel = null;

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadItems();
            LoadAutomationDefition();            
        }

        private void BtnStartAutomation_Click(object sender, RoutedEventArgs e)
        {
            this.StartAutomation();
        }

        private void BtnGenerateDocuments_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Deseja realmente gerar documentos para items selecionados?",
                "Gerar items selecionados?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    _viewModel.ValidationMessages.Clear();
                    List<PaymentItem> itemsSelected = ViewModelHelper<PaymentItem>.GetSelectedItems((ObservableCollection<BindingViewModel<PaymentItem>>)dtgPaymentInProgress.ItemsSource);
                    if (itemsSelected == null || itemsSelected.Count == 0)
                    {
                        MessageBox.Show("Nenhum item selecionado!");
                        return;
                    }

                    int queueID = _viewModel.GenerateAutomationQueue(itemsSelected);
                    string arguments = $" \"{queueID}\"";

                    Function thisFunction = _viewModel.Manager.GetFunction(this.GetType().FullName);
                    RunAutomation runAutomation = new RunAutomation();
                    runAutomation.StartNewWindow(
                        thisFunction.GetParameterByName("StartAutomationProjectCode").Value,
                        thisFunction.GetParameterByName("StartAutomationProcessCodeQueue").Value,
                        arguments);

                }
                catch (Exception ex)
                {
                    _viewModel.AddValidationMessage("", ex.Message);
                }
            }
        }

        private void BtnTypeInvoice_Click(object sender, RoutedEventArgs e)
        {
            UserControl userControl = new PaymentInProgressInvoiceControl();
            WindowManager.CreateNewWindow(userControl);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Deseja realmente excluir o item selecionado?",
                "Excluir item selecionado?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    _viewModel.ValidationMessages.Clear();
                    // Set selected item
                    BindingViewModel<PaymentItem> bindingViewModel = (BindingViewModel<PaymentItem>)((Button)sender).Tag;
                    _viewModel.Entity = bindingViewModel.Entity;
                    _viewModel.DeactivateItem();
                    _viewModel.LoadItems();
                }
                catch (Exception ex)
                {
                    _viewModel.AddValidationMessage("", ex.Message);
                }
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            _viewModel.Close();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadItems();
        }        

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ChkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<BindingViewModel<PaymentItem>> items = (ObservableCollection<BindingViewModel<PaymentItem>>)dtgPaymentInProgress.ItemsSource;
            foreach (var item in items)
            {
                item.IsSelected = (bool)((CheckBox)e.Source).IsChecked;
            }
        }

        private void Image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            StartAutoContextMenu.PlacementTarget = imgContextMenu;
            StartAutoContextMenu.IsOpen = true;
        }


        private void StartAutomation()
        {
            try
            {
                _viewModel.ValidationMessages.Clear();
                Function thisFunction = _viewModel.Manager.GetFunction(this.GetType().FullName);
                RunAutomation runAutomation = new RunAutomation();
                runAutomation.StartNewWindow(
                    thisFunction.GetParameterByName("StartAutomationProjectCode").Value,
                    thisFunction.GetParameterByName("StartAutomationProcessCode").Value,
                    string.Empty);
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("", ex.Message);
            }
        }

        private void StartAutomation(string projectCode, string processCode, string arguments)
        {
            try
            {
                _viewModel.ValidationMessages.Clear();
                Function thisFunction = _viewModel.Manager.GetFunction(this.GetType().FullName);
                RunAutomation runAutomation = new RunAutomation();
                runAutomation.StartNewWindow(projectCode, processCode, arguments);
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("", ex.Message);
            }
        }

        private void LoadAutomationDefition()
        {
            List<ProjectProcess> processes = _viewModel.GetProcesses();
            if (processes != null && processes.Count > 0)
            {
                foreach (var process in processes)
                {
                    MenuItem menuItem = new MenuItem();
                    menuItem.Header = process.ShortName;
                    menuItem.Name = process.Code;
                    menuItem.Tag = process;
                    menuItem.Click += MenuItem_Click;
                    StartAutoContextMenu.Items.Add(menuItem);
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            if (menuItem.Name.Equals("PagArgentinaDespachoAndDocuments"))
            {
                this.StartAutomation();

            }
            else if (menuItem.Name.Equals("PagArgentinaUploadData"))
            {
                PaymentInProgressSendToBankControl userControl = new PaymentInProgressSendToBankControl();
                WindowManager.CreateNewWindow(userControl);
            }
            else if (menuItem.Name.Equals("PagArgentinaStatusBank"))
            {
                ProjectProcess process = (ProjectProcess)menuItem.Tag;
                this.StartAutomation(process.Project.Code, process.Code, string.Empty);
            }
        }

        private void BtnDeleteSelectedItems_Click(object sender, RoutedEventArgs e)
        {
            // Ask if the user wants to pay the selected items
            if (MessageBox.Show("Deseja realmente excluir itens selecionados?",
                "Excluir items selecionados?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                List<PaymentItem> itemsSelected = ViewModelHelper<PaymentItem>.GetSelectedItems((ObservableCollection<BindingViewModel<PaymentItem>>)dtgPaymentInProgress.ItemsSource);
                _viewModel.DeactivateItems(itemsSelected);
                _viewModel.LoadItems();                
            }
        }

        private void DtgPaymentInProgress_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

        }

        private void ChkSelectItem_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            BindingViewModel<PaymentItem> bindingViewModel = (BindingViewModel<PaymentItem>)chk.Tag;
            bindingViewModel.IsSelected = (bool)chk.IsChecked;
        }

        private void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            UserControl userControl = new ConfigPaymentDocumentControl();
            WindowManager.CreateNewWindow(userControl);
        }
        
    }
}
