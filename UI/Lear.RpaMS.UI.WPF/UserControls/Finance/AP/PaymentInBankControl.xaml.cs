﻿using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using Lear.RpaMS.UI.WPF.Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for PaymentInBankControl.xaml
    /// </summary>
    public partial class PaymentInBankControl : UserControl
    {
        // View model class
        private PaymentInBankViewModel _viewModel = null;

        public PaymentInBankControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (PaymentInBankViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.UpdateData();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            _viewModel.Close();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            this.UpdateData();
        }

        private void BtnStartAutomation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _viewModel.ValidationMessages.Clear();
                Function thisFunction = _viewModel.Manager.GetFunction(this.GetType().FullName);
                RunAutomation runAutomation = new RunAutomation();
                runAutomation.StartNewWindow(
                    thisFunction.GetParameterByName("StartAutomationProjectCode").Value,
                    thisFunction.GetParameterByName("StartAutomationProcessCode").Value,
                    string.Empty);
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("", ex.Message);
            }
        }

        private void UpdateData()
        {
            _viewModel.LoadItems();
            dtgPaymentsInBank.ItemsSource = _viewModel.ItemsGroupView;
        }

    }
}
