﻿using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataLayer
{
    public class CompanyData
    {


        public List<Company> GetCompanies()
        {
            List<Company> companies = new List<Company>();
            SqlCommand command = null;
            try
            {
                command = DataConnectionBase.GetCommand();
                command.CommandText = "SELECT * FROM T_Company";
                command.Connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Company company = new Company();
                        company.CompanyCode = reader["Company"].ToString();
                        company.CompanyName = reader["CompanyName"].ToString();
                        company.LocalCurrency = reader["LocalCurrency"].ToString();
                        companies.Add(company);
                    }
                }
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }
            return companies;
        }


    }
}
