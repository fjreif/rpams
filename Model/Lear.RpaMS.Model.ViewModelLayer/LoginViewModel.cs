﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer.Membership;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer
{
    public class LoginViewModel : ViewModelBase
    {

        #region Constructor
        public LoginViewModel() : base()
        {
            DisplayStatusMessage("Login to Application");

            Entity = new User
            {
                UserName = Environment.UserName
            };
        }
        #endregion

        #region Properties
        private User _Entity;

        public User Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }
        #endregion

        #region Login Method
        public bool Login()
        {
            bool ret = false;

            if (Validate())
            {
                // Check Credentials in User Table
                if (ValidateCredentials())
                {
                    // Mark as logged in
                    Entity.IsLoggedIn = true;

                    //Store the current user logged
                    AppLayer.AppSettings.Instance.CurrentUser = Entity;
                    AppLayer.AppSettings.Instance.CurrentMachineName = Environment.MachineName;

                    // Send message that login was successful
                    MessageBroker.Instance.SendMessage(
                        MessageBrokerMessages.LOGIN_SUCCESS, Entity);

                    // Close the user control
                    Close(false);

                    ret = true;
                }
            }

            return ret;
        }

        public bool LoginWindows()
        {
            bool ret = false;

            // Check Credentials
            if (ValidateCredentials(true))
            {
                // Mark as logged in
                Entity.IsLoggedIn = true;

                //Store the current user logged
                AppLayer.AppSettings.Instance.CurrentUser = Entity;
                AppLayer.AppSettings.Instance.CurrentMachineName = Environment.MachineName;

                // Send message that login was successful
                MessageBroker.Instance.SendMessage(
                    MessageBrokerMessages.LOGIN_SUCCESS, Entity);

                // Close the user control
                Close(false);

                ret = true;
            }

            return ret;
        }
        #endregion

        #region Validate Method
        public bool Validate()
        {
            bool ret = false;

            Entity.IsLoggedIn = false;
            //Store the current user logged
            AppLayer.AppSettings.Instance.CurrentUser = null;

            ValidationMessages.Clear();
            if (string.IsNullOrEmpty(Entity.UserName))
            {
                AddValidationMessage("UserName", "User Name Must Be Filled In");
            }
            if (string.IsNullOrEmpty(Entity.Password))
            {
                AddValidationMessage("Password", "Password Must Be Filled In");
            }

            ret = (ValidationMessages.Count == 0);

            return ret;
        }
        #endregion

        #region ValidateCredentials Method
        public bool ValidateCredentials(bool loginWithWindows = false)
        {
            bool ret = false;
            try
            {
                string url = AppLayer.AppSettings.Instance.CurrentProject.GetParameterByName("SharePointSite").Value;
                if (loginWithWindows)
                {
                    ret = MembershipService.ValidateLogin(url);
                }
                else
                {
                    ret = MembershipService.ValidateLogin(url, Entity.UserName, Entity.Password);
                }

                if (!ret)
                {
                    AddValidationMessage("LoginFailed",
                                    "Invalid User Name and/or Password.");
                }

            }
            catch (Exception ex)
            {
                PublishException(ex);
                AddValidationMessage("ErrorLogin", ex.Message);
            }

            return ret;
        }
        #endregion

        #region Close Method
        public override void Close(bool wasCancelled = true)
        {
            if (wasCancelled)
            {
                //Store the current user logged
                AppLayer.AppSettings.Instance.CurrentUser = null;

                // Display Informational Message
                MessageBroker.Instance.SendMessage(
                    MessageBrokerMessages.DISPLAY_TIMEOUT_INFO_MESSAGE_TITLE,
                       "User NOT Logged In.");
            }

            base.Close(wasCancelled);
        }
        #endregion

    }
}
