﻿using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for PendingPaymentItems.xaml
    /// </summary>
    public partial class PendingPaymentItemsControl : UserControl
    {
        public PendingPaymentItemsControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (PendingPaymentItemsViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        // View model class
        private PendingPaymentItemsViewModel _viewModel = null;


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadItems();
        }

        private void BtnImportSelectedItems_Click(object sender, RoutedEventArgs e)
        {
            UserControl userControl = new ImportPaymentItemsControl();
            Function function = _viewModel.Manager.GetFunction(userControl.GetType().FullName);
            string _title = function.Name;
            Window window = new Window
            {
                Title = _title,
                Content = userControl,
                Height = function.ScreenHeight,
                Width = function.ScreenWidth
            };
            window.ShowInTaskbar = function.IsShowInTaskbar;
            window.ResizeMode = ResizeMode.NoResize;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            _viewModel.Close();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Deseja realmente excluir o item selecionado?",
                "Excluir item selecionado?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    // Set selected item
                    BindingViewModel<PaymentItem> bindingViewModel = (BindingViewModel<PaymentItem>)((Button)sender).Tag;
                    _viewModel.Entity = bindingViewModel.Entity;
                    _viewModel.DeactivateItem();
                    _viewModel.LoadItems();
                }
                catch (Exception ex)
                {
                    
                }
            }
        }

        private void BtnPaySelectedItems_Click(object sender, RoutedEventArgs e)
        {
            // Ask if the user wants to pay the selected items
            if (MessageBox.Show("Deseja realmente pagar itens selecionados?",
                "Pagar items selecionados?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                List<PaymentItem> itemsSelected = ViewModelHelper<PaymentItem>.GetSelectedItems((ObservableCollection<BindingViewModel<PaymentItem>>)dtgPendingItems.ItemsSource);
                if (itemsSelected == null || itemsSelected.Count == 0)
                {
                    MessageBox.Show("Nenhum item selecionado!");
                    return;
                }

                _viewModel.PaySelectedItems(itemsSelected);
                _viewModel.LoadItems();
            }
        }

        private void ChkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<BindingViewModel<PaymentItem>> items = (ObservableCollection<BindingViewModel<PaymentItem>>)dtgPendingItems.ItemsSource;
            foreach (var item in items)
            {
                item.IsSelected = (bool)((CheckBox)e.Source).IsChecked;
            }
        }

        private void BtnDeleteSelectedItems_Click(object sender, RoutedEventArgs e)
        {
            // Ask if the user wants to pay the selected items
            if (MessageBox.Show("Deseja realmente excluir itens selecionados?",
                "Excluir items selecionados?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                List<PaymentItem> itemsSelected = ViewModelHelper<PaymentItem>.GetSelectedItems((ObservableCollection<BindingViewModel<PaymentItem>>)dtgPendingItems.ItemsSource);
                _viewModel.DeactivateItems(itemsSelected);
                _viewModel.LoadItems();
            }
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadItems();
        }

        private void ChkSelectItem_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            BindingViewModel<PaymentItem> bindingViewModel = (BindingViewModel<PaymentItem>)chk.Tag;
            bindingViewModel.IsSelected = (bool)chk.IsChecked;
        }
    }
}
