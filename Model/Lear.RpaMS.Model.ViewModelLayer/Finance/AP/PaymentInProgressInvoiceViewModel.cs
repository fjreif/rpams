﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Document;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.RPA;
using Lear.RpaMS.UI.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class PaymentInProgressInvoiceViewModel : ViewModelBase
    {
        private ViewModelManager manager = null;
        private Project automationProject = null;
        private Function function = null;
        private InvoiceDocument _Entity = new InvoiceDocument();
        private ObservableCollection<TrackingNumberNode> _TreeNode = new ObservableCollection<TrackingNumberNode>();
        private List<TrackingNumberNode> _ListTrackingNumberNode;
        private bool _IsEntitySelected = false;

        public PaymentInProgressInvoiceViewModel() : base()
        {
            manager = new ViewModelManager();
        }

        public InvoiceDocument Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }

        public bool IsEntitySelected
        {
            get { return _IsEntitySelected; }
            set
            {
                _IsEntitySelected = value;
                RaisePropertyChanged("IsEntitySelected");
            }
        }

        public ObservableCollection<TrackingNumberNode> TreeNode
        {
            get { return _TreeNode; }
            set
            {
                _TreeNode = value;
                RaisePropertyChanged("TreeNode");
            }
        }

        public override void Init()
        {
            function = manager.CheckAndAddFunction(this.LinkedComponent);
            DisplayStatusMessage(function.Name);
            function.Parameters = FunctionService.LoadParameters(function.Id);
        }

        public virtual void LoadItems()
        {
            try
            {
                ValidationMessages.Clear();
                this.LoadProject();
                TreeViewInvoiceData data = new TreeViewInvoiceData();
                _ListTrackingNumberNode = data.GetTreeViewNode(automationProject.Id);
                TreeNode = new ObservableCollection<TrackingNumberNode>(_ListTrackingNumberNode);
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorLoadItems", ex.Message);
            }
        }

        public void SearchTrackingNumber(string search)
        {

            List<TrackingNumberNode> tempList = _ListTrackingNumberNode.FindAll(node => node.TrackingNumber.ToLower().Contains(search.ToLower()));
            if (tempList != null)
            {
                TreeNode = new ObservableCollection<TrackingNumberNode>(tempList);
            }
        }

        public void ClearSearch()
        {
            TreeNode = new ObservableCollection<TrackingNumberNode>(_ListTrackingNumberNode);
        }

        public void UpdateInvoice()
        {
            try
            {
                if (_Entity != null)
                {
                    if (_Entity.DocumentPageDataID != null)
                    {
                        DocumentService.UpdateDocumentPageData(
                            Convert.ToInt32(_Entity.DocumentPageDataID),
                            DocumentValueTypeListCode.InvoiceNumber,
                            _Entity.Value);
                    } else
                    {
                        // Insert new invoice number
                        DocumentPage page = new DocumentPage();
                        page.Id = _Entity.DocumentPageID;
                        Entity.DocumentPageDataID = DocumentService.InsertDocumentPageData(
                            page,
                            DocumentValueTypeListCode.InvoiceNumber,
                            _Entity.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorUpdate", ex.Message);
            }
        }

        public void ShowInvoice()
        {
            Process p = new Process();
            p.StartInfo.FileName = Entity.DocumentPageFullName;
            p.Start();
        }

        private void LoadProject()
        {
            if (automationProject == null)
            {
                FunctionParameter parameter = function.GetParameterByName("AutomationProjectCode");
                automationProject = ProjectService.LoadProjectByCode(parameter.Value);
            }            
        }
        

    }
}
