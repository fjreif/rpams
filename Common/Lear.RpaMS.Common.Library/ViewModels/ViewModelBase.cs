﻿using System;
using System.Collections.ObjectModel;

namespace Lear.RpaMS.Common.Library
{
    public class ViewModelBase : CommonBase
    {

        #region Private Variables
        private ObservableCollection<ValidationMessage> _ValidationMessages = new ObservableCollection<ValidationMessage>();
        private bool _IsValidationVisible = false;
        private string _linkedComponent = string.Empty;
        #endregion

        #region Public Properties
        public ObservableCollection<ValidationMessage> ValidationMessages
        {
            get { return _ValidationMessages; }
            set
            {
                _ValidationMessages = value;
                RaisePropertyChanged("ValidationMessages");
            }
        }

        public bool IsValidationVisible
        {
            get { return _IsValidationVisible; }
            set
            {
                _IsValidationVisible = value;
                RaisePropertyChanged("IsValidationVisible");
            }
        }

        public string LinkedComponent
        {
            get { return _linkedComponent; }
            set
            {
                _linkedComponent = value;
                RaisePropertyChanged("LinkedComponent");
            }
        }
        #endregion

        #region AddBusinessRuleMessage Method
        public virtual void AddValidationMessage(string propertyName, string msg)
        {
            _ValidationMessages.Add(new ValidationMessage { Message = msg, PropertyName = propertyName });
            IsValidationVisible = true;
        }
        #endregion

        #region Clear Method
        public virtual void Clear()
        {
            ValidationMessages.Clear();
            IsValidationVisible = false;
        }
        #endregion

        #region DisplayStatusMessage Method
        public virtual void DisplayStatusMessage(string msg = "")
        {
            MessageBroker.Instance.SendMessage(MessageBrokerMessages.DISPLAY_STATUS_MESSAGE, msg);
        }
        #endregion

        #region PublishException Method
        public void PublishException(Exception ex)
        {
            // Publish Exception
            ExceptionManager.Instance.Publish(ex);
        }
        #endregion

        #region Close Method
        public virtual void Close(bool wasCancelled = true)
        {
            MessageBroker.Instance.SendMessage(MessageBrokerMessages.CLOSE_USER_CONTROL, wasCancelled);
        }
        #endregion


        public virtual void Init() { }


        #region Dispose Method
        public virtual void Dispose()
        {
        }
        #endregion


    }
}
