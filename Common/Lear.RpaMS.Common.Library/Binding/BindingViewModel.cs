﻿namespace Lear.RpaMS.Common.Library.Binding
{
    /// <summary>
    /// Generic class to binding with UI Components
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BindingViewModel<T> : CommonBase
    {
        private T _Entity;
        private bool _isSelected;

        public T Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        public BindingViewModel()
        {
            this.IsSelected = false;
        }

    }
}
