﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Membership;
using Lear.RpaMS.Model.ViewModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Lear.RpaMS.UI.WPF
{
    public partial class MainWindow
    {

        private void CreateUserMenu()
        {
            try
            {
                // Fixed menu items are positions 0 = File, 1 = User, 2 = Login
                List<UserMenuItem> menuItems = _viewModel.LoadMenuItem();
                if (menuItems != null && menuItems.Count > 0)
                {
                    Binding binding = new Binding("UserEntity.IsLoggedIn");
                    binding.Source = _viewModel;
                    binding.Mode = BindingMode.OneWay;

                    foreach (UserMenuItem userMenuItem in menuItems)
                    {
                        MenuItem itemMenu = new MenuItem();
                        itemMenu.Name = userMenuItem.GetCode();
                        itemMenu.Header = userMenuItem.Name;
                        itemMenu.SetBinding(MenuItem.IsEnabledProperty, binding);

                        if (userMenuItem.FunctionID == null && userMenuItem.ParentMenuItemID == null)
                        {
                            if (!this.MainMenuContainsMenuItem(itemMenu))
                            {
                                MainMenu.Items.Insert(MainMenu.Items.Count - 1, itemMenu);
                            }
                        }
                        else if (userMenuItem.ParentMenuItemID != null)
                        {
                            MenuItem menuItemAux = new MenuItem();
                            menuItemAux.Name = "_" + userMenuItem.ParentMenuItemID;

                            MenuItem parentMenuItem = this.MainMenuGetMenuItem(menuItemAux);
                            if (parentMenuItem != null)
                            {
                                if (userMenuItem.FunctionID != null)
                                {
                                    itemMenu.Tag = userMenuItem.Component;
                                    itemMenu.Click += MenuItem_Click;
                                }

                                if (!this.MenuItemContainsMenuItem(parentMenuItem, itemMenu))
                                {
                                    parentMenuItem.Items.Add(itemMenu);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Create User Menu. Error Message: " + ex.Message);
            }
        }

        private void DestroyUserMenu()
        {
            if (MainMenu.Items.Count > 3)
            {
                int endIndex = MainMenu.Items.Count - 1;
                for (int i = 2; i < endIndex; i++)
                {
                    MenuItem menuItem = MainMenu.Items[i] as MenuItem;
                    MainMenu.Items.Remove(menuItem);
                }
            }
        }

        private bool MainMenuContainsMenuItem(MenuItem itemMenu)
        {
            foreach (MenuItem item in MainMenu.Items)
            {
                if (item.Name == itemMenu.Name)
                {
                    return true;
                }
                if (item.HasItems)
                {
                    MenuItemContainsMenuItem(item, itemMenu);
                }
            }

            return false;
        }

        private bool MenuItemContainsMenuItem(MenuItem itemTarget, MenuItem itemSearch)
        {
            foreach (MenuItem item in itemTarget.Items)
            {
                if (item.Name == itemSearch.Name)
                {
                    return true;
                }

                if (item.HasItems)
                {
                    MenuItemContainsMenuItem(item, itemSearch);
                }
            }

            return false;
        }

        private MenuItem MainMenuGetMenuItem(MenuItem itemMenu)
        {
            foreach (MenuItem item in MainMenu.Items)
            {
                if (item.Name == itemMenu.Name)
                {
                    return item;
                }
                if (item.HasItems)
                {
                    MenuItem menuItem = GetMenuItem(item, itemMenu);
                    if (menuItem != null)
                    {
                        return menuItem;
                    }
                }
            }

            return null;
        }

        private MenuItem GetMenuItem(MenuItem itemTarget, MenuItem itemSearch)
        {
            foreach (MenuItem item in itemTarget.Items)
            {
                if (item.Name == itemSearch.Name)
                {
                    return item;
                }
                if (item.HasItems)
                {
                    MenuItem menuItem = GetMenuItem(item, itemSearch);
                    if (menuItem != null)
                    {
                        return menuItem;
                    }
                }
            }

            return null;
        }

    }
}
