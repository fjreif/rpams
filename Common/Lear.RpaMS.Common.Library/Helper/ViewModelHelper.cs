﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lear.RpaMS.Common.Library.Binding;
using System.Collections.ObjectModel;

namespace Lear.RpaMS.Common.Library.Helper
{
    public class ViewModelHelper<T>
    {

        public static List<BindingViewModel<T>> ModelToViewModel(List<T> origin)
        {
            List<BindingViewModel<T>> listItems = new List<BindingViewModel<T>>();
            foreach (var item in origin)
            {
                listItems.Add(new BindingViewModel<T>() { Entity = item });
            }
            return listItems;
        }

        public static List<T> GetSelectedItems(ObservableCollection<BindingViewModel<T>> bindingItems)
        {
            List<T> items = new List<T>();
            foreach (var item in bindingItems)
            {
                if (item.IsSelected)
                {
                    items.Add(item.Entity);
                }
            }            
            return items;
        }

        public static void ViewModelToModel()
        {

        }

    }
}
