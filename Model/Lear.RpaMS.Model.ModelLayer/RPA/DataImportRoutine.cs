﻿using Lear.RpaMS.Common.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ModelLayer.RPA
{
    public class DataImportRoutine : CommonBase
    {

        private int _Id;
        private string _Code;
        private string _Name;
        private string _Description;
        private bool _Active;

        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                RaisePropertyChanged("ID");
            }
        }

        public string Code
        {
            get { return _Code; }
            set
            {
                _Code = value;
                RaisePropertyChanged("Code");
            }
        }

        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                RaisePropertyChanged("Description");
            }
        }


        public bool Active
        {
            get { return _Active; }
            set
            {
                _Active = value;
                RaisePropertyChanged("Active");
            }
        }


    }
}
