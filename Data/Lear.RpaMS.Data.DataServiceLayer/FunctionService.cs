﻿using Lear.RpaMS.Data.DataLayer.Function;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataServiceLayer
{
    public class FunctionService
    {       

        public static List<FunctionParameter> LoadParameters(int functionId)
        {
            FunctionData data = new FunctionData();
            return data.LoadParameters(functionId);
        }


    }
}
