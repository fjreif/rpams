﻿using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Model;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataLayer.Function
{
    public class FunctionData
    {
                
        private static string TABLE_NAME = "RPA_Function";


        public Model.ModelLayer.Function LoadFunctionById(int Id)
        {
            SqlCommand command = null;
            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.SELECT);


            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return null;
        }

        public Model.ModelLayer.Function LoadFunctionByComponent(string componentName)
        {
            SqlCommand command = null;
            Model.ModelLayer.Function function = null;

            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.SELECT);
                query.AddFields("ID, Component, IsNewWindow, ScreenHeight, ScreenWidth, IsShowInTaskbar, Active");
                string fieldName = query.GetFieldWithTranslation("Name");
                //query.AddFieldWithTranslation("Name");
                query.AddField(fieldName);
                query.AddTable(TABLE_NAME);
                query.AddWhereConditions("Component = @Component");

                command = DataConnectionBase.GetCommand();
                command.CommandText = query.GetQuery();
                command.Parameters.Add("@Component", System.Data.SqlDbType.VarChar);
                command.Parameters["@Component"].Value = componentName;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        function = new Model.ModelLayer.Function();                        
                        function.FunctionId = Convert.ToInt32(reader["ID"]);
                        function.Name = reader[fieldName].ToString();
                        function.Component = reader["Component"].ToString();
                        function.IsNewWindow = Convert.ToBoolean(reader["IsNewWindow"]);
                        function.ScreenHeight = Convert.ToInt32(SqlHelper.GetValue(reader["ScreenHeight"], 0));
                        function.ScreenWidth = Convert.ToInt32(SqlHelper.GetValue(reader["ScreenWidth"], 0));
                        function.IsShowInTaskbar = Convert.ToBoolean(SqlHelper.GetValue(reader["IsShowInTaskbar"], true));
                        function.Active = Convert.ToBoolean(reader["Active"]);
                    }
                }
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return function;
        }

        public List<FunctionParameter> LoadParameters(int functionId)
        {
            SqlCommand command = null;
            List<FunctionParameter> parameters = null;

            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.SELECT);
                query.AddFields("ID, FunctionID, Name, Value, DateTimeInsert, UserCodeInsert, DateTimeUpdate, UserCodeUpdate, Active");
                query.AddTable("RPA_FunctionParameter");
                query.AddWhereConditions("FunctionID = @FunctionID");

                command = DataConnectionBase.GetCommand();
                command.CommandText = query.GetQuery();
                command.Parameters.Add("@FunctionID", System.Data.SqlDbType.Int);
                command.Parameters["@FunctionID"].Value = functionId;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    parameters = new List<FunctionParameter>();
                    while (reader.Read())
                    {
                        FunctionParameter parameter = new FunctionParameter();
                        parameter.Id = Convert.ToInt32(reader["ID"]);
                        parameter.FunctionId = Convert.ToInt32(reader["FunctionID"]);
                        parameter.Name = reader["Name"].ToString();
                        parameter.Value = reader["Value"].ToString();
                        parameter.Active = Convert.ToBoolean(reader["Active"]);
                        parameters.Add(parameter);
                    }
                }
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return parameters;
        }


    }
}
