﻿using Lear.RpaMS.Data.DataLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataServiceLayer.Finance.AP
{
    public class PaymentItemService
    {

        public static List<PaymentItem> LoadPendingPaymentItems(string companyCode)
        {
            PaymentItemData data = new PaymentItemData();
            return data.LoadPendingPaymentItems(companyCode);
        }

        public static List<PaymentItem> LoadPaymentInProgressItems(string companyCode)
        {
            PaymentItemData data = new PaymentItemData();
            return data.LoadPaymentInProgressItems(companyCode);
        }


    }
}
