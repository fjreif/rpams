﻿using Lear.RpaMS.Business.Finance.AP;
using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class PendingPaymentItemsViewModel : ViewModelBase
    {
        private ObservableCollection<BindingViewModel<PaymentItem>> _BindingItems = new ObservableCollection<BindingViewModel<PaymentItem>>();
        private ViewModelManager manager = null;
        private Function function = null;
        private PaymentItem _Entity = new PaymentItem();
        private int _importRoutineID = 0;

        #region Constructor
        public PendingPaymentItemsViewModel() : base()
        {
            manager = new ViewModelManager();            
        }
        #endregion

        #region Public Properties
        public ObservableCollection<BindingViewModel<PaymentItem>> BindingItems
        {
            get { return _BindingItems; }
            set
            {
                _BindingItems = value;
                RaisePropertyChanged("BindingItems");
            }
        }

        public ViewModelManager Manager
        {
            get { return manager;  }
        }

        public PaymentItem Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }

        #endregion

        public override void Init()
        {
            function = manager.CheckAndAddFunction(this.LinkedComponent);
            DisplayStatusMessage(function.Name);
            function.Parameters = FunctionService.LoadParameters(function.Id);
        }

        public virtual void LoadItems()
        {
            try
            {
                ValidationMessages.Clear();
                List<PaymentItem> _items = PaymentItemService.LoadPendingPaymentItems(AppLayer.AppSettings.Instance.CurrentCompany.CompanyCode);
                List<BindingViewModel<PaymentItem>> _paymentItems = ViewModelHelper<PaymentItem>.ModelToViewModel(_items);
                BindingItems = new ObservableCollection<BindingViewModel<PaymentItem>>(_paymentItems);
                foreach (var item in BindingItems)
                {
                    item.RowNumber = BindingItems.IndexOf(item) + 1;
                }
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorLoadItems", ex.Message + " - " + ex.StackTrace);
            }
        }


        public virtual void PaySelectedItems(List<PaymentItem> selectedItems)
        {
            ValidationMessages.Clear();

            try
            {
                string dataRoutineCode = function.GetParameterByName("DATA_IMPORT_ROUTINE_CODE").Value;
                if (_importRoutineID == 0)
                {
                    DataImportRoutine importRoutine = DataImportRoutineService.LoadRoutineByCode(dataRoutineCode);
                    if (importRoutine == null)
                    {
                        throw new Exception($"Routine Code '{dataRoutineCode}' not found in Table RPA_DataImportRoutine.");
                    }
                    _importRoutineID = importRoutine.Id;
                }

                int executionID = DataImportExecutionService.StartExecution(_importRoutineID, "");

                try
                {
                    ImportPaymentInProgressBusiness importBusiness = new ImportPaymentInProgressBusiness();
                    string ret = importBusiness.ImportPaymentInProgress(
                        selectedItems, 
                        _importRoutineID, 
                        executionID);
                    if (!String.IsNullOrEmpty(ret))
                    {
                        AddValidationMessage("ErrorImportPaySelectedItems", ret);
                        DataImportExecutionService.EndExecution(executionID, StatusExecution.ERROR, ret);
                    }
                    else
                    {
                        DataImportExecutionService.EndExecution(executionID, StatusExecution.SUCCESS, "");
                    }

                }
                catch (Exception ex)
                {
                    ValidationMessages.Clear();
                    AddValidationMessage("ErrorImportPaySelectedItems", ex.Message);
                    DataImportExecutionService.EndExecution(executionID, StatusExecution.ERROR, ex.Message + " - " + ex.StackTrace);
                }


            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorImportPaySelectedItems", ex.Message + " - " + ex.StackTrace);
            }
        }

        public void DeactivateItem()
        {
            try
            {
                PaymentItemService.DeactivatePendingPaymentItem(Entity.Id);
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorDeactivateItem", ex.Message);
            }
        }

        public void DeactivateItems(List<PaymentItem> items)
        {
            try
            {
                List<int> ids = new List<int>();
                foreach (var item in items)
                {
                    ids.Add(item.Id);
                }
                PaymentItemService.DeactivatePendingPaymentItems(ids);
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorDeactivateItems", ex.Message);
            }
        }

    }
}
