﻿using Lear.RpaMS.AppLayer;
using Lear.RpaMS.Data.DataLayer.Function;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer
{
    public class ViewModelManager
    {

        public Function LoadFunction(string componentName)
        {
            FunctionData data = new FunctionData();
            return data.LoadFunctionByComponent(componentName);
        }

        public Function CheckAndAddFunction(string componentName)
        {
            Function function = this.GetFunction(componentName);
            if (function == null)
            {
                function = LoadFunction(componentName);
                this.Addfunction(function);
            }
            return function;
        }

        public void Addfunction(Function function)
        {
            FunctionManager.Instance.AddFunction(function);
        }

        public Function GetFunction(string componentName)
        {
            return FunctionManager.Instance.GetFunction(componentName);
        }

        public List<Function> GetAllFunction()
        {
            return FunctionManager.Instance.GetAllFunction();
        }


    }
}
