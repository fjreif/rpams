﻿using System;
using System.Configuration;

namespace Lear.RpaMS.Common.Library
{
    public class ConfigurationSettings : CommonBase
    {

        #region LoadSettings Method
        public virtual void LoadSettings()
        {
            // TODO: Load any common application settings here
        }
        #endregion

        #region GetSetting Method
        protected T GetSetting<T>(string key, object defaultValue)
        {
            T ret = default(T);
            string value;

            value = ConfigurationManager.AppSettings[key];            
            if (string.IsNullOrEmpty(value))
            {
                ret = (T)defaultValue;
            }
            else
            {
                ret = (T)Convert.ChangeType(value, typeof(T));
            }

            return ret;
        }
        #endregion

        #region GetSetting Method
        protected T GetConnectionString<T>(string key, object defaultValue)
        {
            T ret = default(T);
            string value;

            value = ConfigurationManager.ConnectionStrings[key].ConnectionString;
            if (string.IsNullOrEmpty(value))
            {
                ret = (T)defaultValue;
            }
            else
            {
                ret = (T)Convert.ChangeType(value, typeof(T));
            }

            return ret;
        }
        #endregion

    }

}
