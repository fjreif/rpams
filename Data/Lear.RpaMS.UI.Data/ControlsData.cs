﻿using Lear.RpaMS.Data.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.UI.Data
{
    public class ControlsData
    {

        public DataTable LoadDatatable(string tableName)
        {
            SqlCommand command = null;
            DataTable data = null;
            try
            {
                string query = $"SELECT * FROM {tableName}";                
                command = DataConnectionBase.GetCommand();
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                command.Connection.Open();
                data = new DataTable();
                data.Load(command.ExecuteReader());
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }
            return data;
        }

    }
}
