﻿using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Business.Finance.AP
{
    public class ImportPaymentInProgressBusiness
    {

        private int _dataImportRoutineID;
        private int _dataExecutionID;

        public string ImportPaymentInProgress(
            List<PaymentItem> items,
            int dataImportRoutineID,
            int dataExecutionID)
        {
            string ret = string.Empty;
            _dataImportRoutineID = dataImportRoutineID;
            _dataExecutionID = dataExecutionID;

            try
            {
                DataTable data = this.GetTableImport();
                this.FillDataTable(data, items);
                if (data != null && data.Rows.Count > 0)
                {
                    this.WriteToDatabase(data);
                }
            }
            catch (Exception ex)
            {
                ret = "Error: " + ex.Message + " - " + ex.StackTrace;
            }
            return ret;
        }

        private void FillDataTable(DataTable data, List<PaymentItem> items)
        {
            foreach (PaymentItem item in items)
            {
                DataRow dataRow = data.NewRow();
                dataRow["Code"] = item.Company + (string.IsNullOrEmpty(item.PlantCode) ? "0" : item.PlantCode) + item.VendorCode + item.ProvisionDoc + item.ReferenceImportation;
                dataRow["Company"] = item.Company;
                dataRow["PlantCode"] = item.PlantCode;
                dataRow["PlantName"] = item.PlantName;
                dataRow["VendorCode"] = item.VendorCode;
                dataRow["VendorName"] = item.VendorName;
                dataRow["DocumentProvision"] = item.ProvisionDoc;
                dataRow["ReferenceImportation"] = item.ReferenceImportation;
                dataRow["ShipmentNumber"] = item.ShipmentNumber;
                dataRow["DocumentCurrency"] = item.Currency;
                dataRow["ImportDocumentValue"] = item.Value;
                dataRow["ShipmentSeqNumber"] = item.ShipmentSeqNumber;
                dataRow["ImportReference"] = item.ReferenceImportation;
                dataRow["ExpirationDate"] = item.ExpirationDate;
                dataRow["ShipmentStatus"] = item.ShipmentStatus;
                dataRow["Account"] = item.Account;
                dataRow["InvoiceReference"] = item.InvoiceReference;
                dataRow["Assignment"] = item.Assignment;
                dataRow["InvoiceDocumentNumber"] = item.InvoiceDocumentNumber;
                dataRow["InvoiceDocumentType"] = item.InvoiceDocumentType;
                dataRow["InvoicePostingDate"] = item.InvoicePostingDate;
                dataRow["InvoiceDocumentDate"] = item.InvoiceDocumentDate;
                dataRow["DelayAfterNetDueDate"] = item.DelayAfterNetDueDate;
                dataRow["InvoiceNetDueDate"] = item.InvoiceNetDueDate;
                dataRow["InvoiceDocumentCurrency"] = item.InvoiceDocumentCurrency;
                dataRow["InvoiceAmountCurrency"] = item.InvoiceAmountCurrency;
                dataRow["InvoiceAmountLocalCurrency"] = item.InvoiceAmountLocalCurrency;
                dataRow["InvoiceText"] = item.InvoiceText;
                dataRow["BusinessPlaceCode"] = item.BusinessPlaceCode;
                dataRow["UserCode"] = AppLayer.AppSettings.Instance.CurrentUser.UserName;
                dataRow["MachineName"] = AppLayer.AppSettings.Instance.CurrentMachineName;
                dataRow["DataImportRoutineID"] = _dataImportRoutineID;
                dataRow["DataImportExecutionID"] = _dataExecutionID;
                data.Rows.Add(dataRow);
            }
        }


        private DataTable GetTableImport()
        {
            DataTable data = new DataTable();
            data.Columns.Add("Code", typeof(string));
            data.Columns.Add("Company", typeof(int));
            data.Columns.Add("PlantCode", typeof(string));
            data.Columns.Add("PlantName", typeof(string));
            data.Columns.Add("VendorCode", typeof(string));
            data.Columns.Add("VendorName", typeof(string));
            data.Columns.Add("DocumentProvision", typeof(string));
            data.Columns.Add("ReferenceImportation", typeof(string));
            data.Columns.Add("ShipmentNumber", typeof(string));
            data.Columns.Add("DocumentCurrency", typeof(string));
            data.Columns.Add("ImportDocumentValue", typeof(decimal));
            data.Columns.Add("ShipmentSeqNumber", typeof(string));
            data.Columns.Add("ImportReference", typeof(string));
            data.Columns.Add("ExpirationDate", typeof(DateTime));
            data.Columns.Add("ShipmentStatus", typeof(string));
            data.Columns.Add("Account", typeof(string));
            data.Columns.Add("InvoiceReference", typeof(string));
            data.Columns.Add("Assignment", typeof(string));
            data.Columns.Add("InvoiceDocumentNumber", typeof(string));
            data.Columns.Add("InvoiceDocumentType", typeof(string));
            data.Columns.Add("InvoicePostingDate", typeof(DateTime));
            data.Columns.Add("InvoiceDocumentDate", typeof(DateTime));
            data.Columns.Add("DelayAfterNetDueDate", typeof(int));
            data.Columns.Add("InvoiceNetDueDate", typeof(DateTime));
            data.Columns.Add("InvoiceDocumentCurrency", typeof(string));
            data.Columns.Add("InvoiceAmountCurrency", typeof(decimal));
            data.Columns.Add("InvoiceAmountLocalCurrency", typeof(decimal));
            data.Columns.Add("InvoiceText", typeof(string));
            data.Columns.Add("BusinessPlaceCode", typeof(string));
            data.Columns.Add("UserCode", typeof(string));
            data.Columns.Add("MachineName", typeof(string));
            data.Columns.Add("DataImportRoutineID", typeof(int));
            data.Columns.Add("DataImportExecutionID", typeof(int));

            return data;
        }

        private void WriteToDatabase(DataTable data)
        {
            ImportPaymentItemService.ImportPaymentInProgressData(data, _dataExecutionID);
        }


    }
}
