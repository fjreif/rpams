﻿using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ViewModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Lear.RpaMS.UI.WPF.Automation
{
    public class WindowManager
    {

        public static void CreateNewWindow(UserControl userControl)
        {
            ViewModelManager manager = new ViewModelManager();
            Function function = manager.CheckAndAddFunction(userControl.GetType().FullName);
            string _title = function.Name;
            Window window = new Window
            {
                Title = _title,
                Content = userControl,
                Height = function.ScreenHeight,
                Width = function.ScreenWidth
            };
            window.ShowInTaskbar = function.IsShowInTaskbar;
            if (!function.IsResizable)
            {
                window.ResizeMode = ResizeMode.NoResize;
            }
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();           

            
        }

    }
}
