﻿using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for ConfigPaymentDocumentControl.xaml
    /// </summary>
    public partial class ConfigPaymentDocumentControl : UserControl
    {
        //Window
        private Window window;
        // View model class
        private ConfigPaymentDocumentViewModel _viewModel = null;

        public ConfigPaymentDocumentControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (ConfigPaymentDocumentViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();

            this.Init();
        }

        private void Init()
        {
            this.EnableFieldsDocment6401(_viewModel.EnableDocument6401);
        }

        private void chkEnableDocument6401_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            this.EnableFieldsDocment6401((bool)chk.IsChecked);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (_viewModel.SaveConfig())
            {
                window.Close();
            }
        }

        private void EnableFieldsDocment6401(bool isEnabled)
        {
            txtDtDocumentFrom.IsEnabled = isEnabled;
            txtDtDocumentTo.IsEnabled = isEnabled;
            txtDocument6401Location.IsEnabled = isEnabled;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            _viewModel.Close();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            window = Window.GetWindow(this);
        }
    }
}
