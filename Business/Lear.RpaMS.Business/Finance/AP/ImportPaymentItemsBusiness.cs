﻿using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Linq;
using System.Data;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;

namespace Lear.RpaMS.Business.Finance.AP
{
    public class ImportPaymentItemsBusiness
    {
        private string _fileNameImportsToExpire;
        private string _fileNameVendorLineItems;
        private Company _company;
        private User _user;
        private string _shipmentStatusOpenParam;
        private int _dataImportRoutineID;
        private int _dataExecutionID;
        private DataTable _dtImportsToExpire;
        private DataTable _dtVendorLineItems;

        //Columns names
        private static string PWS_SUPPLIER_CODE = "Supplier code";
        private static string PWS_SUPPLIER_NAME = "SUPPLIER";
        private static string PWS_PROVISION_DOC = "DOC. OF PROVISIO";
        private static string PWS_REFERENCE_IMPORTATION = "Reference of Importation";
        private static string PWS_SHIPMENT_NUMBER = "Shipment Numb.";
        private static string PWS_CURRENCY = "CURR.";
        private static string PWS_VALUE = "ME VALUE";
        private static string PWS_SHIPMENT_SEQ_NUMBER = "Shipment Seq.Number";
        private static string PWS_INTERNAL_REFERENCE = "Internal reference";
        private static string PWS_EXPIRATION_DATE = "EXPIRATION DATE";
        private static string PWS_SHIPMENT_STATUS = "Shipment Status";
        private static string PWS_DECLARATION_TYPE = "Declaration Type";

        private static string FBL1N_ACCOUNT = "Account";
        private static string FBL1N_REFERENCE = "Reference";
        private static string FBL1N_ASSIGNMENT = "Assignment";
        private static string FBL1N_DOCUMENT_NUMBER = "Document Number";
        private static string FBL1N_DOCUMENT_TYPE = "Document Type";
        private static string FBL1N_POSTING_DATE = "Posting Date";
        private static string FBL1N_DOCUMENT_DATE = "Document Date";
        private static string FBL1N_ARREARS_AFTER_NET_DUE_DATE = "Arrears after net due date";
        private static string FBL1N_NET_DUE_DATE = "Net due date";
        private static string FBL1N_DOCUMENT_CURRENCY = "Document currency";
        private static string FBL1N_AMOUNT_IN_DOC_CURRENCY = "Amount in doc. curr.";
        private static string FBL1N_LOCAL_CURRENCY = "Local Currency";
        private static string FBL1N_AMOUNT_IN_LOCAL_CURRENCY = "Amount in local currency";
        private static string FBL1N_TEXT = "Text";
        private static string FBL1N_BUSINESS_PLACE = "Business Place";

        //
        private static string SHIPMENT_STATUS_OPEN = "DS-DESEMBARAÇADO";


        /// <summary>
        /// Import data from Imports to Expire (PWS) and Vendor Line Items (SAP FBL1N)
        /// </summary>
        /// <param name="fileImportsToExpire"></param>
        /// <param name="fileVendorLineItems"></param>
        /// <returns></returns>
        public string ImportPendingPayments(string fileImportsToExpire,
            string fileVendorLineItems,
            Company company,
            User user,
            string shipmentStatusOpenParam,
            int dataImportRoutineID,
            int dataExecutionID)
        {
            string ret = string.Empty;
            
            _fileNameImportsToExpire = fileImportsToExpire;
            _fileNameVendorLineItems = fileVendorLineItems;
            _company = company;
            _user = user;
            _shipmentStatusOpenParam = shipmentStatusOpenParam;
            _dataImportRoutineID = dataImportRoutineID;
            _dataExecutionID = dataExecutionID;

            if (_company == null)
            {
                throw new ArgumentException("Error: Company cannot be null", "company");
            }

            try
            {
                this.ReadImportsToExpire();
                this.ReadVendorLineItems();
                DataTable filteredTable = this.FilterData();               

                if (filteredTable != null && filteredTable.Rows.Count > 0)
                {
                    this.WriteToDatabase(filteredTable);
                }

                

            }
            catch (Exception ex)
            {
                ret = "Error: " + ex.Message + " - " + ex.StackTrace;
            }
            return ret;
        }               

        private void ReadImportsToExpire()
        {
            if (_dtImportsToExpire == null || _dtImportsToExpire.Columns.Count == 0)
            {
                //Create the DataTable
                _dtImportsToExpire = new DataTable();
                _dtImportsToExpire.Columns.Add("ROW_NUMBER", typeof(int));
                _dtImportsToExpire.Columns.Add("PLANT_CODE", typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_SUPPLIER_CODE, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_SUPPLIER_NAME, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_PROVISION_DOC, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_REFERENCE_IMPORTATION, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_SHIPMENT_NUMBER, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_CURRENCY, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_VALUE, typeof(decimal));
                _dtImportsToExpire.Columns.Add(PWS_SHIPMENT_SEQ_NUMBER, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_INTERNAL_REFERENCE, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_EXPIRATION_DATE, typeof(DateTime));
                _dtImportsToExpire.Columns.Add(PWS_SHIPMENT_STATUS, typeof(string));
                _dtImportsToExpire.Columns.Add(PWS_DECLARATION_TYPE, typeof(string));
            }

            _dtImportsToExpire.Rows.Clear();

            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook excelBook = excelApp.Workbooks.Open(_fileNameImportsToExpire);
            Excel.Worksheet excelSheet = excelBook.Sheets[1];
            Excel.Range excelRange = excelSheet.UsedRange;

            try
            {
                int rows = excelRange.Rows.Count;
                int cols = excelRange.Columns.Count;

                Excel.Range range = excelSheet.Range[excelSheet.Cells[2, 1], excelSheet.Cells[rows, cols]];
                object[,] holder = range.Value2;

                try
                {
                    for (int i = 1; i <= rows - 1; i++)
                    {
                        if ((holder.Length / cols) < i)
                        {
                            break;
                        }

                        DataRow row = _dtImportsToExpire.NewRow();
                        row["ROW_NUMBER"] = i + 1;
                        row["PLANT_CODE"] = _company.CompanyCode;
                        row[PWS_SUPPLIER_CODE] = Convert.ToString(holder[i, 1]);
                        row[PWS_SUPPLIER_NAME] = Convert.ToString(holder[i, 2]);
                        row[PWS_PROVISION_DOC] = Convert.ToString(ExcelHelper.GetValue(holder[i, 3], string.Empty));
                        row[PWS_REFERENCE_IMPORTATION] = Convert.ToString(ExcelHelper.GetValue(holder[i, 4], string.Empty));
                        row[PWS_SHIPMENT_NUMBER] = Convert.ToString(ExcelHelper.GetValue(holder[i, 6], string.Empty));
                        row[PWS_CURRENCY] = Convert.ToString(ExcelHelper.GetValue(holder[i, 7], string.Empty));
                        row[PWS_VALUE] = Convert.ToDecimal(ExcelHelper.GetValue(holder[i, 8], 0));
                        row[PWS_SHIPMENT_SEQ_NUMBER] = Convert.ToString(ExcelHelper.GetValue(holder[i, 9], string.Empty));
                        row[PWS_INTERNAL_REFERENCE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 10], string.Empty));
                        row[PWS_EXPIRATION_DATE] = ExcelHelper.GetDateTime(holder[i, 11]);
                        row[PWS_SHIPMENT_STATUS] = Convert.ToString(ExcelHelper.GetValue(holder[i, 12], string.Empty));
                        row[PWS_DECLARATION_TYPE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 13], string.Empty));
                        _dtImportsToExpire.Rows.Add(row);
                    }
                }
                catch (Exception)
                {

                    throw;
                }              

            }
            finally
            {
                //release com objects to fully kill excel process from running in the background
                if (excelRange != null)
                {
                    Marshal.ReleaseComObject(excelRange);
                }
                if (excelSheet != null)
                {
                    Marshal.ReleaseComObject(excelSheet);
                }

                if (excelBook != null)
                {
                    //close and release
                    excelBook.Close();
                    Marshal.ReleaseComObject(excelBook);
                }

                if (excelApp != null)
                {
                    //quit and release
                    excelApp.Quit();
                    Marshal.ReleaseComObject(excelApp);
                }
            }

        }

               

        private void ReadVendorLineItems()
        {
            if (_dtVendorLineItems == null || _dtVendorLineItems.Columns.Count == 0)
            {
                //Create the DataTable
                _dtVendorLineItems = new DataTable();
                _dtVendorLineItems.Columns.Add("ROW_NUMBER", typeof(int));
                _dtVendorLineItems.Columns.Add("PLANT_CODE", typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_ACCOUNT, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_REFERENCE, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_ASSIGNMENT, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_DOCUMENT_NUMBER, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_DOCUMENT_TYPE, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_POSTING_DATE, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_DOCUMENT_DATE, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_ARREARS_AFTER_NET_DUE_DATE, typeof(int));
                _dtVendorLineItems.Columns.Add(FBL1N_NET_DUE_DATE, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_DOCUMENT_CURRENCY, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_AMOUNT_IN_DOC_CURRENCY, typeof(decimal));
                _dtVendorLineItems.Columns.Add(FBL1N_LOCAL_CURRENCY, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_AMOUNT_IN_LOCAL_CURRENCY, typeof(decimal));
                _dtVendorLineItems.Columns.Add(FBL1N_TEXT, typeof(string));
                _dtVendorLineItems.Columns.Add(FBL1N_BUSINESS_PLACE, typeof(string));
            }

            _dtVendorLineItems.Rows.Clear();

            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook excelBook = excelApp.Workbooks.Open(_fileNameVendorLineItems);
            Excel.Worksheet excelSheet = excelBook.Sheets[1];
            Excel.Range excelRange = excelSheet.UsedRange;

            try
            {
                int rows = excelRange.Rows.Count;
                int cols = excelRange.Columns.Count;

                Excel.Range range = excelSheet.Range[excelSheet.Cells[2, 1], excelSheet.Cells[rows, cols]];
                object[,] holder = range.Value2;

                for (int i = 1; i <= rows; i++)
                {
                    if ((holder.Length  /cols ) < i)
                    {
                        break;
                    }

                    DataRow row = _dtVendorLineItems.NewRow();
                    row["ROW_NUMBER"] = i + 1;
                    row["PLANT_CODE"] = _company.CompanyCode;
                    string account = Convert.ToString(ExcelHelper.GetValue(holder[i, 1], string.Empty));
                    if (String.IsNullOrEmpty(account))
                    {
                        continue;
                    }
                    row[FBL1N_ACCOUNT] = account;
                    row[FBL1N_REFERENCE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 3], string.Empty));
                    row[FBL1N_ASSIGNMENT] = Convert.ToString(ExcelHelper.GetValue(holder[i, 4], string.Empty));
                    row[FBL1N_DOCUMENT_NUMBER] = Convert.ToString(ExcelHelper.GetValue(holder[i, 5], string.Empty));
                    row[FBL1N_DOCUMENT_TYPE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 6], string.Empty));
                    row[FBL1N_POSTING_DATE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 7], string.Empty));
                    row[FBL1N_DOCUMENT_DATE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 8], string.Empty));
                    row[FBL1N_ARREARS_AFTER_NET_DUE_DATE] = Convert.ToInt32(ExcelHelper.GetValue(holder[i, 9], 0));
                    row[FBL1N_NET_DUE_DATE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 10], string.Empty));
                    row[FBL1N_DOCUMENT_CURRENCY] = Convert.ToString(ExcelHelper.GetValue(holder[i, 11], string.Empty));
                    row[FBL1N_AMOUNT_IN_DOC_CURRENCY] = Convert.ToDecimal(ExcelHelper.GetValue(holder[i, 12], 0));
                    row[FBL1N_LOCAL_CURRENCY] = Convert.ToString(ExcelHelper.GetValue(holder[i, 13], string.Empty));
                    row[FBL1N_AMOUNT_IN_LOCAL_CURRENCY] = Convert.ToDecimal(ExcelHelper.GetValue(holder[i, 14], 0));
                    row[FBL1N_TEXT] = Convert.ToString(ExcelHelper.GetValue(holder[i, 15], string.Empty));
                    row[FBL1N_BUSINESS_PLACE] = Convert.ToString(ExcelHelper.GetValue(holder[i, 16], string.Empty));

                    _dtVendorLineItems.Rows.Add(row);
                }
            }
            finally
            {
                //release com objects to fully kill excel process from running in the background
                if (excelRange != null)
                {
                    Marshal.ReleaseComObject(excelRange);
                }
                if (excelSheet != null)
                {
                    Marshal.ReleaseComObject(excelSheet);
                }

                if (excelBook != null)
                {
                    //close and release
                    excelBook.Close();
                    Marshal.ReleaseComObject(excelBook);
                }

                if (excelApp != null)
                {
                    //quit and release
                    excelApp.Quit();
                    Marshal.ReleaseComObject(excelApp);
                }
            }

        }


        private DataTable FilterData()
        {
            DataTable filteredTable = null;

            string shipmentStatusCriteria = string.Empty;
            // Create filter criteria
            if (String.IsNullOrEmpty(_shipmentStatusOpenParam))
            {
                shipmentStatusCriteria = "[" + PWS_SHIPMENT_STATUS + "] = '" + SHIPMENT_STATUS_OPEN + "'";

            } else if (_shipmentStatusOpenParam.Contains(";"))
            {
                string[] statusCriteria = _shipmentStatusOpenParam.Split(';');
                for (int i = 0; i < statusCriteria.Length; i++)
                {
                    string status = statusCriteria[i];
                    if (shipmentStatusCriteria.Length > 0)
                    {
                        shipmentStatusCriteria += " OR [" + PWS_SHIPMENT_STATUS + "] = '" + status + "'";
                    } else
                    {
                        shipmentStatusCriteria = "[" + PWS_SHIPMENT_STATUS + "] = '" + status + "'";
                    }
                }

            } else
            {
                shipmentStatusCriteria = _shipmentStatusOpenParam;
            }

            // Filter items to expire with Shipment Status
            DataRow[] filteredRows = _dtImportsToExpire.Select(shipmentStatusCriteria + $" AND [{PWS_VALUE}] > 0");
            if (filteredRows.Length > 0)
            {
                DataTable newData = new DataTable();
                newData = filteredRows.CopyToDataTable();
                _dtImportsToExpire = newData;
            } else
            {
                _dtImportsToExpire.Rows.Clear();
            }

            
            if (_dtImportsToExpire.Rows.Count > 0 && _dtVendorLineItems.Rows.Count > 0)
            {
                filteredTable = this.GetTableImport();

                foreach (DataRow item in _dtImportsToExpire.Rows)
                {
                    string provisionDoc = item[PWS_PROVISION_DOC].ToString();
                    decimal value = Convert.ToDecimal(item[PWS_VALUE]);

                    var rows = from row in _dtVendorLineItems.Rows.Cast<DataRow>()
                               where row.Field<string>(FBL1N_DOCUMENT_NUMBER) == provisionDoc
                                && Math.Abs(row.Field<decimal>(FBL1N_AMOUNT_IN_DOC_CURRENCY)) == Math.Abs(value)
                               select row;
                    
                    if (rows != null && rows.Count() > 0)
                    {
                        DataRow rowVendorItem = rows.First();
                        DataRow importRow = filteredTable.NewRow();
                        importRow["Company"] = _company.CompanyCode;
                        importRow["VendorCode"] = item[PWS_SUPPLIER_CODE];
                        importRow["VendorName"] = item[PWS_SUPPLIER_NAME];
                        importRow["DocumentProvision"] = item[PWS_PROVISION_DOC];
                        importRow["ReferenceImportation"] = item[PWS_REFERENCE_IMPORTATION];
                        importRow["ShipmentNumber"] = item[PWS_SHIPMENT_NUMBER];
                        importRow["DocumentCurrency"] = item[PWS_CURRENCY];
                        importRow["ImportDocumentValue"] = item[PWS_VALUE];
                        importRow["ShipmentSeqNumber"] = item[PWS_SHIPMENT_SEQ_NUMBER];
                        importRow["ImportReference"] = item[PWS_INTERNAL_REFERENCE];
                        importRow["ExpirationDate"] = item[PWS_EXPIRATION_DATE];
                        importRow["ShipmentStatus"] = item[PWS_SHIPMENT_STATUS];
                        importRow["Account"] = rowVendorItem[FBL1N_ACCOUNT];
                        importRow["InvoiceReference"] = rowVendorItem[FBL1N_REFERENCE];
                        importRow["Assignment"] = rowVendorItem[FBL1N_ASSIGNMENT];
                        importRow["InvoiceDocumentNumber"] = rowVendorItem[FBL1N_DOCUMENT_NUMBER];
                        importRow["InvoiceDocumentType"] = rowVendorItem[FBL1N_DOCUMENT_TYPE];
                        importRow["InvoicePostingDate"] = ExcelHelper.GetDateTime(rowVendorItem[FBL1N_POSTING_DATE]);
                        importRow["InvoiceDocumentDate"] = ExcelHelper.GetDateTime(rowVendorItem[FBL1N_DOCUMENT_DATE]);
                        importRow["DelayAfterNetDueDate"] = rowVendorItem[FBL1N_ARREARS_AFTER_NET_DUE_DATE];
                        importRow["InvoiceNetDueDate"] = ExcelHelper.GetDateTime(rowVendorItem[FBL1N_NET_DUE_DATE]);
                        importRow["InvoiceDocumentCurrency"] = rowVendorItem[FBL1N_DOCUMENT_CURRENCY];
                        importRow["InvoiceAmountCurrency"] = rowVendorItem[FBL1N_AMOUNT_IN_DOC_CURRENCY];
                        importRow["InvoiceAmountLocalCurrency"] = rowVendorItem[FBL1N_AMOUNT_IN_LOCAL_CURRENCY];
                        importRow["InvoiceText"] = rowVendorItem[FBL1N_TEXT];
                        importRow["BusinessPlaceCode"] = rowVendorItem[FBL1N_BUSINESS_PLACE];
                        importRow["UserCode"] = _user.UserName;
                        importRow["MachineName"] = AppLayer.AppSettings.Instance.CurrentMachineName;
                        importRow["DataImportRoutineID"] = _dataImportRoutineID;
                        importRow["DataImportExecutionID"] = _dataExecutionID;
                        filteredTable.Rows.Add(importRow);
                    }
                }
            }

            return filteredTable;
        }


        private DataTable GetTableImport()
        {
            DataTable data = new DataTable();
            data.Columns.Add("Company", typeof(int));
            data.Columns.Add("VendorCode", typeof(string));
            data.Columns.Add("VendorName", typeof(string));
            data.Columns.Add("DocumentProvision", typeof(string));
            data.Columns.Add("ReferenceImportation", typeof(string));
            data.Columns.Add("ShipmentNumber", typeof(string));
            data.Columns.Add("DocumentCurrency", typeof(string));
            data.Columns.Add("ImportDocumentValue", typeof(decimal));
            data.Columns.Add("ShipmentSeqNumber", typeof(string));
            data.Columns.Add("ImportReference", typeof(string));
            data.Columns.Add("ExpirationDate", typeof(DateTime));
            data.Columns.Add("ShipmentStatus", typeof(string));
            data.Columns.Add("Account", typeof(string));
            data.Columns.Add("InvoiceReference", typeof(string));
            data.Columns.Add("Assignment", typeof(string));
            data.Columns.Add("InvoiceDocumentNumber", typeof(string));
            data.Columns.Add("InvoiceDocumentType", typeof(string));
            data.Columns.Add("InvoicePostingDate", typeof(DateTime));
            data.Columns.Add("InvoiceDocumentDate", typeof(DateTime));
            data.Columns.Add("DelayAfterNetDueDate", typeof(int));
            data.Columns.Add("InvoiceNetDueDate", typeof(DateTime));
            data.Columns.Add("InvoiceDocumentCurrency", typeof(string));
            data.Columns.Add("InvoiceAmountCurrency", typeof(decimal));
            data.Columns.Add("InvoiceAmountLocalCurrency", typeof(decimal));
            data.Columns.Add("InvoiceText", typeof(string));
            data.Columns.Add("BusinessPlaceCode", typeof(string));
            data.Columns.Add("UserCode", typeof(string));
            data.Columns.Add("MachineName", typeof(string));
            data.Columns.Add("DataImportRoutineID", typeof(int));
            data.Columns.Add("DataImportExecutionID", typeof(int));

            return data;
        }


        private void WriteToDatabase(DataTable data)
        {
            ImportPaymentItemService.ImportData(data, _dataExecutionID);
        }

    }
}
