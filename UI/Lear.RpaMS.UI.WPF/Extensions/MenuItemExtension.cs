﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lear.RpaMS.UI.WPF.Extensions
{
    class MenuItemExtension
    {

        public static readonly DependencyProperty IsNewWindowProperty = DependencyProperty.RegisterAttached("IsNewWindow",
                                                                                                        typeof(bool),
                                                                                                        typeof(MenuItemExtension),
                                                                                                        new PropertyMetadata(default(bool)));

        public static void SetIsNewWindow(UIElement element, bool value)
        {
            element.SetValue(IsNewWindowProperty, value);
        }

        public static bool GetIsNewWindow(UIElement element)
        {
            return (bool) element.GetValue(IsNewWindowProperty);
        }

    }
}
