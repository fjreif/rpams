﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class EditPaymentInProgressViewModel : ViewModelBase
    {
        private ViewModelManager manager = null;
        private Function function = null;
        private PaymentItem _Entity = new PaymentItem();

        #region Constructor
        public EditPaymentInProgressViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion

        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public PaymentItem Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }

        public override void Init()
        {
            function = manager.CheckAndAddFunction(this.LinkedComponent);
            DisplayStatusMessage(function.Name);
            function.Parameters = FunctionService.LoadParameters(function.Id);
            function.Processes = FunctionService.LoadProcesses(function.Id);
        }

    }
}
