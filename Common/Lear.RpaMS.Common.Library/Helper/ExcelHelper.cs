﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Common.Library.Helper
{
    public class ExcelHelper
    {

        public static object GetValueFromDynamic(dynamic value, object defaultValue)
        {
            try
            {
                return value.Value2;
            }
            catch (Exception)
            {
                if (defaultValue != null)
                {
                    return default;
                }
            }
            return null;
        }

        public static object GetValue(object value, object defaultValue)
        {
            try
            {
                if (value != null)
                {
                    return value;
                }

                return defaultValue;
            }
            catch (Exception)
            {
                if (defaultValue != null)
                {
                    return defaultValue;
                }
            }
            return null;
        }

        public static Nullable<DateTime> GetDateTime(dynamic value)
        {
            try
            {
                string valueAsString = Convert.ToString(value);

                try
                {
                    double d = double.Parse(valueAsString);
                    DateTime convFromDouble = DateTime.FromOADate(d);
                    return convFromDouble;
                }
                catch (Exception)
                {}

                DateTime conv = Convert.ToDateTime(value.ToString());
                return conv;

            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Nullable<DateTime> GetDateTimeFromDouble(dynamic value)
        {
            try
            {
                string dateAsDouble = Convert.ToString(value);
                double d = double.Parse(dateAsDouble);
                DateTime conv = DateTime.FromOADate(d);
                return conv;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
