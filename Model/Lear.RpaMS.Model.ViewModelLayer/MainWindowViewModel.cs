﻿using Lear.RpaMS.Business.Membership;
using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Common.Library.Notification;
using Lear.RpaMS.Data.DataLayer;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Membership;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Membership;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;
using System.Windows;

namespace Lear.RpaMS.Model.ViewModelLayer
{
    public class MainWindowViewModel : ViewModelBase
    {

        #region Private Variables
        private const int SECONDS = 500;
        private Timer _InfoMessageTimer = null;
        private int _InfoMessageTimeout;
        private Timer _NotificationMessageTimer = null;
        private int _NotificationMessageTimeout;
        private User _UserEntity = new User();
        private ViewModelManager manager = null;

        private ObservableCollection<NotificationMessage> _NotificationMessages = new ObservableCollection<NotificationMessage>();
        private object _lock = new object();
        private bool _IsNotificationVisible = false;

        private Company _SelectedCompany;
        private ObservableCollection<Company> _Companies = new ObservableCollection<Company>();

        private string _LoginMenuHeader = "Login";
        private string _StatusMessage;
        private bool _IsInfoMessageVisible = true;
        private string _InfoMessage = string.Empty;
        private string _InfoMessageTitle = string.Empty;

        private string _CurrentUser = string.Empty;
        private string _DatabaseConnection = string.Empty;
        private string _AppVersion = string.Empty;
        #endregion

        #region Constructor
        public MainWindowViewModel() : base()
        {
            System.Windows.Data.BindingOperations.EnableCollectionSynchronization(_NotificationMessages, _lock);
            manager = new ViewModelManager();
        }
        #endregion


        #region Public Properties
        public User UserEntity
        {
            get { return _UserEntity; }
            set
            {
                _UserEntity = value;
                RaisePropertyChanged("UserEntity");
            }
        }

        public Company SelectedCompany
        {
            get { return _SelectedCompany; }
            set
            {
                _SelectedCompany = value;
                RaisePropertyChanged("SelectedCompany");
            }
        }

        public ObservableCollection<Company> Companies
        {
            get { return _Companies; }
            set
            {
                _Companies = value;
                RaisePropertyChanged("Companies");
            }
        }

        public int InfoMessageTimeout
        {
            get { return _InfoMessageTimeout; }
            set
            {
                _InfoMessageTimeout = value;
                RaisePropertyChanged("InfoMessageTimeout");
            }
        }

        public bool IsInfoMessageVisible
        {
            get { return _IsInfoMessageVisible; }
            set
            {
                _IsInfoMessageVisible = value;
                RaisePropertyChanged("IsInfoMessageVisible");
            }
        }

        public string InfoMessage
        {
            get { return _InfoMessage; }
            set
            {
                _InfoMessage = value;
                RaisePropertyChanged("InfoMessage");
            }
        }

        public string InfoMessageTitle
        {
            get { return _InfoMessageTitle; }
            set
            {
                _InfoMessageTitle = value;
                RaisePropertyChanged("InfoMessageTitle");
            }
        }

        public int NotificationMessageTimeout
        {
            get { return _NotificationMessageTimeout; }
            set
            {
                _NotificationMessageTimeout = value;
                RaisePropertyChanged("NotificationMessageTimeout");
            }
        }

        public string LoginMenuHeader
        {
            get { return _LoginMenuHeader; }
            set
            {
                _LoginMenuHeader = value;
                RaisePropertyChanged("LoginMenuHeader");
            }
        }

        public string StatusMessage
        {
            get { return _StatusMessage; }
            set
            {
                _StatusMessage = value;
                RaisePropertyChanged("StatusMessage");
            }
        }

        public string CurrentUser
        {
            get { return _CurrentUser; }
            set
            {
                _CurrentUser = value;
                RaisePropertyChanged("CurrentUser");
            }
        }

        public string DatabaseConnection
        {
            get { return _DatabaseConnection; }
            set
            {
                _DatabaseConnection = value;
                RaisePropertyChanged("DatabaseConnection");
            }
        }

        public string AppVersion
        {
            get { return _AppVersion; }
            set
            {
                _AppVersion = value;
                RaisePropertyChanged("AppVersion");
            }
        }

        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public ObservableCollection<NotificationMessage> NotificationMessages
        {
            get { return _NotificationMessages; }
            set
            {
                _NotificationMessages = value;
                RaisePropertyChanged("NotificationMessages");
            }
        }

        public bool IsNotificationVisible
        {
            get { return _IsNotificationVisible; }
            set
            {
                _IsNotificationVisible = value;
                RaisePropertyChanged("IsNotificationVisible");
            }
        }
        #endregion

        public virtual void CreateInfoMessageTimer()
        {
            if (_InfoMessageTimer == null)
            {
                // Create informational message timer
                _InfoMessageTimer = new Timer(_InfoMessageTimeout);
                // Connect to an Elapsed event
                _InfoMessageTimer.Elapsed += _MessageTimer_Elapsed;
            }
            _InfoMessageTimer.AutoReset = false;
            _InfoMessageTimer.Enabled = true;
            IsInfoMessageVisible = true;
        }

        private void _MessageTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            IsInfoMessageVisible = false;
        }

        #region AddNotificationMessage Method
        public virtual void AddNotificationMessage(string msg)
        {
            _NotificationMessages.Add(new NotificationMessage { Message = msg });
            IsNotificationVisible = true;
        }
        #endregion

        public virtual void CreateNotificationMessageTimer(string msg, int timeOut = 0)
        {
            if (_NotificationMessageTimer == null)
            {
                // Create notification message timer
                if (timeOut > 0)
                {
                    _NotificationMessageTimer = new Timer(timeOut);
                } else
                {
                    _NotificationMessageTimer = new Timer(_NotificationMessageTimeout);
                }                
                // Connect to an Elapsed event
                _NotificationMessageTimer.Elapsed += NotificationTimer_Elapsed;
            }
            _NotificationMessageTimer.AutoReset = false;
            _NotificationMessageTimer.Enabled = true;
            IsNotificationVisible = true;
            AddNotificationMessage(msg);
        }

        private void NotificationTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ClearNotificationMessages();            
        }

        public virtual void ClearNotification()
        {
            NotificationMessages.Clear();
            IsNotificationVisible = false;
        }

        public void LoadCompanies()
        {
            try
            {
                CompanyData data = new CompanyData();
                List<Company> companies = data.GetCompanies();
                foreach (Company company in companies)
                {
                    company.Id = Convert.ToInt32(company.CompanyCode);
                }
                companies.Insert(0, new Company() { Id = 0, CompanyName = "Selecione aqui Empresa Padrão" });
                Companies = new ObservableCollection<Company>(companies);
            }
            catch (System.Exception ex)
            {
                DisplayStatusMessage(ex.Message);
            }
        }

        public void LoadProjectSettings(System.Reflection.Assembly assembly)
        {
            try
            {
                // Get the AssemblyInfo class.
                AssemblyInfo info = new AssemblyInfo(assembly);
                string projectCode = info.Guid;
                // Load project settings
                Project project = ProjectService.LoadProjectByCode(projectCode);
                if (project == null)
                {
                    throw new Exception($"Error: Project '{project}' not found! ");
                }
                project.Parameters = ProjectService.LoadParameters(project.Id);
                AppLayer.AppSettings.Instance.CurrentProject = project;

            }
            catch (System.Exception ex)
            {
                DisplayStatusMessage(ex.Message);
            }
        }

        public void LoadStateCodes()
        {
            // TODO: Write code to load state codes here
            System.Threading.Thread.Sleep(SECONDS);
        }

        public void LoadCountryCodes()
        {
            // TODO: Write code to load country codes here
            System.Threading.Thread.Sleep(SECONDS);
        }

        public void LoadEmployeeTypes()
        {
            // TODO: Write code to load employee types here
            System.Threading.Thread.Sleep(SECONDS);
        }

        public void ClearInfoMessages()
        {
            InfoMessage = string.Empty;
            InfoMessageTitle = string.Empty;
            IsInfoMessageVisible = false;
        }

        public void ClearNotificationMessages()
        {
            this.ClearNotification();            
        }

        public void LoadStatusBarData(System.Reflection.Assembly assembly)
        {
            CurrentUser = $"{Environment.MachineName}\\{Environment.UserName}";

            AssemblyInfo assemblyInfo = new AssemblyInfo(assembly);
            AppVersion = assemblyInfo.AssemblyVersion;

            string conn = AppLayer.AppSettings.Instance.ConnectionString;
            string[] auxConn = conn.Split(';');
            DatabaseConnection = $"{auxConn[0]}\\{auxConn[1]}";
        }

        public void UpdateStatusBarData()
        {
            if (this.UserEntity != null)
            {
                CurrentUser = $"{Environment.MachineName}\\{this.UserEntity.UserName}";
            }
        }

        public void LoadPreferredSettings()
        {            
            int? prefferedCompany = UserPrefferedSettingsBusiness.GetPreferredCompany();
            // Company
            if (prefferedCompany != null)
            {
                if (SelectedCompany == null || SelectedCompany.Id != prefferedCompany)
                {
                    SelectedCompany = this.GetCompany(Convert.ToInt32(prefferedCompany));
                }
            } else
            {
                if (Companies.Count > 0)
                {
                    SelectedCompany = Companies[1];
                }
            }
            AppLayer.AppSettings.Instance.CurrentCompany = SelectedCompany;
        }

        private Company GetCompany(int companyID)
        {
            Company company = null;
            foreach (Company comp in Companies)
            {
                if (comp.Id == companyID)
                {
                    company = comp;
                    break;
                }
            }
            return company;
        }

        public void SavePreferredCompany(Company currentCompany, Company previousCompany)
        {
            try
            {
                Company selectedValue = currentCompany;
                if (currentCompany.Id == 0)
                {
                    if (previousCompany != null)
                    {
                        selectedValue = previousCompany;
                    }

                    UserPreferredSettings settings = MembershipService.LoadPreferredSettings(AppLayer.AppSettings.Instance.CurrentUser.UserName);
                    settings.Company = selectedValue.Id;
                    MembershipService.SavePreferredSettings(
                        AppLayer.AppSettings.Instance.CurrentUser.UserName,
                        settings);
                    MessageBox.Show("Empresa padrão alterada!");
                }

                AppLayer.AppSettings.Instance.CurrentCompany = selectedValue;
                if (SelectedCompany.CompanyCode != selectedValue.CompanyCode)
                {
                    SelectedCompany = selectedValue;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não foi possível alterar Empresa padrão. Erro: " + ex.Message);
            }            
        }

        public List<UserMenuItem> LoadMenuItem()
        {
            return MembershipService.LoadUserMenuItem(AppLayer.AppSettings.Instance.CurrentUser.UserName);
        }

    }
}
