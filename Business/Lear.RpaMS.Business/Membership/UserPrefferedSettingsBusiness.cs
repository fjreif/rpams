﻿using Lear.RpaMS.Data.DataServiceLayer.Membership;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Business.Membership
{
    public class UserPrefferedSettingsBusiness
    {

        public static UserPreferredSettings LoadPreferredSettings()
        {
            return AppLayer.AppSettings.Instance.CurrentUser.PreferredSettings =
                        MembershipService.LoadPreferredSettings(AppLayer.AppSettings.Instance.CurrentUser.UserName);
        }

        public static int? GetPreferredCompany()
        {
            UserPreferredSettings settings = LoadPreferredSettings();
            if (settings != null && settings.Company != null)
            {
                return settings.Company;
            }
            return null;
        }


    }
}
