﻿using Lear.RpaMS.Common.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.UI.Data
{
    public class TrackingNumberNode : CommonBase
    {
        private bool _IsSelected;
        private bool _IsExpanded;
        private string _TrackingNumber;
        private int _DocumentID;
        private ObservableCollection<InvoiceDocument> _Invoices;

        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    RaisePropertyChanged("IsSelected");
                }
            }
        }

        public bool IsExpanded
        {
            get { return _IsExpanded; }
            set
            {
                if (value != _IsExpanded)
                {
                    _IsExpanded = value;
                    RaisePropertyChanged("IsExpanded");
                }
            }
        }

        public string TrackingNumber
        {
            get { return _TrackingNumber; }
            set
            {
                _TrackingNumber = value;
                RaisePropertyChanged("TrackingNumber");
            }
        }

        public int DocumentID
        {
            get { return _DocumentID; }
            set
            {
                _DocumentID = value;
                RaisePropertyChanged("DocumentID");
            }
        }

        public ObservableCollection<InvoiceDocument> Invoices
        {
            get { return _Invoices; }
            set
            {
                _Invoices = value;
                RaisePropertyChanged("Invoices");
            }
        }

    }

    public class InvoiceDocument : CommonBase
    {
        private int _DocumentID;
        private int _DocumentTypeID;
        private int _DocumentPageID;
        private string _Name;
        private string _DocumentPageFullName;
        private int? _DocumentPageDataID;
        private int? _DocumentValueTypeID;
        private string _Value;

        public int DocumentID
        {
            get { return _DocumentID; }
            set
            {
                _DocumentID = value;
                RaisePropertyChanged("DocumentID");
            }
        }

        public int DocumentTypeID
        {
            get { return _DocumentTypeID; }
            set
            {
                _DocumentTypeID = value;
                RaisePropertyChanged("DocumentTypeID");
            }
        }

        public int DocumentPageID
        {
            get { return _DocumentPageID; }
            set
            {
                _DocumentPageID = value;
                RaisePropertyChanged("DocumentPageID");
            }
        }

        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string DocumentPageFullName
        {
            get { return _DocumentPageFullName; }
            set
            {
                _DocumentPageFullName = value;
                RaisePropertyChanged("DocumentPageFullName");
            }
        }

        public int? DocumentPageDataID
        {
            get { return _DocumentPageDataID; }
            set
            {
                _DocumentPageDataID = value;
                RaisePropertyChanged("DocumentPageDataID");
            }
        }

        public int? DocumentValueTypeID
        {
            get { return _DocumentValueTypeID; }
            set
            {
                _DocumentValueTypeID = value;
                RaisePropertyChanged("DocumentValueTypeID");
            }
        }


        public string Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                RaisePropertyChanged("Value");
            }
        }


    }

}
