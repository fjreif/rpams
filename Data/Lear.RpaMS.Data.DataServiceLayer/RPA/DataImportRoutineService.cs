﻿using Lear.RpaMS.Data.DataLayer.RPA;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataServiceLayer.RPA
{
    public class DataImportRoutineService
    {

        public static DataImportRoutine LoadRoutineByCode(string code)
        {
            DataImportRoutineData data = new DataImportRoutineData();
            return data.LoadRoutineByCode(code);
        }

    }
}
