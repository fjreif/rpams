﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.UI.Data
{
    public class PaymentInBank : CommonBase
    {
        private int _Id;
        private int _Company;
        private int _BankReviewStatusID;
        private string _StatusBankReviewName;
        private string _Reference;
        private string _CustomerReference;
        private string _Ticket;
        private DateTime _AdmissionDate;
        private string _Concept;
        private string _StatusPayment;
        private string _Currency;
        private double _Amount;
        private string _Information;
        private string _DealNumber;
        private string _UserCodeUpdate;

        public int Id { get => _Id; set { _Id = value; RaisePropertyChanged("Id"); } }
        public int Company { get => _Company; set { _Company = value; RaisePropertyChanged("Company"); } }
        public int BankReviewStatusID { get => _BankReviewStatusID; set { _BankReviewStatusID = value; RaisePropertyChanged("BankReviewStatusID"); } }
        public string StatusBankReviewName { get => _StatusBankReviewName; set { _StatusBankReviewName = value; RaisePropertyChanged("BankReviewStatusID"); } }
        public string Reference { get => _Reference; set { _Reference = value; RaisePropertyChanged("Reference"); } }
        public string CustomerReference { get => _CustomerReference; set { _CustomerReference = value; RaisePropertyChanged("CustomerReference"); } }
        public string Ticket { get => _Ticket; set { _Ticket = value; RaisePropertyChanged("Ticket"); } }
        public DateTime AdmissionDate { get => _AdmissionDate; set { _AdmissionDate = value; RaisePropertyChanged("AdmissionDate"); } }
        public string Concept { get => _Concept; set { _Concept = value; RaisePropertyChanged("Concept"); } }
        public string StatusPayment { get => _StatusPayment; set { _StatusPayment = value; RaisePropertyChanged("StatusPayment"); } }
        public string Currency { get => _Currency; set { _Currency = value; RaisePropertyChanged("Currency"); } }
        public double Amount { get => _Amount; set { _Amount = value; RaisePropertyChanged("Amount"); } }
        public string Information { get => _Information; set { _Information = value; RaisePropertyChanged("Information"); } }
        public string DealNumber { get => _DealNumber; set { _DealNumber = value; RaisePropertyChanged("DealNumber"); } }
        public string UserCodeUpdate { get => _UserCodeUpdate; set { _UserCodeUpdate = value; RaisePropertyChanged("UserCodeUpdate"); } }        

        public List<PaymentItem> Items { get; set; }

    }    


}
