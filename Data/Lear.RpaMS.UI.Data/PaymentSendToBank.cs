﻿using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.UI.Data
{
    public class PaymentSendToBank
    {

        private int _DocumentID;
        private int _PaymentID;
        private string _Name;
        private string _FullNameInsert;
        private string _FullName;
        private int _DocumentTypeID;
        private DateTime _DateTimeInsert;
        private double _TotalAmount;

        public int DocumentID { get => _DocumentID; set { _DocumentID = value; } }
        public int PaymentID { get => _PaymentID; set { _PaymentID = value;  } }
        public string Name { get => _Name; set { _Name = value; } }
        public string FullNameInsert { get => _FullNameInsert; set { _FullNameInsert = value; } }
        public string FullName { get => _FullName; set { _FullName = value; } }
        public int DocumentTypeID { get => _DocumentTypeID; set { _DocumentTypeID = value; } }
        public DateTime DateTimeInsert { get => _DateTimeInsert; set { _DateTimeInsert = value; } }
        public double TotalAmount { get => _TotalAmount; set { _TotalAmount = value; } }


        public List<PaymentItem> Items { get; set; }

    }
}
