﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Controls
{
    /// <summary>
    /// Interaction logic for TextBoxMultiSelectionControl.xaml
    /// </summary>
    public partial class TextBoxMultiSelectionControl : UserControl
    {
        public TextBoxMultiSelectionControl()
        {
            InitializeComponent();
        }

        private void ImgOpenMultiSelection_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Image clicked!!");
        }
    }
}
