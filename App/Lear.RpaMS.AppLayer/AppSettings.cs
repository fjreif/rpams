﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.AppLayer
{
    public class AppSettings : ConfigurationSettings
    {

        #region Instance Property
        private static AppSettings _Instance;

        public static AppSettings Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AppSettings();
                }

                return _Instance;
            }
            set { _Instance = value; }
        }
        #endregion

        #region Private Properties
        private string _ConnectionString = string.Empty;
        private User _currentUser;
        private Company _currentCompany;
        private string _currentMachineName;
        private int _currentTransactionLanguage;
        #endregion

        #region Public Properties
        public string ConnectionString
        {
            get { return _ConnectionString; }
            set
            {
                _ConnectionString = value;
                RaisePropertyChanged("ConnectionString");
            }
        }

        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                RaisePropertyChanged("CurrentUser");
            }
        }

        public Company CurrentCompany
        {
            get { return _currentCompany; }
            set
            {
                _currentCompany = value;
                RaisePropertyChanged("CurrentCompany");
            }
        }

        public string CurrentMachineName
        {
            get { return _currentMachineName; }
            set
            {
                _currentMachineName = value;
                RaisePropertyChanged("CurrentMachineName");
            }
        }

        public int CurrentTransactionLanguage
        {
            get { return _currentTransactionLanguage; }
            set
            {
                _currentTransactionLanguage = value;
                RaisePropertyChanged("CurrentTransactionLanguage");
            }
        }
        #endregion

        #region LoadSettings Method
        public override void LoadSettings()
        {
            //InfoMessageTimeout = GetSetting<int>("InfoMessageTimeout", 1200);
            ConnectionString = GetConnectionString<string>("Portal_db", "");
            //ConnectionString = GetConnectionString<string>("RpaMS_Local", "");
        }
        #endregion

    }
}
