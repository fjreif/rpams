﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Data;

namespace Lear.RpaMS.Model.ViewModelLayer.Automation
{
    public class RunAutomationViewModel : ViewModelBase
    {


        private ObservableCollection<string> _OutputMessages = new ObservableCollection<string>();
        private object _lock = new object();
        public ObservableCollection<string> OutputMessages
        {
            get { return _OutputMessages; }
            set
            {
                _OutputMessages = value;
                RaisePropertyChanged("OutputMessages");
            }
        }

        private bool _IsAutomationRunning = false;
        private bool _CanCloseWindow = false;        
        // Elapsed time
        private System.Timers.Timer timer;
        private int _ElapsedHours;
        private int _ElapsedMinutes;
        private int _ElapsedSeconds;
        //Status
        private string _CurrentStaus = "Parado...";
        private int processExecutionID;

        private string _ProjectName = "Project Name Binding";
        private string _ProcessName = "Process Name Binding";
        Project automationProject = null;
        ProjectProcess automationProcess = null;
        private int automationProcessID;
        private Thread processThread;

        public bool IsAutomationRunning
        {
            get { return _IsAutomationRunning; }
            set
            {
                _IsAutomationRunning = value;
                RaisePropertyChanged("IsAutomationRunning");

                CanCloseWindow = !value;
            }
        }

        public bool CanCloseWindow
        {
            get { return _CanCloseWindow; }
            set
            {
                _CanCloseWindow = value;
                RaisePropertyChanged("CanCloseWindow");
            }
        }

        public int ElapsedHours
        {
            get { return _ElapsedHours; }
            set
            {
                _ElapsedHours = value;
                RaisePropertyChanged("ElapsedHours");
            }
        }

        public int ElapsedMinutes
        {
            get { return _ElapsedMinutes; }
            set
            {
                _ElapsedMinutes = value;
                RaisePropertyChanged("ElapsedMinutes");
            }
        }

        public int ElapsedSeconds
        {
            get { return _ElapsedSeconds; }
            set
            {
                _ElapsedSeconds = value;
                RaisePropertyChanged("ElapsedSeconds");
            }
        }

        public string CurrentStaus
        {
            get { return _CurrentStaus; }
            set
            {
                _CurrentStaus = value;
                RaisePropertyChanged("CurrentStaus");
            }
        }

        public string ProjectName
        {
            get { return _ProjectName; }
            set
            {
                _ProjectName = value;
                RaisePropertyChanged("ProjectName");
            }
        }

        public string ProcessName
        {
            get { return _ProcessName; }
            set
            {
                _ProcessName = value;
                RaisePropertyChanged("ProcessName");
            }
        }


        #region Constructor
        public RunAutomationViewModel() : base()
        {
            BindingOperations.EnableCollectionSynchronization(_OutputMessages, _lock);
        }
        #endregion

        public void Init(string projectCode, string processCode)
        {
            automationProject = ProjectService.LoadProjectByCode(projectCode);
            automationProject.Processes = ProjectService.LoadProcesses(automationProject.Id);
            automationProject.VerifyDynamicVariables();
            automationProcess = automationProject.GetProcessByCode(processCode);

            ProjectName = automationProject.Name;
            ProcessName = automationProcess.Name;
        }


        private void StartTimer()
        {
            this.EraseTimer();
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (ElapsedSeconds < 59)
            {
                ElapsedSeconds += 1;
            } else
            {
                ElapsedSeconds = 0;

                if (ElapsedMinutes < 59) {
                    ElapsedMinutes += 1;
                } else
                {
                    ElapsedMinutes = 0;

                    if (ElapsedHours < 23)
                    {
                        ElapsedHours += 1;
                    } else
                    {
                        ElapsedHours = 0;
                    }
                }
            }
        }    
        
        private void EraseTimer()
        {
            ElapsedHours = 0;
            ElapsedMinutes = 0;
            ElapsedSeconds = 0;
        }

        private void StopTimer()
        {
            if (timer != null)
            {
                timer.Stop();
            }
        }
        


        public void StartAutomation(string arguments)
        {
            try
            {
                this.StartTimer();
                this.OutputMessages.Clear();
                IsAutomationRunning = true;
                this.StartProcess(arguments);
                this.RegisterProcessExecutionStart();                
            }
            catch (Exception ex)
            {
                AddValidationMessage("RunAutomation", ex.Message);
            }
        }

        private void RegisterProcessExecutionStart()
        {
            try
            {
                processExecutionID = ProjectService.StartProcessExecution(automationProcess.Id, string.Empty);
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorProcessExecutionStart", ex.Message);
            }
        }

        private void RegisterProcessExecutionEnd(StatusExecution status, string message)
        {
            try
            {
                if (processExecutionID > 0)
                {
                    ProjectService.EndProcessExecution(processExecutionID, status, message);
                }
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorProcessExecutionEnd", ex.Message);
            }

        }

        private void StartProcess(string arguments)
        {
            // Create a thread
            processThread = new Thread(new ThreadStart(() =>
            {
                Process process = new Process();
                if (automationProcess.IsCreateWindow)
                {
                    process.StartInfo.CreateNoWindow = false;
                } else
                {
                    process.StartInfo.CreateNoWindow = true;
                }
                process.StartInfo.FileName = System.IO.Path.Combine(automationProcess.Path, automationProcess.FileName);
                process.StartInfo.Arguments = automationProcess.Arguments 
                                        + (!String.IsNullOrEmpty(arguments) ? arguments : string.Empty);
                process.StartInfo.WorkingDirectory = automationProcess.Path;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.EnableRaisingEvents = true;
                process.Exited += Process_Exited;
                process.OutputDataReceived += Process_OutputDataReceived;

                OutputMessages.Add(DateTime.Now.ToString("HH:mm:ss") + " - FileName: " + process.StartInfo.FileName);
                OutputMessages.Add(DateTime.Now.ToString("HH:mm:ss") + " - Arguments: " + process.StartInfo.Arguments);

                if (process.Start())
                {
                    automationProcessID = process.Id;
                }
                process.BeginOutputReadLine();
                process.WaitForExit();
            }));
            // Setup and start thread as before
            processThread.Start();

            CurrentStaus = "Executando...";
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Process process = (Process)sender;
            OutputMessages.Add(DateTime.Now.ToString("HH:mm:ss") + " - " + e.Data);
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            try
            {
                Process process = (Process)sender;
                this.StopTimer();
                IsAutomationRunning = false;
                CurrentStaus = "Parado...";
                this.RegisterProcessExecutionEnd(StatusExecution.SUCCESS, string.Empty);
            }
            catch (Exception ex)
            {
                
                
            }
        }

        public void StopAutomation()
        {
            try
            {  
                if (automationProcessID > 0)
                {
                    try
                    {
                        Process[] processes = Process.GetProcessesByName("node");
                        if (processes != null && processes.Length > 0)
                        {
                            foreach (var process in processes)
                            {
                                try
                                {
                                    process.Kill();
                                }
                                catch (Exception)
                                {}
                            }
                        }
                    }
                    catch (Exception)
                    {}

                    try
                    {
                        Process process = Process.GetProcessById(automationProcessID);
                        while (process != null)
                        {
                            process.Kill();
                            Thread.Sleep(1000);
                        }
                    }
                    catch (Exception)
                    {}
                }

                if (processThread != null && processThread.IsAlive)
                {
                    processThread.Abort();
                }

                this.StopTimer(); 
                IsAutomationRunning = false;
                CurrentStaus = "Cancelado.";
                this.RegisterProcessExecutionEnd(StatusExecution.CANCELED, string.Empty);
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorStop", ex.Message);
                this.StopTimer();
                IsAutomationRunning = false;
                CurrentStaus = "Cancelado.";
                this.RegisterProcessExecutionEnd(StatusExecution.ERROR, ex.Message);
            }            
        }


        private void WriteOutput(string output)
        {
            if (!String.IsNullOrEmpty(output))
            {

            }
        }

    }
}
