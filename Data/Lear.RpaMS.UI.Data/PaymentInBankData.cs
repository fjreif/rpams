﻿using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Data.DataLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Lear.RpaMS.UI.Data
{
    public class PaymentInBankData
    {

        public List<PaymentInBank> GetPaymentInBank(
            int company,
            DateTime? dateFrom,
            DateTime? dateTo,
            int statusBankReviewID)
        {
            SqlCommand command = null;
            List<PaymentInBank> list = null;

            try
            {
                command = DataConnectionBase.GetCommand();
                command.CommandText = "SP_AP_GetPaymentInBank";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add("@TranslationLanguageID", System.Data.SqlDbType.Int);
                command.Parameters["@TranslationLanguageID"].Value = AppLayer.AppSettings.Instance.CurrentTranslationLanguage;
                command.Parameters.Add("@Company", System.Data.SqlDbType.Int);
                command.Parameters["@Company"].Value = company;
                command.Parameters.Add("@AdmissionDateFrom", System.Data.SqlDbType.DateTime);
                if (dateFrom == null)
                {
                    command.Parameters["@AdmissionDateFrom"].Value = SqlHelper.GetMinDate();
                }
                else
                {
                    command.Parameters["@AdmissionDateFrom"].Value = dateFrom;
                }

                command.Parameters.Add("@AdmissionDateTo", System.Data.SqlDbType.DateTime);
                if (dateTo == null)
                {
                    command.Parameters["@AdmissionDateTo"].Value = SqlHelper.GetMinDate();
                }
                else
                {
                    command.Parameters["@AdmissionDateTo"].Value = dateTo;
                }
                command.Parameters.Add("@StatusBankRevisionID", System.Data.SqlDbType.Int);
                command.Parameters["@StatusBankRevisionID"].Value = statusBankReviewID;

                list = new List<PaymentInBank>();

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string reference = SqlHelper.GetValue(reader["Reference"], string.Empty).ToString();
                        PaymentInBank item = list.Find(bankItem => bankItem.Reference == reference);
                        bool newItem = false;
                        if (item == null)
                        {
                            newItem = true;

                            item = new PaymentInBank();
                            item.Id = Convert.ToInt32(reader["ID"]);
                            item.Company = Convert.ToInt32(reader["Company"]);
                            item.BankReviewStatusID = Convert.ToInt32(reader["BankReviewStatusID"]);
                            item.StatusBankReviewName = SqlHelper.GetValue(reader["StatusBankReviewName"], string.Empty).ToString();
                            item.Reference = SqlHelper.GetValue(reader["Reference"], string.Empty).ToString();
                            item.CustomerReference = SqlHelper.GetValue(reader["CustomerReference"], string.Empty).ToString();
                            item.Ticket = SqlHelper.GetValue(reader["Ticket"], string.Empty).ToString();
                            item.AdmissionDate = Convert.ToDateTime(reader["AdmissionDate"]);
                            item.Concept = SqlHelper.GetValue(reader["Concept"], string.Empty).ToString();
                            item.StatusPayment = SqlHelper.GetValue(reader["StatusPayment"], string.Empty).ToString();
                            item.Currency = SqlHelper.GetValue(reader["Currency"], string.Empty).ToString();
                            item.Amount = Convert.ToDouble(reader["Amount"]);
                            item.Information = SqlHelper.GetValue(reader["Information"], string.Empty).ToString();
                            item.DealNumber = SqlHelper.GetValue(reader["DealNumber"], string.Empty).ToString();
                            item.UserCodeUpdate = SqlHelper.GetValue(reader["UserCodeUpdate"], string.Empty).ToString();

                            item.Items = new List<Model.ModelLayer.Finance.AP.PaymentItem>();
                        }

                        int? paymentID = SqlHelper.GetNullableInt(reader["PaymentID"], null);
                        if (paymentID != null)
                        {
                            Model.ModelLayer.Finance.AP.PaymentItem paymentItem = new Model.ModelLayer.Finance.AP.PaymentItem();
                            paymentItem.Id = Convert.ToInt32(paymentID);
                            paymentItem.ImportReference = SqlHelper.GetValue(reader["ImportReference"], string.Empty).ToString();
                            paymentItem.ImportDocumentValue = Convert.ToDouble(SqlHelper.GetValue(reader["ImportDocumentValue"], 0));
                            paymentItem.PlantCode = SqlHelper.GetValue(reader["PlantCode"], string.Empty).ToString();
                            paymentItem.PlantName = SqlHelper.GetValue(reader["PlantName"], string.Empty).ToString();
                            paymentItem.BusinessPlaceCode = SqlHelper.GetValue(reader["BusinessPlaceCode"], string.Empty).ToString();
                            item.Items.Add(paymentItem);
                        }

                        if (newItem)
                        {
                            list.Add(item);
                        }
                    }
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }
            return list;
        }

    }
}
