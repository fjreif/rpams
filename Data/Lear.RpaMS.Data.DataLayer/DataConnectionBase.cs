﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Lear.RpaMS.AppLayer;

namespace Lear.RpaMS.Data.DataLayer
{
    public class DataConnectionBase
    {

        public static SqlCommand GetCommand()
        {
            string connectionString = AppSettings.Instance.ConnectionString;
            var connection = new SqlConnection(connectionString);
            var command = new SqlCommand();
            command.Connection = connection;
            return command;
        }

    }
}
