﻿using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataLayer.RPA
{
    public class DataImportRoutineData
    {

        private static readonly string TABLE_NAME = "RPA_DataImportRoutine";

        public DataImportRoutine LoadRoutineByCode(string routineCode)
        {
            SqlCommand command = null;
            DataImportRoutine importRoutine = null;

            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.SELECT);
                query.AddFields("ID, Code, Active");
                string fieldName = query.GetFieldWithTranslation("Name");
                string fieldDescription = query.GetFieldWithTranslation("Description");
                query.AddField(fieldName);
                query.AddField(fieldDescription);
                query.AddTable(TABLE_NAME);                
                query.AddWhereConditions("Code = @Code");

                command = DataConnectionBase.GetCommand();
                command.CommandText = query.GetQuery();
                command.Parameters.Add("@Code", System.Data.SqlDbType.VarChar);
                command.Parameters["@Code"].Value = routineCode;
                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        importRoutine = new DataImportRoutine();
                        importRoutine.Id = Convert.ToInt32(reader["ID"]);
                        importRoutine.Code = SqlHelper.GetValue(reader["Code"], string.Empty).ToString();
                        importRoutine.Name = SqlHelper.GetValue(reader[fieldName], string.Empty).ToString();
                        importRoutine.Description = SqlHelper.GetValue(reader[fieldDescription], string.Empty).ToString();
                        importRoutine.Active = Convert.ToBoolean(SqlHelper.GetValue(reader["Active"], true));
                    }
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return importRoutine;
        }

    }
}
