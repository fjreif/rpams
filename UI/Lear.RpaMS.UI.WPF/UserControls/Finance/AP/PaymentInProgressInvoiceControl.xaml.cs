﻿using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using Lear.RpaMS.UI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for PaymentInProgressInvoiceControl.xaml
    /// </summary>
    public partial class PaymentInProgressInvoiceControl : UserControl
    {
        // View model class
        private PaymentInProgressInvoiceViewModel _viewModel = null;

        public PaymentInProgressInvoiceControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (PaymentInProgressInvoiceViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadItems();
        }


        private void BtnSaveInvoiceData_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.UpdateInvoice();
        }

        private void TrvTrackingNumber_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e != null && e.NewValue.GetType() == typeof(InvoiceDocument))
            {
                _viewModel.Entity = (InvoiceDocument)e.NewValue;
                _viewModel.IsEntitySelected = true;
            } else
            {
                _viewModel.Entity = null;
                _viewModel.IsEntitySelected = false;
            }
        }
        

        private void BtnUpdateTree_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadItems();
        }

        private void BtnShowInvoice_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ShowInvoice();
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = (TextBox)sender;

            if (txt.Text.Length >= 5)
            {
                _viewModel.SearchTrackingNumber(txt.Text);
            } else
            {
                _viewModel.ClearSearch();
            }

        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string text = txtSearch.Text;
            if (text.Length > 0)
            {
                _viewModel.SearchTrackingNumber(text);
            }
        }
    }
}
