﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Data.DataServiceLayer.Queue;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.Queue;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class PaymentInProgressViewModel : ViewModelBase
    {

        private ObservableCollection<BindingViewModel<PaymentItem>> _BindingItems = new ObservableCollection<BindingViewModel<PaymentItem>>();
        private ViewModelManager manager = null;
        private Function function = null;
        private PaymentItem _Entity = new PaymentItem();
        private ObservableCollection<StatusBase> _listState = null;
        private int _StatePaymentID;
        private string _ShipmentNumber;
        private string _TrackingNumber;
        private string _ReferenceBank;

        #region Constructor
        public PaymentInProgressViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion

        #region Public Properties
        public ObservableCollection<BindingViewModel<PaymentItem>> BindingItems
        {
            get { return _BindingItems; }
            set
            {
                _BindingItems = value;
                RaisePropertyChanged("BindingItems");
            }
        }

        public ObservableCollection<StatusBase> ListState
        {
            get { return _listState; }
            set
            {
                _listState = value;
                RaisePropertyChanged("ListState");
            }
        }

        public int StatePaymentID
        {
            get { return _StatePaymentID; }
            set
            {
                _StatePaymentID = value;
                RaisePropertyChanged("StatePaymentID");
            }
        }

        public string ShipmentNumber
        {
            get { return _ShipmentNumber; }
            set
            {
                _ShipmentNumber = value;
                RaisePropertyChanged("ShipmentNumber");
            }
        }

        public string TrackingNumber
        {
            get { return _TrackingNumber; }
            set
            {
                _TrackingNumber = value;
                RaisePropertyChanged("TrackingNumber");
            }
        }

        public string ReferenceBank
        {
            get { return _ReferenceBank; }
            set
            {
                _ReferenceBank = value;
                RaisePropertyChanged("ReferenceBank");
            }
        }


        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public PaymentItem Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }
        #endregion


        public override void Init()
        {
            function = manager.CheckAndAddFunction(this.LinkedComponent);
            DisplayStatusMessage(function.Name);
            function.Parameters = FunctionService.LoadParameters(function.Id);
            function.Processes = FunctionService.LoadProcesses(function.Id);

            this.LoadListState();
        }

        private void LoadListState()
        {
            List<StatusBase> state = StatusBaseService.LoadAllStatePayment();
            state.Insert(0, new StatusBase() { Id = 0 });
            ListState = new ObservableCollection<StatusBase>(state);
        }

        public virtual void LoadItems()
        {
            try
            {
                ValidationMessages.Clear();
                List<PaymentItem> _items = PaymentItemService.LoadPaymentInProgressItemsSearch(
                    AppLayer.AppSettings.Instance.CurrentCompany.CompanyCode, 
                    StatePaymentID,
                    ShipmentNumber,
                    TrackingNumber,
                    null,
                    ReferenceBank,
                    null);
                List<BindingViewModel<PaymentItem>> _paymentItems = ViewModelHelper<PaymentItem>.ModelToViewModel(_items);
                this.CheckStatusColor(_paymentItems);
                BindingItems = new ObservableCollection<BindingViewModel<PaymentItem>>(_paymentItems);
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorLoadItems", ex.Message + " - " + ex.StackTrace);
            }
        }

        public List<ProjectProcess> GetProcesses()
        {
            return function.Processes;
        }

        private void CheckStatusColor(List<BindingViewModel<PaymentItem>> paymentItems)
        {            
            string colorStatusNone = AppLayer.AppSettings.Instance.CurrentProject.GetParameterByName("GridViewColumnColorStatusNone").Value;
            foreach (var item in paymentItems)
            {
                item.RowNumber = paymentItems.IndexOf(item) + 1;

                item.ColorColumn1 = colorStatusNone;
                item.ColorColumn2 = colorStatusNone;
                item.ColorColumn3 = colorStatusNone;
                item.ColorColumn4 = colorStatusNone;

                //Check status 1 = get tracking number
                if (item.Entity.TrackingStatusID != null && item.Entity.TrackingStatusID > 0)
                {
                    item.ToolTipColumn1 = item.Entity.TrackingStatus.Name;
                    //check not found
                    if (item.Entity.TrackingStatusID == 1)
                    {
                        item.ColorColumn1 = CommonStatusColor.COLOR_STATUS_ERROR;
                    }
                    //check not scanned
                    else if (item.Entity.TrackingStatusID == 2)
                    {
                        item.ColorColumn1 = CommonStatusColor.COLOR_STATUS_ALERT;
                    }
                    //Check if ok
                    else if (item.Entity.TrackingStatusID == 3)
                    {
                        item.ColorColumn1 = CommonStatusColor.COLOR_STATUS_SUCCESS;
                    }
                }

                //Check status 2 = Status INVOICE and SHIPPING DOC
                if (item.Entity.InvoiceDocStatusID != null && item.Entity.InvoiceDocStatusID > 0 &&
                    item.Entity.ShippingDocStatusID != null && item.Entity.ShippingDocStatusID > 0)
                {
                    item.ToolTipColumn2 = "Invoice: " + item.Entity.InvoiceDocStatus.Name + "\n" +
                                          "Doc Embarque: " + item.Entity.ShippingDocStatus.Name;

                    //NotFound
                    if (item.Entity.InvoiceDocStatusID == 1 ||
                        item.Entity.ShippingDocStatusID == 1)
                    {
                        item.ColorColumn2 = CommonStatusColor.COLOR_STATUS_ERROR;

                    }
                    //DownloadOk
                    else if (item.Entity.InvoiceDocStatusID == 2 &&
                             item.Entity.ShippingDocStatusID == 2)
                    {
                        item.ColorColumn2 = CommonStatusColor.COLOR_STATUS_SUCCESS;
                    }
                    //MoreThanOneDocument
                    else if (item.Entity.InvoiceDocStatusID == 3)
                    {
                        item.ColorColumn2 = CommonStatusColor.COLOR_STATUS_ALERT;
                    }
                    //NotFoundInvoice
                    else if (item.Entity.InvoiceDocStatusID == 4)
                    {
                        item.ColorColumn2 = CommonStatusColor.COLOR_STATUS_ERROR;
                    }
                }

                //Check Status 3 = document created?
                if (item.Entity.LetterDocStatusID != null)
                {
                    item.ToolTipColumn3 = item.Entity.LetterDocStatus.Name;
                    //LetterOk
                    if (item.Entity.LetterDocStatusID == 5)
                    {
                        item.ColorColumn3 = CommonStatusColor.COLOR_STATUS_SUCCESS;
                    }
                    //LetterError
                    else if (item.Entity.LetterDocStatusID == 6)
                    {
                        item.ColorColumn3 = CommonStatusColor.COLOR_STATUS_ERROR;
                    }
                }

                //Check Status 4 = document sent to bank?
                if (item.Entity.SentToBankDocStatusID != null)
                {
                    item.ToolTipColumn4 = item.Entity.SentToBankDocStatus.Name;
                    if (!String.IsNullOrEmpty(item.Entity.SentToBankMessage))
                    {
                        item.ToolTipColumn4 += "\n" + item.Entity.SentToBankMessage;
                    }
                    //SentToBankOk
                    if (item.Entity.SentToBankDocStatusID == 7)
                    {
                        item.ColorColumn4 = CommonStatusColor.COLOR_STATUS_SUCCESS;
                    }
                    //SentToBankError
                    else if (item.Entity.SentToBankDocStatusID == 8)
                    {
                        item.ColorColumn4 = CommonStatusColor.COLOR_STATUS_ERROR;
                    }
                }

            }
        }


        public void DeactivateItem()
        {
            try
            {
                PaymentItemService.DeactivatePaymentInProgressItem(Entity.Id);
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorDeactivateItem", ex.Message);
            }
        }

        public void DeactivateItems(List<PaymentItem> items)
        {
            try
            {
                List<int> ids = new List<int>();
                foreach (var item in items)
                {
                    ids.Add(item.Id);
                }
                PaymentItemService.DeactivatePaymentInProgressItems(ids);
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("ErrorDeactivateItems", ex.Message);
            }
        }

        public int GenerateAutomationQueue(List<PaymentItem> items)
        {
            QueueItem queueItem = new QueueItem();
            int[] ids = new int[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                ids[i] = items[i].Id;
            }
            string jsonItems = JsonConvert.SerializeObject(ids);
            queueItem.TransactionItem = jsonItems;
            queueItem.TransactionItemType = ids.GetType().FullName;
            return QueueService.InsertQueueItem(queueItem);
        }


    }
}
