﻿using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using Lear.RpaMS.UI.Data;
using Lear.RpaMS.UI.WPF.Automation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for PaymentInProgressSendToBank.xaml
    /// </summary>
    public partial class PaymentInProgressSendToBankControl : UserControl
    {
        public PaymentInProgressSendToBankControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (PaymentInProgressSendToBankViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        // View model class
        private PaymentInProgressSendToBankViewModel _viewModel = null;

        private void BtnStartAutomation_Click(object sender, RoutedEventArgs e)
        {
            List<PaymentSendToBank> itemsSelected = ViewModelHelper<PaymentSendToBank>.GetSelectedItems((ObservableCollection<BindingViewModel<PaymentSendToBank>>)dtgPaymentsInProgressSendToBank.ItemsSource);
            if (itemsSelected == null || itemsSelected.Count == 0)
            {
                MessageBox.Show("Nenhum item selecionado!");
                return;
            }

            if (MessageBox.Show("Deseja realmente enviar documentos selecionados para o banco?",
                "Enviar items selecionados?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    _viewModel.ValidationMessages.Clear();
                    int queueID = _viewModel.GenerateAutomationQueue(itemsSelected);
                    string arguments = $" \"{queueID}\"";

                    Function thisFunction = _viewModel.Manager.GetFunction(this.GetType().FullName);
                    RunAutomation runAutomation = new RunAutomation();
                    runAutomation.StartNewWindow(
                        thisFunction.GetParameterByName("StartAutomationProjectCode").Value,
                        thisFunction.GetParameterByName("StartAutomationProcessCode").Value,
                        arguments);

                }
                catch (Exception ex)
                {
                    _viewModel.AddValidationMessage("", ex.Message);
                }
            }
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            this.UpdateItems();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.UpdateItems();
        }

        private void UpdateItems()
        {
            _viewModel.LoadItems();
        }

        private void ChkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<BindingViewModel<PaymentSendToBank>> items = (ObservableCollection<BindingViewModel<PaymentSendToBank>>)dtgPaymentsInProgressSendToBank.ItemsSource;
            foreach (var item in items)
            {
                item.IsSelected = (bool)((CheckBox)e.Source).IsChecked;
            }
        }

        private void BtnViewDocument_Click(object sender, RoutedEventArgs e)
        {
            PaymentSendToBank sendToBank = (PaymentSendToBank)((Button)sender).Tag;
            _viewModel.ShowDocument(sendToBank.FullName);
        }        

        private void BtnDeleteDocument_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Deseja realmente excluir o documento selecionado?",
               "Excluir documento selecionado?",
               MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    _viewModel.ValidationMessages.Clear();
                    // Set selected item
                    PaymentSendToBank selectedDocument = (PaymentSendToBank)((Button)sender).Tag;
                    _viewModel.DeleteDocument(selectedDocument);
                    _viewModel.LoadItems();
                }
                catch (Exception ex)
                {
                    _viewModel.AddValidationMessage("", ex.Message);
                }
            }
        }

        private void ChkSelectItem_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            BindingViewModel<PaymentSendToBank> item = (BindingViewModel<PaymentSendToBank>)chk.Tag;
            item.IsSelected = Convert.ToBoolean(chk.IsChecked);
        }
    }
}
