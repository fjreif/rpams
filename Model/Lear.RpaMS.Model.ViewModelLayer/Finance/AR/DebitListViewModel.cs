﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Common.Library.Binding;
using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AR;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AR;
using Lear.RpaMS.Model.ModelLayer.RPA;
using Lear.RpaMS.UI.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AR
{
    public class DebitListViewModel : ViewModelBase
    {
        private ViewModelManager manager = null;
        private Function function = null;
        private ObservableCollection<BindingViewModel<Debit>> _BindingItems = new ObservableCollection<BindingViewModel<Debit>>();
        private ObservableCollection<TypeBase> _listType = null;
        private Debit _Entity = new Debit();
        private ItemControl _BusinessPlace;
        private ItemControl _Customer;
        private DateTime? _FiscalNoteDateFrom;
        private DateTime? _FiscalNoteDateTo;
        private DateTime? _PaydayDateFrom;
        private DateTime? _PaydayDateTo;
        private int _DebitTypeID;
        private ObservableCollection<StatusBase> _ListStatusDebit = null;
        private ObservableCollection<DebitDestinationArea> _ListDebitDestinationArea = null;
        private ObservableCollection<DebitEvidence> _ListDebitEvidence = null;
        private int _SelectedDebitDestinationAreaID;
        private int _SelectedDebitEvidenceID;
        private int _SelectedStatusDebitID;

        #region Constructor
        public DebitListViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion

        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public ObservableCollection<BindingViewModel<Debit>> BindingItems
        {
            get { return _BindingItems; }
            set
            {
                _BindingItems = value;
                RaisePropertyChanged("BindingItems");
            }
        }

        public ObservableCollection<TypeBase> ListType
        {
            get { return _listType; }
            set
            {
                _listType = value;
                RaisePropertyChanged("ListType");
            }
        }

        public Debit Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                RaisePropertyChanged("Entity");
            }
        }

        public ItemControl BusinessPlace
        {
            get { return _BusinessPlace; }
            set
            {
                _BusinessPlace = value;
                RaisePropertyChanged("BusinessPlace");
            }
        }

        public ItemControl Customer
        {
            get { return _Customer; }
            set
            {
                _Customer = value;
                RaisePropertyChanged("Customer");
            }
        }

        public DateTime? FiscalNoteDateFrom
        {
            get { return _FiscalNoteDateFrom; }
            set
            {
                _FiscalNoteDateFrom = value;
                RaisePropertyChanged("FiscalNoteDateFrom");
            }
        }

        public DateTime? FiscalNoteDateTo
        {
            get { return _FiscalNoteDateTo; }
            set
            {
                _FiscalNoteDateTo = value;
                RaisePropertyChanged("FiscalNoteDateTo");
            }
        }

        public DateTime? PaydayDateFrom
        {
            get { return _PaydayDateFrom; }
            set
            {
                _PaydayDateFrom = value;
                RaisePropertyChanged("PaydayDateFrom");
            }
        }

        public DateTime? PaydayDateTo
        {
            get { return _PaydayDateTo; }
            set
            {
                _PaydayDateTo = value;
                RaisePropertyChanged("PaydayDateTo");
            }
        }


        public ObservableCollection<StatusBase> ListStatusDebit
        {
            get { return _ListStatusDebit; }
            set
            {
                _ListStatusDebit = value;
                RaisePropertyChanged("ListStatusDebit");
            }
        }

        public ObservableCollection<DebitDestinationArea> ListDebitDestinationArea
        {
            get { return _ListDebitDestinationArea; }
            set
            {
                _ListDebitDestinationArea = value;
                RaisePropertyChanged("ListDebitDestinationArea");
            }
        }

        public ObservableCollection<DebitEvidence> ListDebitEvidence
        {
            get { return _ListDebitEvidence; }
            set
            {
                _ListDebitEvidence = value;
                RaisePropertyChanged("ListDebitEvidence");
            }
        }

        public int SelectedDebitDestinationAreaID
        {
            get { return _SelectedDebitDestinationAreaID; }
            set
            {
                _SelectedDebitDestinationAreaID = value;
                RaisePropertyChanged("SelectedDebitDestinationAreaID");
            }
        }

        public int SelectedDebitEvidenceID
        {
            get { return _SelectedDebitEvidenceID; }
            set
            {
                _SelectedDebitEvidenceID = value;
                RaisePropertyChanged("SelectedDebitEvidenceID");
            }
        }

        public int SelectedStatusDebitID
        {
            get { return _SelectedStatusDebitID; }
            set
            {
                _SelectedStatusDebitID = value;
                RaisePropertyChanged("SelectedStatusDebitID");
            }
        }

        public int DebitTypeID
        {
            get { return _DebitTypeID; }
            set
            {
                _DebitTypeID = value;
                RaisePropertyChanged("DebitTypeID");
            }
        }

        public override void Init()
        {
            try
            {
                ValidationMessages.Clear();
                function = manager.CheckAndAddFunction(this.LinkedComponent);
                DisplayStatusMessage(function.Name);
                function.Parameters = FunctionService.LoadParameters(function.Id);
                function.Processes = FunctionService.LoadProcesses(function.Id);

                this.LoadListType();
                this.LoadListDebitDestinationArea();
                this.LoadListDebitEvidences();
                this.LoadListStatusDebit();

                FiscalNoteDateFrom = DateTime.Now.AddDays(30 * -1);
                FiscalNoteDateTo = DateTime.Now;
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorInit", ex.Message);
            }
        }

        public virtual async Task LoadItems()
        {
            try
            {
                ValidationMessages.Clear();
                List<Debit> items = await DebitService.LoadDebits(
                        Convert.ToInt32(AppLayer.AppSettings.Instance.CurrentCompany.CompanyCode), 
                        Convert.ToInt32(BusinessPlace.Id), Convert.ToInt32(Customer.Id), DebitTypeID,
                        null, null, null, FiscalNoteDateFrom, FiscalNoteDateTo,
                        SelectedDebitDestinationAreaID, SelectedDebitEvidenceID, SelectedStatusDebitID,
                        PaydayDateFrom, PaydayDateTo);
                if (items != null)
                {
                    List<BindingViewModel<Debit>> itemsDebit = ViewModelHelper<Debit>.ModelToViewModel(items);
                    BindingItems = new ObservableCollection<BindingViewModel<Debit>>(itemsDebit);
                }
                else
                {
                    BindingItems = new ObservableCollection<BindingViewModel<Debit>>();
                }
                this.HideLoadingSpinner();
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorLoadItems", ex.Message);
            }
        }

        private void LoadListType()
        {
            List<TypeBase> types = TypeBaseService.LoadAllDebitType();
            types.Insert(0, new TypeBase() { Id = 0 });
            ListType = new ObservableCollection<TypeBase>(types);
        }

        private void LoadListStatusDebit()
        {
            List<StatusBase> status = StatusBaseService.LoadAllStatusDebitAR();
            status.Insert(0, new StatusBase() { Id = 0 });
            ListStatusDebit = new ObservableCollection<StatusBase>(status);
        }

        private void LoadListDebitDestinationArea()
        {
            List<DebitDestinationArea> destination = DebitService.LoadAllDestinationArea();
            destination.Insert(0, new DebitDestinationArea() { Id = 0 });
            ListDebitDestinationArea = new ObservableCollection<DebitDestinationArea>(destination);
        }

        private void LoadListDebitEvidences()
        {
            List<DebitEvidence> evidences = DebitService.LoadAllEvicences();
            evidences.Insert(0, new DebitEvidence() { Id = 0 });
            ListDebitEvidence = new ObservableCollection<DebitEvidence>(evidences);
        }

        public void LoadDocuments()
        {
            if (this.Entity != null)
            {
                FunctionParameter parameter = function.GetParameterByName("MainDirectoryDebitNotes");
                string directoryCode = parameter.Value;
                this.Entity.Documents = DebitService.LoadDocumentsByDirectory(this.Entity.Id, directoryCode, true);
            }
        }

        public void ShowDocument(Document document)
        {
            try
            {
                ValidationMessages.Clear();
                Process p = new Process();
                p.StartInfo.FileName = document.FullName;
                p.Start();
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorShowDocument", ex.Message);
            }
        }

        public List<ProjectProcess> GetProcesses()
        {
            return function.Processes;
        }

    }
}
