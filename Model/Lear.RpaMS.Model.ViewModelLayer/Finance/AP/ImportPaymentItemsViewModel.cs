﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.RPA;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class ImportPaymentItemsViewModel : ViewModelBase
    {

        public enum ReportType
        {
            ImportsToExpire,
            VendorLineItems
        }


        #region Constructor
        public ImportPaymentItemsViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion


        private string _FileReportImportsToExpire;
        private string _FileReportVendorLineItems;
        private ViewModelManager manager = null;
        private Function function = null;
        private int _importRoutineID = 0;
        
        public string FileReportImportsToExpire
        {
            get { return _FileReportImportsToExpire; }
            set
            {
                _FileReportImportsToExpire = value;
                RaisePropertyChanged("FileReportImportsToExpire");
            }
        }

        public string FileReportVendorLineItems
        {
            get { return _FileReportVendorLineItems; }
            set
            {
                _FileReportVendorLineItems = value;
                RaisePropertyChanged("FileReportVendorLineItems");
            }
        }

        public override void Init()
        {
            function = manager.CheckAndAddFunction(this.LinkedComponent);
            DisplayStatusMessage(function.Name);
            function.Parameters = FunctionService.LoadParameters(function.Id);
        }

        public void ShowDialogFileChoose(ReportType type)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel (*.xls;*.xlsx)|*.xlsx;*.xls|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                switch (type)
                {
                    case ReportType.ImportsToExpire:
                        FileReportImportsToExpire = openFileDialog.FileName;
                        break;
                    case ReportType.VendorLineItems:
                        FileReportVendorLineItems = openFileDialog.FileName;
                        break;
                }
            }

        }


        public void ImportItems()
        {
            if (Validate())
            {
                try
                {
                    ValidationMessages.Clear();
                    string dataRoutineCode = function.GetParameterByName("DATA_IMPORT_ROUTINE_CODE").Value;
                    if (_importRoutineID == 0)
                    {
                        DataImportRoutine importRoutine = DataImportRoutineService.LoadRoutineByCode(dataRoutineCode);
                        if (importRoutine == null)
                        {
                            throw new Exception($"Routine Code '{dataRoutineCode}' not found in Table RPA_DataImportRoutine.");
                        }
                        _importRoutineID = importRoutine.Id;
                    }
                    int executionID = DataImportExecutionService.StartExecution(_importRoutineID, "");

                    try
                    {
                        Business.Finance.AP.ImportPaymentItemsBusiness importBusiness = new Business.Finance.AP.ImportPaymentItemsBusiness();
                        string ret = importBusiness.ImportPendingPayments(FileReportImportsToExpire,
                            FileReportVendorLineItems,
                            AppLayer.AppSettings.Instance.CurrentCompany,
                            AppLayer.AppSettings.Instance.CurrentUser,
                            function.GetParameterByName("SHIPMENT_STATUS_OPEN").Value,
                            _importRoutineID,
                            executionID);
                        if (!String.IsNullOrEmpty(ret))
                        {
                            AddValidationMessage("ImportItems", ret);
                            DataImportExecutionService.EndExecution(executionID, StatusExecution.ERROR, ret);
                        } else
                        {
                            DataImportExecutionService.EndExecution(executionID, StatusExecution.SUCCESS, "");
                        }

                        MessageBox.Show("Importação realizada com sucesso!");
                    }
                    catch (Exception ex)
                    {
                        ValidationMessages.Clear();
                        AddValidationMessage("ErrorImportItems", ex.Message);
                        DataImportExecutionService.EndExecution(executionID, StatusExecution.ERROR, ex.Message + " - " + ex.StackTrace);
                    }
                }
                catch (Exception ex)
                {
                    ValidationMessages.Clear();
                    AddValidationMessage("ErrorImportItems", ex.Message + " - " + ex.StackTrace);
                }                

            }
        }


        private bool Validate()
        {
            bool ret = false;
            ValidationMessages.Clear();

            if (String.IsNullOrEmpty(FileReportImportsToExpire))
            {
                AddValidationMessage("FileReportImportsToExpire", "Relatório WPS precisa ser informado");
            }

            if (String.IsNullOrEmpty(FileReportVendorLineItems))
            {
                AddValidationMessage("FileReportVendorLineItems", "Relatório FBL1N precisa ser informado");
            }

            ret = (ValidationMessages.Count == 0);
            return ret;
        }




    }
}
