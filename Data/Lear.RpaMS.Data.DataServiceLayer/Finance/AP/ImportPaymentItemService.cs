﻿using Lear.RpaMS.Data.DataLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataServiceLayer.Finance.AP
{
    public class ImportPaymentItemService
    {

        public static void ImportData(DataTable table, int dataImportExecutionID)
        {
            ImportPaymentItemData data = new ImportPaymentItemData();
            data.ImportData(table, dataImportExecutionID);
        }

        public static void ImportPaymentInProgressData(DataTable table, int dataImportExecutionID)
        {
            ImportPaymentItemData data = new ImportPaymentItemData();
            data.ImportPaymentInProgressData(table, dataImportExecutionID);
        }

    }
}
