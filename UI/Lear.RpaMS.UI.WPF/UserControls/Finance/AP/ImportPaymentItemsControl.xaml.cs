﻿using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for ImportPaymentItems.xaml
    /// </summary>
    public partial class ImportPaymentItemsControl : UserControl
    {
        public ImportPaymentItemsControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (ImportPaymentItemsViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        // Login view model class
        private ImportPaymentItemsViewModel _viewModel = null;


        private void BtnImport_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ImportItems();
        }

        private void BtnChooseReportSAP_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ShowDialogFileChoose(ImportPaymentItemsViewModel.ReportType.VendorLineItems);
        }

        private void BtnChooseReportWPS_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ShowDialogFileChoose(ImportPaymentItemsViewModel.ReportType.ImportsToExpire);
        }
    }
}
