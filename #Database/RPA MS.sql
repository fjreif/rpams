CREATE TABLE [RPA_TransalationLanguage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NULL,
	[Code] [varchar](6) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_RPA_Language] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [RPA_Function](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name_EN] [varchar](100) NULL,
	[Name_PT] [varchar](100) NULL,
	[Name_ES] [varchar](100) NULL,
	[Component] [varchar](150) NULL,
	[IsNewWindow] [bit] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_RPA_Function] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [RPA_FunctionParameter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FunctionID] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Value] [varchar](max) NULL,
	[DateTimeInsert] [datetime] NULL,
	[UserCodeInsert] [varchar](15) NULL,
	[DateTimeUpdate] [datetime] NULL,
	[UserCodeUpdate] [varchar](15) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_RPA_FunctionParameter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [AP_PendingPaymentItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Company] [int] NULL,
	[VendorCode] [varchar](8) NULL,
	[VendorName] [varchar](50) NULL,
	[DocumentProvision] [varchar](10) NULL,
	[ReferenceImportation] [varchar](20) NULL,
	[ShipmentNumber] [varchar](20) NULL,
	[DocumentCurrency] [varchar](3) NULL,
	[ImportDocumentValue] [numeric](16, 2) NULL,
	[ShipmentSeqNumber] [varchar](20) NULL,
	[ImportReference] [varchar](20) NULL,
	[ExpirationDate] [datetime] NULL,
	[ShipmentStatus] [varchar](50) NULL,
	[Account] [varchar](10) NULL,
	[InvoiceReference] [varchar](20) NULL,
	[Assignment] [varchar](20) NULL,
	[InvoiceDocumentNumber] [varchar](20) NULL,
	[InvoiceDocumentType] [varchar](3) NULL,
	[InvoicePostingDate] [datetime] NULL,
	[InvoiceDocumentDate] [datetime] NULL,
	[DelayAfterNetDueDate] [int] NULL,
	[InvoiceNetDueDate] [datetime] NULL,
	[InvoiceDocumentCurrency] [varchar](3) NULL,
	[InvoiceAmountCurrency] [numeric](16, 2) NULL,
	[InvoiceAmountLocalCurrency] [numeric](16, 2) NULL,
	[InvoiceText] [varchar](300) NULL,
	[BusinessPlaceCode] [varchar](5) NULL,
	[UserCode] [varchar](15) NULL,
	[DateTimeInsert] [datetime] NULL,
	[DataImportRoutineID] [int] NULL,
	[DataImportExecutionID] [int] NULL,
 CONSTRAINT [PK_AP_PendingPaymentItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [RPA_DataImportRoutine](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name_EN] [varchar](150) NULL,
	[Name_PT] [varchar](150) NULL,
	[Name_ES] [varchar](150) NULL,
	[Description_EN] [varchar](500) NULL,
	[Description_PT] [varchar](500) NULL,
	[Description_ES] [varchar](500) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_RPA_DataImportRoutine] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [RPA_DataImportExecution](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataImportRoutineID] [int] NULL,
	[UserCode] [varchar](15) NULL,
	[MachineName] [varchar](20) NULL,
	[DateTimeStart] [datetime] NULL,
	[DateTimeEnd] [datetime] NULL,
	[Message] [varchar](max) NULL,
 CONSTRAINT [PK_RPA_DataImportExecution] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

