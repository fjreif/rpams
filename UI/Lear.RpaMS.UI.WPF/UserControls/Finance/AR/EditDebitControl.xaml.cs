﻿using Lear.RpaMS.Model.ModelLayer.Finance.AR;
using Lear.RpaMS.Model.ViewModelLayer.Finance.AR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AR
{
    /// <summary>
    /// Interaction logic for EditDebitControl.xaml
    /// </summary>
    public partial class EditDebitControl : UserControl
    {
        //Window
        private Window window;
        // View model class
        private EditDebitViewModel _viewModel = null;
        //
        private bool canClose = false;

        public EditDebitControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (EditDebitViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        public void SetEntity(Debit _entity)
        {
            _viewModel.InitEntity(_entity);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.Save();
            this.canClose = true;
            window.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.CancelEdit();
        }

        private void CancelEdit()
        {
            if (MessageBox.Show("Deseja realmente sair sem salvar alterações?",
                "Editar Débito?",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    _viewModel.ValidationMessages.Clear();
                    this.canClose = true;
                    window.Close();
                }
                catch (Exception ex)
                {
                    _viewModel.AddValidationMessage("", ex.Message);
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            window = Window.GetWindow(this);
            window.Closing += Window_Closing;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!canClose)
            {
                this.CancelEdit();
                e.Cancel = !canClose;
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {            

        }

    }
}
