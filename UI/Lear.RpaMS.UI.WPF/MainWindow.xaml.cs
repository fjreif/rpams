﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Membership;
using Lear.RpaMS.Model.ViewModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Lear.RpaMS.UI.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Main window's view model class
        private MainWindowViewModel _viewModel = null;
        // Hold the main window's original status message
        private string _originalMessage = string.Empty;

        public MainWindow()
        {
            InitializeComponent();            

            // Connect to instance of the view model created by the XAML
            _viewModel = (MainWindowViewModel)this.Resources["viewModel"];
            
            // Get the original status message
            _originalMessage = _viewModel.StatusMessage;            
            
            // Initialize the Message Broker Events
            MessageBroker.Instance.MessageReceived += Instance_MessageReceived;
        }

        private void Instance_MessageReceived(object sender, MessageBrokerEventArgs e)
        {
            switch (e.MessageName)
            {
                case MessageBrokerMessages.LOGIN_SUCCESS:
                    _viewModel.UserEntity = (User)e.MessagePayload;
                    _viewModel.LoginMenuHeader = "Logout " +
                        _viewModel.UserEntity.UserName;
                    _viewModel.UpdateStatusBarData();
                    _viewModel.LoadPreferredSettings();
                    this.CreateUserMenu();
                    break;

                case MessageBrokerMessages.LOGOUT:
                    _viewModel.UserEntity.IsLoggedIn = false;
                    _viewModel.LoginMenuHeader = "Login";
                    this.DestroyUserMenu();
                    break;

                case MessageBrokerMessages.DISPLAY_TIMEOUT_INFO_MESSAGE_TITLE:
                    _viewModel.InfoMessageTitle = e.MessagePayload.ToString();
                    _viewModel.CreateInfoMessageTimer();
                    break;

                case MessageBrokerMessages.DISPLAY_TIMEOUT_INFO_MESSAGE:
                    _viewModel.InfoMessage = e.MessagePayload.ToString();
                    _viewModel.CreateInfoMessageTimer();
                    break;

                case MessageBrokerMessages.DISPLAY_STATUS_MESSAGE:
                    // Set new status message
                    _viewModel.StatusMessage = e.MessagePayload.ToString();
                    break;

                case MessageBrokerMessages.CLOSE_USER_CONTROL:
                    CloseUserControl();
                    break;

                case MessageBrokerMessages.DISPLAY_TIMEOUT_NOTIFICATION_MESSAGE:
                    _viewModel.CreateNotificationMessageTimer(e.MessagePayload.ToString(), 1500);
                    break;

                case MessageBrokerMessages.HIDE_LOADING_SPINNER:
                    
                    break;
            }
        }        

        private async void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MenuItem mnu = (MenuItem)sender;
                string cmd = string.Empty;

                // The Tag property contains a command 
                // or the name of a user control to load
                if (mnu.Tag != null)
                {
                    cmd = mnu.Tag.ToString();
                    if (cmd.Contains("."))
                    {
                        // Display a user control
                        await LoadUserControl(cmd, mnu);
                    }
                    else
                    {
                        // Process special commands
                        ProcessMenuCommands(cmd);
                    }
                }
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("", ex.Message);                
            }           
        }

        private bool ShouldLoadUserControl(string controlName)
        {
            bool ret = true;

            // Make sure you don't reload a control already loaded.
            if (contentArea.Children.Count > 0)
            {
                if (((UserControl)contentArea.Children[0]).GetType().Name ==
                    controlName.Substring(controlName.LastIndexOf(".") + 1))
                {
                    ret = false;
                }
            }

            return ret;
        }

        private async void LoadUserControl(string controlName)
        {
            Type ucType = null;
            UserControl uc = null;

            if (ShouldLoadUserControl(controlName))
            {
                // Create a Type from controlName parameter
                ucType = Type.GetType(controlName);
                if (ucType == null)
                {
                    MessageBox.Show("The Control: " + controlName
                                     + " does not exist.");
                }
                else
                {
                    // Close current user control in content area
                    // NOTE: Optionally add current user control to a list 
                    //     so you can restore it when you close the newly added one
                    CloseUserControl();

                    // Create an instance of this control
                    uc = (UserControl)Activator.CreateInstance(ucType);
                    if (uc != null)
                    {
                        // Display control in content area
                        await DisplayUserControl(uc);
                    }
                }
            }
        }

        private async Task LoadUserControl(string controlName, MenuItem menuItem)
        {

            Type ucType = null;
            UserControl uc = null;

            if (ShouldLoadUserControl(controlName))
            {
                // Create a Type from controlName parameter
                ucType = Type.GetType(controlName);
                if (ucType == null)
                {
                    MessageBox.Show("The Control: " + controlName
                                     + " does not exist.");
                }
                else
                {
                    // Close current user control in content area
                    // NOTE: Optionally add current user control to a list 
                    //     so you can restore it when you close the newly added one
                    CloseUserControl();

                    // Create an instance of this control
                    uc = (UserControl)Activator.CreateInstance(ucType);
                    Function function = _viewModel.Manager.GetFunction(ucType.FullName);

                    if (function != null)
                    {
                        if (function.IsNewWindow)
                        {
                            //string _title = menuItem.Header.ToString();
                            string _title = function.Name;
                            if (String.IsNullOrEmpty(_title))
                            {
                                _title = "Title is not defined!!";
                            }
                            Window window = new Window
                            {
                                Title = _title,
                                Content = uc
                            };
                            if (function.ScreenHeight > 0)
                            {
                                window.Height = function.ScreenHeight;
                            }
                            if (function.ScreenWidth > 0)
                            {
                                window.Width = function.ScreenWidth;
                            }
                            window.Owner = this;
                            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                            window.ShowInTaskbar = function.IsShowInTaskbar;                            
                            window.ShowDialog();

                        } else
                        {
                            this.ShowOrHideAreas(false);
                            // Display control in content area
                            await DisplayUserControl(uc);
                        }

                    } else
                    {
                        MessageBox.Show("The Control: " + controlName
                                     + " does not exist in the Database Function Table.");
                    }



                }
            }
        }

        private void ShowOrHideAreas(bool isVisible)
        {
            MainMenu.Visibility = isVisible ? Visibility.Visible : Visibility.Collapsed;
            topArea.Visibility = isVisible ? Visibility.Visible : Visibility.Collapsed;
        }

        private void ProcessMenuCommands(string command)
        {
            switch (command.ToLower())
            {
                case "exit":
                    this.Close();
                    break;

                case "login":
                    if (_viewModel.UserEntity.IsLoggedIn)
                    {
                        // Logging out, so close any open windows
                        CloseUserControl();
                        // Reset the user object
                        _viewModel.UserEntity = new User();
                        // Make menu display Login
                        _viewModel.LoginMenuHeader = "Login";

                        // Send message that login was successful
                        MessageBroker.Instance.SendMessage(
                            MessageBrokerMessages.LOGOUT, null);
                    }
                    else
                    {
                        // Display the login screen
                        LoadUserControl(
                           "Lear.RpaMS.UI.WPF.UserControls.LoginControl");
                    }
                    break;

                default:
                    break;
            }
        }


        private void CloseUserControl()
        {
            // Remove current user control
            contentArea.Children.Clear();

            // Restore the original status message
            _viewModel.StatusMessage = _originalMessage;

            // Restore the others areas
            this.ShowOrHideAreas(true);
        }

        public async Task DisplayUserControl(UserControl uc)
        {
            // Add new user control to content area            
            //contentArea.Children.Add(uc);
            _viewModel.IsInfoMessageVisible = true;
            _viewModel.InfoMessage = "Loading ...";
            await Dispatcher.BeginInvoke(new Action(() => {
                contentArea.Children.Add(uc);
            }), DispatcherPriority.Background);

            // Turn off informational message area
            _viewModel.ClearInfoMessages();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Call method to load resources application
            await LoadApplication();

            // Turn off informational message area
            _viewModel.ClearInfoMessages();

            // Set current language
            AppLayer.AppSettings.Instance.CurrentTranslationLanguage = TranslationLanguage.PORTUGUESE;
        }

        public async Task LoadApplication()
        {
            _viewModel.LoadStatusBarData(System.Reflection.Assembly.GetExecutingAssembly());

            _viewModel.InfoMessage = "Loading Companies...";
            await Dispatcher.BeginInvoke(new Action(() => {
                _viewModel.LoadCompanies();
            }), DispatcherPriority.Background);

            _viewModel.InfoMessage = "Loading Project Settings...";
            await Dispatcher.BeginInvoke(new Action(() =>
            {
                _viewModel.LoadProjectSettings(System.Reflection.Assembly.GetExecutingAssembly());
            }), DispatcherPriority.Background);

            //_viewModel.InfoMessage = "Loading Country Codes...";
            //await Dispatcher.BeginInvoke(new Action(() => {
            //    _viewModel.LoadCountryCodes();
            //}), DispatcherPriority.Background);

            //_viewModel.InfoMessage = "Loading Employee Types...";
            //await Dispatcher.BeginInvoke(new Action(() => {
            //    _viewModel.LoadEmployeeTypes();
            //}), DispatcherPriority.Background);
        }

        private void CbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Company previousCompany = null;
            if (e.RemovedItems != null && e.RemovedItems.Count > 0)
            {
                previousCompany = (Company)e.RemovedItems[0];
            }
            ComboBox comboBox = (sender as ComboBox);
            AppLayer.AppSettings.Instance.CurrentCompany = comboBox.SelectedItem as Company;
            if (comboBox.SelectedIndex == 0)
            {
                _viewModel.SavePreferredCompany((sender as ComboBox).SelectedItem as Company,
                    previousCompany);
            }
        }
        
    }
}
