﻿using Lear.RpaMS.Common.Library;

namespace Lear.RpaMS.Model.ModelLayer
{
    public class Company : CommonBase
    {

        private string _CompanyCode;
        private string _CompanyName;
        private string _LocalCurrency;


        public string CompanyCode
        {
            get { return _CompanyCode; }
            set
            {
                _CompanyCode = value;
                RaisePropertyChanged("Company");
            }
        }

        public string CompanyName
        {
            get { return _CompanyName; }
            set
            {
                _CompanyName = value;
                RaisePropertyChanged("CompanyName");
            }
        }


        public string LocalCurrency
        {
            get { return _LocalCurrency; }
            set
            {
                _LocalCurrency = value;
                RaisePropertyChanged("LocalCurrency");
            }
        }


    }
}
