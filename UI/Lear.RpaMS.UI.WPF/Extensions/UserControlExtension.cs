﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lear.RpaMS.UI.WPF.Extensions
{
    class UserControlExtension
    {

        public static readonly DependencyProperty TitleProperty = DependencyProperty.RegisterAttached("Title",
                                                                                                        typeof(string),
                                                                                                        typeof(UserControlExtension),
                                                                                                        new PropertyMetadata(default(string)));

        public static void SetTitle(UIElement element, string value)
        {
            element.SetValue(TitleProperty, value);
        }

        public static string GetTitle(UIElement element)
        {
            return (string)element.GetValue(TitleProperty);
        }

    }
}
