﻿using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Data.DataLayer;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.UI.Data
{
    public class PaymentSendToBankData
    {

        public List<PaymentSendToBank> GetPaymentSendToBank(int company)
        {
            SqlCommand command = null;
            List<PaymentSendToBank> list = null;

            try
            {
                int[] listPaymentStateID = new int[] {
                    StatusPaymentListCode.DocumentCreated
                };
                List<PaymentItem> listPayment = PaymentItemService.LoadPaymentInProgressItemsSearch(
                    company.ToString(), StatusPaymentListCode.DocumentCreated,
                    null,
                    null,
                    null,
                    null,
                    listPaymentStateID);

                if (listPayment != null && listPayment.Count > 0)
                {
                    string listPayments = string.Empty;
                    listPayment.ForEach(item =>
                    {
                        if (listPayments.Length == 0)
                        {
                            listPayments += item.Id;
                        }
                        else
                        {
                            listPayments += "," + item.Id;
                        }
                    });

                    StringBuilder sql = new StringBuilder();
                    sql.Append(@"SELECT Max(PaymentDoc.DocumentID) AS DocumentID 
                                    ,PaymentDoc.PaymentID
                                    ,Max(Doc.Name) As Name
                                    ,Max(Doc.FullNameInsert) As FullNameInsert
                                    ,dbo.UDF_AP_GetDirectoryFolderByProject(1) + '\\' 
                                                                + FORMAT(Max(Doc.DateTimeInsert), 'yyyyMM') + '\\'
                                                                + Max(Payment.PlantName) + '\\' 
                                                                + Max(Payment.VendorCode) + '\\' 
                                                                + Max(Doc.Name) As FullName
                                    ,Doc.DocumentTypeID
                                    ,Max(Doc.DateTimeInsert) As DateTimeInsert
                                    FROM [AP_PaymentInProgressDocument] PaymentDoc
                                    INNER JOIN RPA_Document Doc ON PaymentDoc.DocumentID = Doc.ID
                                    INNER JOIN AP_PaymentInProgress Payment ON PaymentDoc.PaymentID = Payment.ID
                    ");
                    sql.Append($"WHERE Doc.DocumentTypeID = {DocumentTypeListCode.ImportPaymentCitiBank}");
                    sql.Append($"AND PaymentDoc.PaymentID IN ({listPayments})");
                    sql.Append("AND Doc.Active = 1 AND PaymentDoc.Active = 1");
                    sql.Append(@"GROUP BY PaymentDoc.PaymentID
                                    ,Doc.DocumentTypeID
                                order by Max(PaymentDoc.DocumentID)");

                    command = DataConnectionBase.GetCommand();
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = sql.ToString();
                    command.Connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        list = new List<PaymentSendToBank>();

                        while (reader.Read())
                        {
                            int documentID = Convert.ToInt32(reader["DocumentID"]);
                            PaymentSendToBank documentToSend = list.Find(item => item.DocumentID == documentID);
                            bool newItem = false;
                            if (documentToSend == null)
                            {
                                newItem = true;
                                documentToSend = new PaymentSendToBank();
                                documentToSend.DocumentID = documentID;
                                documentToSend.Name = SqlHelper.GetValue(reader["Name"], string.Empty).ToString();
                                documentToSend.FullNameInsert = SqlHelper.GetValue(reader["FullNameInsert"], string.Empty).ToString();
                                documentToSend.FullName = SqlHelper.GetValue(reader["FullName"], string.Empty).ToString();
                                documentToSend.DocumentTypeID = Convert.ToInt32(reader["DocumentTypeID"]);
                                documentToSend.DateTimeInsert = Convert.ToDateTime(reader["DateTimeInsert"]);

                                documentToSend.Items = new List<PaymentItem>();
                            }

                            int paymentID = Convert.ToInt32(reader["PaymentID"]);
                            PaymentItem paymentItem = listPayment.Find(payment => payment.Id == paymentID);
                            documentToSend.Items.Add(paymentItem);

                            if (newItem)
                            {
                                list.Add(documentToSend);
                            }
                        }
                    }

                    if (list != null && list.Count > 0)
                    {
                        foreach (var document in list)
                        {
                            double total = 0;
                            document.Items.ForEach(item =>
                            {
                                total += item.Value;
                            });
                            document.TotalAmount = total;
                        }
                    }

                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return list;
        }



    }
}
