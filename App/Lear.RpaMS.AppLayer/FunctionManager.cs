﻿using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.AppLayer
{
    public class FunctionManager
    {

        #region Instance Property
        private static FunctionManager _Instance;

        public static FunctionManager Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new FunctionManager();
                }

                return _Instance;
            }
            set { _Instance = value; }
        }
        #endregion


        private static Dictionary<string, Function> _functionList = new Dictionary<string, Function>();

        public Function GetFunction(string componentName)
        {
            if (_functionList.ContainsKey(componentName))
            {
                Function func = _functionList[componentName];
                return func;
            }
            return null;
        }


        public void AddFunction(Function function)
        {
            if (!_functionList.ContainsKey(function.Component))
            {
                _functionList.Add(function.Component, function);
            }
        }

        public List<Function> GetAllFunction()
        {
            if (_functionList.Count == 0)
            {
                return null;
            }
            List<Function> functions = new List<Function>();
            foreach (KeyValuePair<string, Function> item in _functionList)
            {
                Function _func = new Function();
                _func.FunctionId = item.Value.FunctionId;
                _func.Name = item.Value.Name;
                _func.Component = item.Value.Component;
                _func.IsNewWindow = item.Value.IsNewWindow;
                _func.Active = item.Value.Active;
                functions.Add(_func);
            }
            return functions;
        }


    }
}
