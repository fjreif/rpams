﻿using Lear.RpaMS.AppLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Lear.RpaMS.UI.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        App()
        {

            try
            {
                AppSettings.Instance.LoadSettings();

                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("pt-BR");
                CultureInfo regionalSettins = CultureInfo.CurrentCulture;
                //regionalSettins.NumberFormat.CurrencyDecimalSeparator = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message + " - " + ex.StackTrace);                
            }
        }

    }
}
