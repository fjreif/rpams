﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class ConfigPaymentDocumentViewModel : ViewModelBase
    {
        private ViewModelManager manager = null;
        private Function function = null;
        private DateTime? _DocumentDateFrom;
        private DateTime? _DocumentDateTo;
        private bool _EnableDocument6401;
        private string _Document6401Location;

        #region Constructor
        public ConfigPaymentDocumentViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion

        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public DateTime? DocumentDateFrom
        {
            get { return _DocumentDateFrom; }
            set
            {
                _DocumentDateFrom = value;
                RaisePropertyChanged("DocumentDateFrom");
            }
        }

        public DateTime? DocumentDateTo
        {
            get { return _DocumentDateTo; }
            set
            {
                _DocumentDateTo = value;
                RaisePropertyChanged("DocumentDateTo");
            }
        }

        public bool EnableDocument6401
        {
            get { return _EnableDocument6401; }
            set
            {
                _EnableDocument6401 = value;
                RaisePropertyChanged("EnableDocument6401");
            }
        }

        public string Document6401Location
        {
            get { return _Document6401Location; }
            set
            {
                _Document6401Location = value;
                RaisePropertyChanged("Document6401Location");
            }
        }

        public override void Init()
        {
            function = manager.CheckAndAddFunction(this.LinkedComponent);
            DisplayStatusMessage(function.Name);
            function.Parameters = FunctionService.LoadParameters(function.Id);

            this.LoadSettings();
        }

        private void LoadSettings()
        {
            AutoSettings settings = AutoSettingsService.LoadSettingsByCode("EnableDocument6401");
            if (settings != null)
            {
                if (string.IsNullOrEmpty(settings.Value))
                {
                    EnableDocument6401 = false;
                } else
                {
                    EnableDocument6401 = Convert.ToBoolean(settings.Value);
                }
            }

            //If document 6401 is enabled, load the other settings
            if (EnableDocument6401)
            {
                settings = AutoSettingsService.LoadSettingsByCode("Document6401DateFrom");
                if (settings != null)
                {
                    if (!string.IsNullOrEmpty(settings.Value))
                    {
                        DocumentDateFrom = Convert.ToDateTime(settings.Value);
                    }
                }
                settings = AutoSettingsService.LoadSettingsByCode("Document6401DateTo");
                if (settings != null)
                {
                    if (!string.IsNullOrEmpty(settings.Value))
                    {
                        DocumentDateTo = Convert.ToDateTime(settings.Value);
                    }
                }
                settings = AutoSettingsService.LoadSettingsByCode("Document6401Location");
                if (settings != null)
                {
                    if (!string.IsNullOrEmpty(settings.Value))
                    {
                        Document6401Location = settings.Value;
                    }
                }
            }

        }

        public bool SaveConfig()
        {
            try
            {
                ValidationMessages.Clear();
                if (EnableDocument6401)
                {
                    if (DocumentDateTo == null || DocumentDateTo == null)
                    {
                        AddValidationMessage("", "Dt. Documento De e Até devem ser informados!");
                        return false;
                    }

                    if (string.IsNullOrEmpty(Document6401Location))
                    {
                        AddValidationMessage("", "Local dos comprovantes 6401 deve ser informado!");
                        return false;
                    }

                    if (DateTime.Compare((DateTime)DocumentDateFrom, (DateTime)DocumentDateTo) > 0)
                    {
                        AddValidationMessage("", "Dt. Documento De não pode ser maior que Dt. Documento Até!");
                        return false;
                    }

                    if (!System.IO.Directory.Exists(Document6401Location))
                    {
                        AddValidationMessage("", "Local informado não existe!");
                        return false;
                    }

                }

                this.SaveToDatabase();

                DisplayNotificationMessage("Configurações salvas com sucesso!");

                return true;
            }
            catch (Exception ex)
            {
                ValidationMessages.Clear();
                AddValidationMessage("", ex.Message);

                return false;
            }
        }

        private void SaveToDatabase()
        {
            AutoSettingsService.UpdateSettingsByCode("EnableDocument6401", EnableDocument6401.ToString());
            AutoSettingsService.UpdateSettingsByCode("Document6401DateFrom", DocumentDateFrom.ToString());
            AutoSettingsService.UpdateSettingsByCode("Document6401DateTo", DocumentDateTo.ToString());
            AutoSettingsService.UpdateSettingsByCode("Document6401Location", Document6401Location.ToString());            
        }

    }
}
