﻿using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ViewModelLayer.Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Automation
{
    /// <summary>
    /// Interaction logic for RunAutomationControl.xaml
    /// </summary>
    public partial class RunAutomationControl : UserControl
    {
        // View model class
        private RunAutomationViewModel _viewModel = null;
        private string _ProjectCode;
        private string _ProcessCode;
        private string _Arguments;

        public RunAutomationControl()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (RunAutomationViewModel)this.Resources["viewModel"];
            
        }

        public string ProjectCode
        {
            get { return _ProjectCode; }
            set
            {
                _ProjectCode = value;
            }
        }

        public string ProcessCode
        {
            get { return _ProcessCode; }
            set
            {
                _ProcessCode = value;
            }
        }

        public string Arguments
        {
            get { return _Arguments; }
            set
            {
                _Arguments = value;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _viewModel.Init(ProjectCode, ProcessCode);
            }
            catch (Exception ex)
            {
                _viewModel.AddValidationMessage("Loaded", ex.Message);
            }

        }

        private void BtnCloseWindows_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.StartAutomation(this.Arguments);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.StopAutomation();
        }

        private void LbProcessOutputArea_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_viewModel.IsAutomationRunning)
            {
                var scrollViewer = ((ScrollViewer)e.OriginalSource);
                scrollViewer.ScrollToEnd();
            }
        }
    }
}
