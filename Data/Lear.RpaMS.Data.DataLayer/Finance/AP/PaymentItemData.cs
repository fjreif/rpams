﻿using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Model.ModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.Data.DataLayer.Finance.AP
{
    public class PaymentItemData
    {

        public List<PaymentItem> LoadPendingPaymentItems(string companyCode)
        {
            SqlCommand command = null;
            List<PaymentItem> items = null;

            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.SELECT);
                query.AddFields("ID, Company, PlantCode, PlantName, VendorCode, VendorName,DocumentProvision,ReferenceImportation,ShipmentNumber");
                query.AddFields("DocumentCurrency,ImportDocumentValue,ShipmentSeqNumber,ImportReference,ExpirationDate,ShipmentStatus");
                query.AddFields("Account,InvoiceReference,Assignment,InvoiceDocumentNumber,InvoiceDocumentType,InvoicePostingDate,InvoiceDocumentDate");
                query.AddFields("DelayAfterNetDueDate,InvoiceNetDueDate,InvoiceDocumentCurrency,InvoiceAmountCurrency,InvoiceAmountLocalCurrency");
                query.AddFields("InvoiceText,BusinessPlaceCode,UserCode,DateTimeInsert,DataImportRoutineID,DataImportExecutionID");
                query.AddTable("VW_AP_PendingPaymentItem");
                query.AddWhereConditions("Company = @Company");

                command = DataConnectionBase.GetCommand();
                command.CommandText = query.GetQuery();
                command.Parameters.Add("@Company", System.Data.SqlDbType.VarChar);
                command.Parameters["@Company"].Value = companyCode;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    items = new List<PaymentItem>();
                    while (reader.Read())
                    {
                        PaymentItem item = new PaymentItem();
                        item.Id = Convert.ToInt32(reader["ID"]);
                        item.Company = SqlHelper.GetValue(reader["Company"], string.Empty).ToString();
                        item.PlantCode = SqlHelper.GetValue(reader["PlantCode"], string.Empty).ToString();
                        item.PlantName = SqlHelper.GetValue(reader["PlantName"], string.Empty).ToString();
                        item.VendorCode = SqlHelper.GetValue(reader["VendorCode"], string.Empty).ToString();
                        item.VendorName = SqlHelper.GetValue(reader["VendorName"], string.Empty).ToString();
                        item.ProvisionDoc = SqlHelper.GetValue(reader["DocumentProvision"], string.Empty).ToString();
                        item.ReferenceImportation = SqlHelper.GetValue(reader["ReferenceImportation"], string.Empty).ToString();
                        item.ShipmentNumber = SqlHelper.GetValue(reader["ShipmentNumber"], string.Empty).ToString();
                        item.Currency = SqlHelper.GetValue(reader["DocumentCurrency"], string.Empty).ToString();
                        item.Value = Convert.ToDouble(SqlHelper.GetValue(reader["ImportDocumentValue"], 0));
                        item.ShipmentSeqNumber = SqlHelper.GetValue(reader["ShipmentSeqNumber"], string.Empty).ToString();                        
                        item.ImportReference = SqlHelper.GetValue(reader["ImportReference"], string.Empty).ToString();                        
                        item.ExpirationDate = SqlHelper.GetNullableDate(reader["ExpirationDate"], null);
                        item.ShipmentStatus = SqlHelper.GetValue(reader["ShipmentStatus"], string.Empty).ToString();
                        item.Account = SqlHelper.GetValue(reader["Account"], string.Empty).ToString();
                        item.InvoiceReference = SqlHelper.GetValue(reader["InvoiceReference"], string.Empty).ToString();
                        item.Assignment = SqlHelper.GetValue(reader["Assignment"], string.Empty).ToString();
                        item.InvoiceDocumentNumber = SqlHelper.GetValue(reader["InvoiceDocumentNumber"], string.Empty).ToString();
                        item.InvoiceDocumentType = SqlHelper.GetValue(reader["InvoiceDocumentType"], string.Empty).ToString();
                        item.InvoicePostingDate = SqlHelper.GetNullableDate(reader["InvoicePostingDate"], null);
                        item.InvoiceDocumentDate = SqlHelper.GetNullableDate(reader["InvoiceDocumentDate"], null);
                        item.DelayAfterNetDueDate = Convert.ToInt32(SqlHelper.GetValue(reader["DelayAfterNetDueDate"], 0));
                        item.InvoiceNetDueDate = SqlHelper.GetNullableDate(reader["InvoiceNetDueDate"], null);
                        item.InvoiceDocumentCurrency = SqlHelper.GetValue(reader["InvoiceDocumentCurrency"], string.Empty).ToString();
                        item.InvoiceAmountCurrency = Convert.ToDouble(SqlHelper.GetValue(reader["InvoiceAmountCurrency"], 0));
                        item.InvoiceAmountLocalCurrency = Convert.ToDouble(SqlHelper.GetValue(reader["InvoiceAmountLocalCurrency"], 0));
                        item.InvoiceText = SqlHelper.GetValue(reader["InvoiceText"], string.Empty).ToString();
                        item.BusinessPlaceCode = SqlHelper.GetValue(reader["BusinessPlaceCode"], string.Empty).ToString();
                        item.UserCodeInsert = SqlHelper.GetValue(reader["UserCode"], string.Empty).ToString();
                        item.DateInsert = Convert.ToDateTime(reader["DateTimeInsert"]);
                        items.Add(item);
                    }
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return items;
        }


        public List<PaymentItem> LoadPaymentInProgressItems(string companyCode)
        {
            SqlCommand command = null;
            List<PaymentItem> items = null;

            try
            {
                command = DataConnectionBase.GetCommand();
                command.CommandText = "SP_AP_GetPaymentInProgress";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add("@TranslationLanguageID", System.Data.SqlDbType.Int);
                command.Parameters["@TranslationLanguageID"].Value = AppLayer.AppSettings.Instance.CurrentTransactionLanguage;
                command.Parameters.Add("@Company", System.Data.SqlDbType.Int);
                command.Parameters["@Company"].Value = companyCode;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    items = new List<PaymentItem>();
                    while (reader.Read())
                    {
                        PaymentItem item = new PaymentItem();
                        item.Id = Convert.ToInt32(reader["ID"]);
                        item.Company = SqlHelper.GetValue(reader["Company"], string.Empty).ToString();
                        item.PlantCode = SqlHelper.GetValue(reader["PlantCode"], string.Empty).ToString();
                        item.PlantName = SqlHelper.GetValue(reader["PlantName"], string.Empty).ToString();
                        item.VendorCode = SqlHelper.GetValue(reader["VendorCode"], string.Empty).ToString();
                        item.VendorName = SqlHelper.GetValue(reader["VendorName"], string.Empty).ToString();
                        item.ProvisionDoc = SqlHelper.GetValue(reader["DocumentProvision"], string.Empty).ToString();
                        item.ReferenceImportation = SqlHelper.GetValue(reader["ReferenceImportation"], string.Empty).ToString();
                        item.ShipmentNumber = SqlHelper.GetValue(reader["ShipmentNumber"], string.Empty).ToString();
                        item.Currency = SqlHelper.GetValue(reader["DocumentCurrency"], string.Empty).ToString();
                        item.Value = Convert.ToDouble(SqlHelper.GetValue(reader["ImportDocumentValue"], 0));
                        item.ShipmentSeqNumber = SqlHelper.GetValue(reader["ShipmentSeqNumber"], string.Empty).ToString();
                        item.ImportReference = SqlHelper.GetValue(reader["ImportReference"], string.Empty).ToString();
                        item.ExpirationDate = SqlHelper.GetNullableDate(reader["ExpirationDate"], null);
                        item.ShipmentStatus = SqlHelper.GetValue(reader["ShipmentStatus"], string.Empty).ToString();
                        item.Account = SqlHelper.GetValue(reader["Account"], string.Empty).ToString();
                        item.InvoiceReference = SqlHelper.GetValue(reader["InvoiceReference"], string.Empty).ToString();
                        item.Assignment = SqlHelper.GetValue(reader["Assignment"], string.Empty).ToString();
                        item.InvoiceDocumentNumber = SqlHelper.GetValue(reader["InvoiceDocumentNumber"], string.Empty).ToString();
                        item.InvoiceDocumentType = SqlHelper.GetValue(reader["InvoiceDocumentType"], string.Empty).ToString();
                        item.InvoicePostingDate = SqlHelper.GetNullableDate(reader["InvoicePostingDate"], null);
                        item.InvoiceDocumentDate = SqlHelper.GetNullableDate(reader["InvoiceDocumentDate"], null);
                        item.DelayAfterNetDueDate = Convert.ToInt32(SqlHelper.GetValue(reader["DelayAfterNetDueDate"], 0));
                        item.InvoiceNetDueDate = SqlHelper.GetNullableDate(reader["InvoiceNetDueDate"], null);
                        item.InvoiceDocumentCurrency = SqlHelper.GetValue(reader["InvoiceDocumentCurrency"], string.Empty).ToString();
                        item.InvoiceAmountCurrency = Convert.ToDouble(SqlHelper.GetValue(reader["InvoiceAmountCurrency"], 0));
                        item.InvoiceAmountLocalCurrency = Convert.ToDouble(SqlHelper.GetValue(reader["InvoiceAmountLocalCurrency"], 0));
                        item.InvoiceText = SqlHelper.GetValue(reader["InvoiceText"], string.Empty).ToString();
                        item.BusinessPlaceCode = SqlHelper.GetValue(reader["BusinessPlaceCode"], string.Empty).ToString();
                        item.UserCodeInsert = SqlHelper.GetValue(reader["UserCode"], string.Empty).ToString();
                        item.DateInsert = Convert.ToDateTime(reader["DateTimeInsert"]);
                        items.Add(item);
                    }
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return items;
        }


    }
}
