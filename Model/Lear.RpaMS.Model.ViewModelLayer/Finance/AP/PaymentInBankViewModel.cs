﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.UI.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Lear.RpaMS.Model.ViewModelLayer.Finance.AP
{
    public class PaymentInBankViewModel : ViewModelBase
    {
        private ViewModelManager manager = null;
        private Function function = null;
        public ICollectionView ItemsGroupView { get; set; }
        private ObservableCollection<PaymentInBank> _Items = new ObservableCollection<PaymentInBank>();

        private DateTime? _AdmissionDateFrom;
        private DateTime? _AdmissionDateTo;
        private ObservableCollection<StatusBase> _listStatus = null;
        private int _StatusBankReviewID;

        #region Constructor
        public PaymentInBankViewModel() : base()
        {
            manager = new ViewModelManager();
        }
        #endregion


        public ViewModelManager Manager
        {
            get { return manager; }
        }

        public ObservableCollection<PaymentInBank> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                SetGroupDescription();
                RaisePropertyChanged("Items");
            }
        }

        public ObservableCollection<StatusBase> ListStatus
        {
            get { return _listStatus; }
            set
            {
                _listStatus = value;
                RaisePropertyChanged("ListStatus");
            }
        }

        public DateTime? AdmissionDateFrom
        {
            get { return _AdmissionDateFrom; }
            set
            {
                _AdmissionDateFrom = value;
                RaisePropertyChanged("AdmissionDateFrom");
            }
        }

        public DateTime? AdmissionDateTo
        {
            get { return _AdmissionDateTo; }
            set
            {
                _AdmissionDateTo = value;
                RaisePropertyChanged("AdmissionDateTo");
            }
        }

        public int StatusBankReviewID
        {
            get { return _StatusBankReviewID; }
            set
            {
                _StatusBankReviewID = value;
                RaisePropertyChanged("StatusBankReviewID");
            }
        }


        public override void Init()
        {
            try
            {
                function = manager.CheckAndAddFunction(this.LinkedComponent);
                DisplayStatusMessage(function.Name);
                function.Parameters = FunctionService.LoadParameters(function.Id);

                this.LoadListStatus();

                FunctionParameter parameter = function.GetParameterByName("AdmissionDateDaysFrom");
                if (parameter != null)
                {
                    int days = Convert.ToInt32(parameter.Value);
                    AdmissionDateFrom = DateTime.Now.AddDays(days * -1);
                }
                AdmissionDateTo = DateTime.Now;
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorInit", ex.Message);
            }            
        }

        private void LoadListStatus()
        {
            List<StatusBase> status = StatusBaseService.LoadAllStatusBankReview();
            status.Insert(0, new StatusBase() { Id = 0 });
            ListStatus = new ObservableCollection<StatusBase>(status);
        }

        public virtual void LoadItems()
        {
            try
            {
                ValidationMessages.Clear();
                PaymentInBankData data = new PaymentInBankData();
                Items = new ObservableCollection<PaymentInBank>(data.GetPaymentInBank(
                    Convert.ToInt32(AppLayer.AppSettings.Instance.CurrentCompany.CompanyCode),
                    _AdmissionDateFrom,
                    _AdmissionDateTo,
                    _StatusBankReviewID));
            }
            catch (Exception ex)
            {
                AddValidationMessage("ErrorLoadItems", ex.Message);
            }
        }

        private void SetGroupDescription()
        {
            ItemsGroupView = CollectionViewSource.GetDefaultView(Items);
            ItemsGroupView.GroupDescriptions.Add(new PropertyGroupDescription("Reference"));
            
        }

    }
}
