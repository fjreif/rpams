﻿using Lear.RpaMS.Common.Library.Helper;
using Lear.RpaMS.Data.DataLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lear.RpaMS.UI.Data
{
    public class TreeViewInvoiceData
    {

        public List<TrackingNumberNode> GetTreeViewNode(int projectID)
        {
            SqlCommand command = null;
            List<TrackingNumberNode> nodeList = null;
            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.SELECT);
                query.AddFields("TrackingNumber, DocumentID ");
                query.AddTable("VW_AP_TrackingNumberDocument");
                command = DataConnectionBase.GetCommand();
                command.CommandText = query.GetQuery();
                command.CommandType = System.Data.CommandType.Text;
                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    nodeList = new List<TrackingNumberNode>();
                    while (reader.Read())
                    {
                        TrackingNumberNode node = new TrackingNumberNode();
                        node.Invoices = new ObservableCollection<InvoiceDocument>();
                        node.TrackingNumber = Convert.ToString(reader["TrackingNumber"]);
                        node.DocumentID = Convert.ToInt32(reader["DocumentID"]);
                        nodeList.Add(node);
                    }
                }

                if (nodeList != null && nodeList.Count > 0)
                {                    
                    command.CommandText = "SP_AP_GetPaymentInProgressDocumentInvoiceValues";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add("@ProjectID", System.Data.SqlDbType.Int);
                    command.Parameters["@ProjectID"].Value = projectID;

                    List<InvoiceDocument> invoices = new List<InvoiceDocument>();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            InvoiceDocument invoice = new InvoiceDocument();
                            invoice.DocumentID = Convert.ToInt32(reader["DocumentID"]);
                            invoice.DocumentTypeID = Convert.ToInt32(reader["DocumentTypeID"]);
                            invoice.DocumentPageID = Convert.ToInt32(reader["DocumentPageID"]);
                            invoice.Name = SqlHelper.GetValue(reader["Name"], string.Empty).ToString();
                            invoice.DocumentPageFullName = SqlHelper.GetValue(reader["DocumentPageFullName"], string.Empty).ToString();
                            invoice.DocumentPageDataID = SqlHelper.GetNullableInt(reader["DocumentPageDataID"], null);
                            invoice.DocumentValueTypeID = SqlHelper.GetNullableInt(reader["DocumentValueTypeID"], null);
                            invoice.Value = SqlHelper.GetValue(reader["Value"], string.Empty).ToString();
                            invoices.Add(invoice);
                        }
                    }

                    if (invoices != null && invoices.Count > 0)
                    {
                        foreach (TrackingNumberNode node in nodeList)
                        {
                            List<InvoiceDocument> invoicesFound = invoices.FindAll(item => item.DocumentID == node.DocumentID);
                            if (invoicesFound != null && invoicesFound.Count > 0)
                            {
                                node.Invoices = new ObservableCollection<InvoiceDocument>(invoicesFound);
                                foreach (var invoiceDocument in node.Invoices)
                                {
                                    invoiceDocument.DocumentPageFullName = invoiceDocument.DocumentPageFullName.Replace("[@TrackingNumber]", node.TrackingNumber);
                                }
                            }
                        }
                    }

                }              

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }
            return nodeList;
        }

    }
}
