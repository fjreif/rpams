﻿using Lear.RpaMS.Model.ViewModelLayer.Finance.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lear.RpaMS.UI.WPF.UserControls.Finance.AP
{
    /// <summary>
    /// Interaction logic for EditPaymentInProgress.xaml
    /// </summary>
    public partial class EditPaymentInProgress : UserControl
    {
        public EditPaymentInProgress()
        {
            InitializeComponent();

            // Connect to instance of the view model created by the XAML
            _viewModel = (EditPaymentInProgressViewModel)this.Resources["viewModel"];
            _viewModel.LinkedComponent = this.GetType().FullName;
            _viewModel.Init();
        }

        // View model class
        private EditPaymentInProgressViewModel _viewModel = null;

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }



    }
}
